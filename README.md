# InterConnexion Manager

## Launch dev server

`./craft serve`

default is `localhost:3333`

`./craft serve localhost:8000`

`localhost:4444` is used by **VvvebJs**

## Vault file
### encrypt

`./craft encrypt file.ext your_password`

```
./craft encrypt includes/keys/root.json `cat craft.pwd`
./craft encrypt includes/keys/api.json `cat craft.pwd`
```

### decrypt

`./craft decrypt file.ext.enc your_password`

```
./craft decrypt includes/keys/root.json.enc `cat craft.pwd`
./craft decrypt includes/keys/api.json.enc `cat craft.pwd`
```

## Add road

`./craft road:add `

### with ICM

`php icm.php -Pcraft/road/actions -q"action=add&inchtml=1&incjs=1&path=root/machintruc&method=get"`

## Utilisation d'ICM

### Mode Shell

`php icm.php -itest.csv -Itest -Ptest -Otest -otest-final`

#### Usage
```
-i input file
-I input map class
-P process map class
-O output map class
-o output file
-l data payload display (input, process, output)
-e embedded mode (default false)
-q query parameters
```

### Mode API

#### Curl

```
curl --location --request GET 'http://localhost/test' \
	--header 'icmauth: xxxxxxxxxxxxxxxxxxxxxxx' \
	--header 'icmif: test.csv' \
	--header 'icmic: test' \
	--header 'icmpc: test' \
	--header 'icmoc: test' \
	--header 'icmof: test-final' \
	--header 'icmx: output' 
```

#### Wget

```
wget --no-check-certificate --quiet \
	--method GET \
	--header 'icmauth: xxxxxxxxxxxxxxxxxxxxxxx' \
	--header 'icmif: test.csv' \
	--header 'icmic: test' \
	--header 'icmpc: test' \
	--header 'icmoc: test' \
	--header 'icmof: test-final'\
	--header 'icmx: output' \
	'http://localhost/test'
```

#### PHP

```
$curl = curl_init();
curl_setopt_array(
	$curl, 
	array(
		CURLOPT_URL => 'http://localhost/test',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'icmauth: xxxxxxxxxxxxxxxxxxxxxxx',
			'icmif: test.csv',
			'icmic: test',
			'icmpc: test',
			'icmoc: test',
			'icmof: test-final'
			'icmx: output'
		),
	)
);
$response = curl_exec($curl);
curl_close($curl);
echo $response;
```

#### NodeJS

```
var request = require('request');
var options = {
	'method': 'GET',
	'url': 'http://localhost/test',
	'headers': {
		'icmauth': 'xxxxxxxxxxxxxxxxxxxxxxx',
		'icmif': 'test.csv',
		'icmic': 'test',
		'icmpc': 'test',
		'icmoc': 'test',
		'icmof': 'test-final'
		'icmx:' 'output'
	}
};
request(options, function (error, response) {
	if (error) throw new Error(error);
	console.log(response.body);
});
```

#### Paramètres dans l'entête de la requête

```
icmauth: access key
icmif: input file
icmic: input map class
icmpc: process map class
icmoc: output map class
icmof: output file
icmx: data payload display (input, process, output)
```

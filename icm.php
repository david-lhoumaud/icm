<?php

include 'includes/boot.inc.php';
require_once 'includes/infra.class.php';
require_once 'includes/seeder.class.php';
require_once 'includes/classes.class.php';

$__error__=Helpers::load('error');
set_error_handler(array(&$__error__,'error'));

require_once 'includes/views.class.php';
require_once 'includes/maps.class.php';
require_once 'includes/roads.class.php';
require_once 'includes/queries.class.php';
require_once 'includes/interco.class.php';
require_once 'includes/payloader.class.php';
require_once 'includes/controller.class.php';
/**
 * Classe principal de lancement de ICM.
 * 
 * @return stdClass
 */
class ICM {
	
	/**
	 * Propriété de routage Roads ou Controller
	 * @var stdClass
	 */
	public $ICM;
	/**
	 * Lance le routage en mode ligne de commande ou service Web.
	 * Etapes : \
	 *  1 - chargemement de la classe de mappage 'input' \
	 *  2 - rechargement du payload avec Input::__use() \
	 *  3 - Lancement du service Controller|Roads::__input() \
	 *  4 - rechargement du payload avec Input::__end() \
	 * \
	 *  5 - chargemement de la classe de mappage 'process' \
	 *  6 - rechargement du payload avec Process::__use() \
	 *  7 - Lancement du service Controller|Roads::__process() \
	 *  8 - rechargement du payload avec Process::__end() \
	 * \
	 *  9 - chargemement de la classe de mappage 'ouput' \
	 * 10 - rechargement du payload avec Ouput::__use() \
	 * 11 - Lancement du service Controller|Roads::__ouput() \
	 * 12 - rechargement du payload avec Ouput::__end()
	 * 
	 * @example icm.php Lancement en mode Shell "new Controller(self::__params())"
	 * @example icm.php 47 1 Lancement en mode API "Roads::load($method.'/'.($icmroot??''))"
	 * @example icm.php Chargement du mappage d'entré "Maps::load('input/'.$this->ICM->payload('maps/input'), $this->ICM->payload())"
	 * @example icm.php Chargement du mappage intermédiaire "Maps::load('process/'.$this->ICM->payload('maps/process'), $this->ICM->payload())"
	 * @example icm.php Chargement du mappage de sortie "Maps::load('output/'.$this->ICM->payload('maps/output'), $this->ICM->payload())"
	 * @param array|null $_params paramètres de lancement (mode Embarqué)
	 * @param string $origin_class nom de la classe appellante (mode Embarqué)
	 * @return void
	 */
	public function __construct($_params=null, $origin_class='', $__error__=null){
		$method=strtolower($_SERVER['REQUEST_METHOD']??'CMD');
		if ($method=='cmd' && isset($_SERVER['REQUEST_METHOD'])) {
			Helpers::load('output')->error(401);
		}
		if ($method=='cmd' || !is_null($_params)){
			$method='cmd';
			$this->ICM=new Controller(self::__params($_params));
		} else {
			$icmroot=preg_replace('/^\/(.*)$/', '\1', explode('?',$_SERVER['REQUEST_URI']??'')[0]);
			Helpers::load('session');
			if (!($this->ICM=Roads::load($method.'/'.($icmroot??'')))) {
				Helpers::load('output')->error(404);
			}
			$this->ICM->__config();
			if (method_exists($this->ICM, '__init')) $this->ICM->__init();
			$this->ICM->checkPermission();
		}
		$INPUT=Maps::load('input/'.$this->ICM->payload('maps/input'), $this->ICM->payload(), $origin_class);
		if ($INPUT){
			if (method_exists($INPUT, '__construct') && $this->ICM->payload('request/method')!='cmd') $this->ICM->checkPermission($INPUT->for??'root', 'input');
			if (method_exists($INPUT, '__use')) $this->ICM->payloadReload($INPUT->__use());
		}
		if (method_exists($this->ICM, '__input')) $this->ICM->__input();
		if ($INPUT && method_exists($INPUT, '__end')) $this->ICM->payloadReload($INPUT->__end($this->ICM->payload()));
		
		
		
		$PROCESS=Maps::load('process/'.$this->ICM->payload('maps/process'), $this->ICM->payload(), $origin_class);
		if ($PROCESS){
			if (method_exists($PROCESS, '__construct') && $this->ICM->payload('request/method')!='cmd') $this->ICM->checkPermission($PROCESS->for??'root', 'process');
			if (method_exists($PROCESS, '__use')) $this->ICM->payloadReload($PROCESS->__use());
		}
			if (method_exists($this->ICM, '__process')) $this->ICM->__process();
		if ($PROCESS && method_exists($PROCESS, '__end')) $this->ICM->payloadReload($PROCESS->__end($this->ICM->payload()));
		
		if (empty($this->ICM->payload('datas/process'))){
			$this->ICM->payload('datas/process', $this->ICM->payload('datas/input'));
		}
		
		$OUTPUT=Maps::load('output/'.$this->ICM->payload('maps/output'), $this->ICM->payload(), $origin_class);
		if ($OUTPUT){
			if (method_exists($OUTPUT, '__construct') && $this->ICM->payload('request/method')!='cmd') $this->ICM->checkPermission($OUTPUT->for??'root', 'output');
			if (method_exists($OUTPUT, '__use')) $this->ICM->payloadReload($OUTPUT->__use());
		}
		if (!is_null($__error__) && count($__error__->err)>0)$this->ICM->payload('error/scripts',$__error__->err);
		if (method_exists($this->ICM, '__output'))$this->ICM->__output();
		if ($OUTPUT && method_exists($OUTPUT, '__end')) $this->ICM->payloadReload($OUTPUT->__end($this->ICM->payload()));
		
		
	}
	
	/**
	 * Retourne ou inscrit une donnée dans le payload
	 * @method Controller::payload ou Roads::payload
	 * @param string $keys sous forme de chemin exemple : self::payload('data/input')
	 * @param mixed $value donnée à inscrire
	 * @return array|mixed contenu réclamé au payload
	 */
	public function payload($key=null, $value=null){
		return $this->ICM->payload($key, $value);
	}
	
	/**
	 * Gestion des paramètres envoyé en ligne de commande
	 * 
	 * @param array|null $_params si non null, on utilise ces paramètres
	 * @return array Liste des options
	 */
	private function __params($_params=null){
		$shortopts 	=  'e'			// embed using
									.'l::'		// input/process/output
									.'r::'		// input raws
									.'i::'		// input file
									.'I::'		// input
									.'P::'		// process
									.'O::'		// output
									.'o::'		// output file
									.'q::';		// output file
		$options 			= (is_null($_params)?getopt($shortopts):$_params);
		$options['e']	=	(isset($options['e'])?(!empty($options['e'])?$options['e']:true):false);
		if (isset($options['r'])) {
			$options['r']	=	(!empty($options['r'])?$options['r']:'');
		}
		if (isset($options['l'])) {
			$options['l']	=	(!empty($options['l'])?$options['l']:'output');
		}
		$options['i']	=	(isset($options['i'])?(!empty($options['i'])?$options['i']:'icm.input'):'icm.input');
		$options['I']	=	(isset($options['I'])?(!empty($options['I'])?$options['I']:false):false);
		$options['P']	=	(isset($options['P'])?(!empty($options['P'])?$options['P']:false):false);
		$options['O']	=	(isset($options['O'])?(!empty($options['O'])?$options['O']:false):false);
		$options['o']	=	(isset($options['o'])?(!empty($options['o'])?$options['o']:'icm.output'):'icm.output');
		$options['q']	=	(isset($options['q'])?(!empty($options['q'])?$options['q']:false):false);
		return $options;
	}
	
}

$ICM = new ICM(null, '', $__error__,);
#! /bin/bash

[[ ! -d "docs/" ]] && mkdir "docs/"
[[ ! -d "docs/icm" ]] && mkdir "docs/icm"
[[ ! -d "docs/maps" ]] && mkdir "docs/maps"
[[ ! -d "docs/roads" ]] && mkdir "docs/roads"
[[ ! -d "docs/helpers" ]] && mkdir "docs/helpers"
[[ ! -d "docs/queries" ]] && mkdir "docs/queries"
[[ ! -d "docs/interco" ]] && mkdir "docs/interco"
[[ ! -d "docs/views" ]] && mkdir "docs/views"


echo "
Création de la documentation de ICM"
./phpDocumentor.phar run --title="Documentation ICM" --parseprivate \
--ignore="docs/" \
--ignore="includes/maps" \
--ignore="includes/maps.class.php" \
--ignore="includes/roads" \
--ignore="includes/roads.class.php" \
--ignore="includes/helpers" \
--ignore="includes/helpers.class.php" \
--ignore="includes/queries" \
--ignore="includes/queries.class.php" \
--ignore="includes/interco" \
--ignore="includes/interco.class.php" \
--ignore="includes/views" \
--ignore="includes/views.class.php" \
--ignore="backbot/" \
--ignore="libraries/" \
--ignore="files/input/" \
--ignore="files/output/" \
--ignore="includes/statics/" \
--ignore="includes/templates/" \
--force -d ./ -t docs/icm/


echo "
Création de la documentation des classes de mappage 'Maps'"
./phpDocumentor.phar run --title="Documentation Maps" --parseprivate \
--ignore="docs/" \
--ignore="includes/classes.class.php" \
--ignore="includes/roads" \
--ignore="includes/roads.class.php" \
--ignore="includes/helpers" \
--ignore="includes/helpers.class.php" \
--ignore="includes/queries" \
--ignore="includes/queries.class.php" \
--ignore="includes/interco" \
--ignore="includes/interco.class.php" \
--ignore="includes/views" \
--ignore="includes/views.class.php" \
--ignore="includes/infra.class.php" \
--ignore="includes/matchs" \
--ignore="includes/controller.class.php" \
--ignore="includes/map.class.php" \
--ignore="includes/data.class.php" \
--ignore="includes/tools.class.php" \
--ignore="includes/payloader.class.php" \
--ignore="includes/proxy-bot.class.php" \
--ignore="backbot/" \
--ignore="libraries/" \
--ignore="files/input/" \
--ignore="files/output/" \
--ignore="includes/statics/" \
--ignore="includes/templates/" \
--force -d ./includes -t docs/maps/

echo "
Création de la documentation des classes de routage 'Roads'"
./phpDocumentor.phar run --title="Documentation Roads" --parseprivate \
--ignore="docs/" \
--ignore="includes/classes.class.php" \
--ignore="includes/maps" \
--ignore="includes/maps.class.php" \
--ignore="includes/helpers" \
--ignore="includes/helpers.class.php" \
--ignore="includes/queries" \
--ignore="includes/queries.class.php" \
--ignore="includes/interco" \
--ignore="includes/interco.class.php" \
--ignore="includes/views" \
--ignore="includes/views.class.php" \
--ignore="includes/infra.class.php" \
--ignore="includes/matchs" \
--ignore="includes/controller.class.php" \
--ignore="includes/map.class.php" \
--ignore="includes/data.class.php" \
--ignore="includes/tools.class.php" \
--ignore="includes/payloader.class.php" \
--ignore="includes/proxy-bot.class.php" \
--ignore="backbot/" \
--ignore="libraries/" \
--ignore="files/input/" \
--ignore="files/output/" \
--ignore="includes/statics/" \
--ignore="includes/templates/" \
--force -d ./includes -t docs/roads/

echo "
Création de la documentation des classes d'outils 'Helpers'"
./phpDocumentor.phar run --title="Documentation Helpers" --parseprivate \
--ignore="docs/" \
--ignore="includes/classes.class.php" \
--ignore="includes/maps" \
--ignore="includes/maps.class.php" \
--ignore="includes/roads" \
--ignore="includes/roads.class.php" \
--ignore="includes/queries" \
--ignore="includes/queries.class.php" \
--ignore="includes/interco" \
--ignore="includes/interco.class.php" \
--ignore="includes/views" \
--ignore="includes/views.class.php" \
--ignore="includes/infra.class.php" \
--ignore="includes/matchs" \
--ignore="includes/controller.class.php" \
--ignore="includes/map.class.php" \
--ignore="includes/data.class.php" \
--ignore="includes/tools.class.php" \
--ignore="includes/payloader.class.php" \
--ignore="includes/proxy-bot.class.php" \
--ignore="backbot/" \
--ignore="libraries/" \
--ignore="files/input/" \
--ignore="files/output/" \
--ignore="includes/statics/" \
--ignore="includes/templates/" \
--force -d ./includes -t docs/helpers/

echo "
Création de la documentation des classes de requêtes 'Queries'"
./phpDocumentor.phar run --title="Documentation Queries" --parseprivate \
--ignore="docs/" \
--ignore="includes/classes.class.php" \
--ignore="includes/maps" \
--ignore="includes/maps.class.php" \
--ignore="includes/roads" \
--ignore="includes/roads.class.php" \
--ignore="includes/helpers" \
--ignore="includes/helpers.class.php" \
--ignore="includes/interco" \
--ignore="includes/interco.class.php" \
--ignore="includes/views" \
--ignore="includes/views.class.php" \
--ignore="includes/infra.class.php" \
--ignore="includes/matchs" \
--ignore="includes/controller.class.php" \
--ignore="includes/map.class.php" \
--ignore="includes/data.class.php" \
--ignore="includes/tools.class.php" \
--ignore="includes/payloader.class.php" \
--ignore="includes/proxy-bot.class.php" \
--ignore="backbot/" \
--ignore="libraries/" \
--ignore="files/input/" \
--ignore="files/output/" \
--ignore="includes/statics/" \
--ignore="includes/templates/" \
--force -d ./includes -t docs/queries/

echo "
Création de la documentation des classes d'interconnexion"
./phpDocumentor.phar run --title="Documentation Interco" --parseprivate \
--ignore="docs/" \
--ignore="includes/classes.class.php" \
--ignore="includes/maps" \
--ignore="includes/maps.class.php" \
--ignore="includes/roads" \
--ignore="includes/roads.class.php" \
--ignore="includes/helpers" \
--ignore="includes/helpers.class.php" \
--ignore="includes/views" \
--ignore="includes/views.class.php" \
--ignore="includes/infra.class.php" \
--ignore="includes/queries" \
--ignore="includes/queries.class.php" \
--ignore="includes/matchs" \
--ignore="includes/controller.class.php" \
--ignore="includes/map.class.php" \
--ignore="includes/data.class.php" \
--ignore="includes/tools.class.php" \
--ignore="includes/payloader.class.php" \
--ignore="includes/proxy-bot.class.php" \
--ignore="backbot/" \
--ignore="libraries/" \
--ignore="files/input/" \
--ignore="files/output/" \
--ignore="includes/statics/" \
--ignore="includes/templates/" \
--force -d ./includes -t docs/interco/

echo "
Création de la documentation des classes des vues globales"
./phpDocumentor.phar run --title="Documentation Views" --parseprivate \
--ignore="docs/" \
--ignore="includes/classes.class.php" \
--ignore="includes/maps" \
--ignore="includes/maps.class.php" \
--ignore="includes/roads" \
--ignore="includes/roads.class.php" \
--ignore="includes/helpers" \
--ignore="includes/helpers.class.php" \
--ignore="includes/interco" \
--ignore="includes/interco.class.php" \
--ignore="includes/infra.class.php" \
--ignore="includes/queries" \
--ignore="includes/queries.class.php" \
--ignore="includes/matchs" \
--ignore="includes/controller.class.php" \
--ignore="includes/map.class.php" \
--ignore="includes/data.class.php" \
--ignore="includes/tools.class.php" \
--ignore="includes/payloader.class.php" \
--ignore="includes/proxy-bot.class.php" \
--ignore="backbot/" \
--ignore="libraries/" \
--ignore="files/input/" \
--ignore="files/output/" \
--ignore="includes/statics/" \
--ignore="includes/templates/" \
--force -d ./includes -t docs/views/

touch "docs/index.html"

echo '''<html>
<head>
<meta charset="utf-8">
<title>Documentations</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="icm/images/favicon.ico"/>
<link rel="stylesheet" href="icm/css/normalize.css">
<link rel="stylesheet" href="icm/css/base.css">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="icm/css/template.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/css/all.min.css" integrity="sha256-ybRkN9dBjhcS2qrW1z+hfCxq+1aBdwyQM5wlQoQVt/0=" crossorigin="anonymous" />
<script src="https://cdn.jsdelivr.net/npm/fuse.js@3.4.6"></script>
<script src="https://cdn.jsdelivr.net/npm/css-vars-ponyfill@2"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/js/all.min.js" integrity="sha256-0vuk8LXoyrmCjp1f0O300qo1M75ZQyhH9X3J6d+scmk=" crossorigin="anonymous"></script>
</head>
<body id="top">
<div class="phpdocumentor-column -eight phpdocumentor-content" style="margin-left:1%">
	<h2>Documentations du ''' > docs/index.html
echo `date '+%d/%m/%Y à %T'` >> docs/index.html
echo '''</h2>
	<p>Le Framework ICM permet d&apos;effectuer des mappages de donnée de façon organisé.</p>
	<p>ICM (Inter Connexion Manager)</p>
	<p>Développeur: David Lhoumaud</p>
	<dl class="phpdocumentor-table-of-contents">
		<dt class="phpdocumentor-table-of-contents__entry"><a href="using.html" target="_blank"><abbr title="Bash">Utilisation en mode Shell ou API</abbr></a></dt>
		<dd>Exemples d&apos;utilisation de ICM</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="permissions.html" target="_blank"><abbr title="Bash">Les permissions du Mappage et du Routage</abbr></a></dt>
		<dd>Disponibilité des services avec différents niveau de permission</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="payload.html" target="_blank"><abbr title="Class Payload">Utilisation du Payload</abbr></a></dt>
		<dd>Étapes de transmission du <b>Payload</b> dans les classes de <b>Mappage</b> et <b>Routage</b></dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="map.html" target="_blank"><abbr title="Class Map">Utilisation du Mappage</abbr></a></dt>
		<dd>Utilisation du <b>Mappage</b> avec le <b>Payload</b></dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="nginx.html" target="_blank"><abbr title="Serveur HTTP Nginx">Configuration de base de Nginx</abbr></a></dt>
		<dd>Exemples de configuration avec Nginx</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="embedded.html" target="_blank"><abbr title="Embedded">Utilisation embarqué</abbr></a></dt>
		<dd>Exemples d&apos;utilisation de ICM via une autre requete ICM</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="ecbot.html" target="_blank"><abbr title="Easy Curl Bot">Utilisation de ECBot</abbr></a></dt>
		<dd>Exemples d&apos;utilisation de ECBot avec ICM</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="icm/index.html" target="_blank"><abbr title="Input Process Output">Documentation ICM</abbr></a></dt>
		<dd>ICM permet de lancer des requêtes de mappage de type Entrée/Sortie. Il fonctionne en API et en ligne de commande</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="maps/index.html" target="_blank"><abbr title="Class Maps">Documentation des classes de mappage &apos;Maps&apos;</abbr></a></dt>
		<dd>Charge une classe de mappage avec un chemin relatif au répertoire de la classe courante, le sous dossier maps/ et sans extention de nom de fichier</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="roads/index.html" target="_blank"><abbr title="Class Roads">Documentation des classes de routage &apos;Roads&apos;</abbr></a></dt>
		<dd>Charge une classe de routage avec un chemin relatif au répertoire de la classe courante, le sous dossier roads/ et sans extention de nom de fichier</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="helpers/index.html" target="_blank"><abbr title="Class Helpers">Documentation des classes d&apos;outils &apos;Helpers&apos;</abbr></a></dt>
		<dd>Charge une classe d&apos;outil avec un chemin relatif au répertoire de la classe courante et sans extention de nom de fichier</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="queries/index.html" target="_blank"><abbr title="Class Queries">Documentation des classes de requête &apos;Queries&apos;</abbr></a></dt>
		<dd>Charge une classe de requete avec un chemin relatif au répertoire de la classe courante et sans extention de nom de fichier</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="interco/index.html" target="_blank"><abbr title="Class Interco">Documentation des classes &apos;Interco&apos;</abbr></a></dt>
		<dd>Charge une classe d&apos;interconnexion avec un chemin relatif au répertoire de la classe courante et sans extention de nom de fichier</dd>
		
		<dt class="phpdocumentor-table-of-contents__entry"><a href="views/index.html" target="_blank"><abbr title="Class Views">Documentation des classes &apos;Views&apos;</abbr></a></dt>
		<dd>Charge une classe des vues globales avec un chemin relatif au répertoire de la classe courante et sans extention de nom de fichier</dd>
	</dl>
</div>
</body>
</html>''' >> docs/index.html

sudo cp -R /etc/nginx/ ./
sudo chown -R nomdesession:nomdesession nginx/
tar --exclude=./phpDocumentor.phar \
	--exclude=icm.api/libraries \
	--exclude=icm.api/backbot \
	--exclude=icm.api/.phpdoc \
	--exclude=./*.tar.gz \
	--exclude=./*.tgz \
	--exclude=./*.tar.bz2 \
	--exclude=./*.tbz2 \
	--exclude=./*.csv \
	--exclude=./*.dat \
	--exclude=./*.zip \
	--exclude=./*.rar \
	--exclude=./*.7z \
	--exclude=cron/*.tar.gz \
	--exclude=cron/*.tgz \
	--exclude=cron/*.tar.bz2 \
	--exclude=cron/*.tbz2 \
	--exclude=cron/*.csv \
	--exclude=cron/*.json \
	--exclude=cron/*.txt \
	--exclude=cron/*.dat \
	--exclude=cron/*.zip \
	--exclude=cron/*.rar \
	--exclude=cron/*.7z \
	--exclude=icm.api/.git \
	--exclude=icm.api/files/*.csv \
	--exclude=icm.api/files/*.json \
	--exclude=icm.api/files/*.xml \
	--exclude=icm.api/files/*.dat \
	--exclude=icm.api/files/*.txt \
	--exclude=icm.api/*.tgz \
	-czif icm-`date '+%Y%m%d%H%M%S'`.tgz ../ 

echo "Lancer cette commande en 'root' pour ajuster les permissions : chown -R www-data ./"


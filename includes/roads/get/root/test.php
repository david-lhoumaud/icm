<?php
/**
 * Classe de routage GET GetRootTest.
 * 
 * @return stdClass
 */
class GetRootTest extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='public';
	
	public function __construct(){
		$this->html=Helpers::load('html');
		$this->js=Helpers::load('js');
		$this->view=Views::load('root/menu', $this->html);
	}
	
	/**
	 * Initialisation de la route.
	 * 
	 * @return void
	 */
	public function __init(){
		$this->session->set('error-location','test');
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->payload('datas/output/head',
			$this->html->title('ICM - GetRootTest')
		);
		$this->session->access(true);
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		$content=$this->html->div(
			'Contenu de GetRootTest ...'
		);
		$this->payload('datas/output/body', $content);
	}
	
	
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->payload('datas/output/head'), 
				$this->payload('datas/output/body')
			)
		);
	}

}

?>

<?php
/**
 * Classe de routage GET GetRootInfrasManager.
 * 
 * @return stdClass
 */
class GetRootInfrasManager extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	public function __construct(){
		$this->html=Helpers::load('bootstrap');
		$this->menu=Views::load('root/menu', $this->html);
	}
	
	/**
	 * Initialisation de la route.
	 * 
	 * @return void
	 */
	public function __init(){
		$this->session->set('error-location','manager');
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->payload('datas/output/head',
			$this->html->title('ICM - Infras')
			.$this->html->load()
		);
		$this->session->access(true);
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		self::infras_list();
		$content=self::__alert()
						.$this->html->container(
							'</p><p><p class="h1 text-center text-light" >ICM - Infras</p></p><p>'
							.$this->menu->managers('infras', '../')
							.self::diagram($this->payload('datas/process'))
							.'<form method="post" id="form-delete-infra">
							<input type="hidden" id="scroll" name="scroll-'.$this->session->access().'" value="0">
							<input type="hidden" name="action-'.$this->session->access().'" value="delete-infra">
							<input type="hidden" id="delete-infra" name="delete-infra-'.$this->session->access().'" value=""></form>'
						);
		$this->payload('datas/output/body', $content);
	}
	
	/**
	 * Récupère l'architecture des json de configuration des infras.
	 * 
	 * @return void
	 */
	private function infras_list(){
		foreach($this->ls('includes/infras') as $file){
			if (!preg_match('/\.json$/',$file)) continue;
			$path=str_replace(['includes/infras/', '.json'],'',$file);
			$p=explode('/', $path);
			$o='';
			foreach ($p as $index => $value) {
				$o.='/'.$value;
				if (!$this->payload_exists('datas/process'.$o)){
					if ($index==count($p)-1) $this->payload('datas/process'.$o, true);
					else $this->payload('datas/process'.$o, array());
				}
			}
		}
		ksort($this->payload['datas']['process']);
	}
	
	/**
	 * Génère un tableau html pour afficher l'arborescence des données.
	 * 
	 * @return string retourne le tableau HTML
	 */
	private function diagram($datas, $theme='dark', $start=true, $parent='', $start_name=''){
		if (!is_array($datas)) return '';
		$tmp='<table class="table table-striped table-'.$theme.' table-bordered" style="width:100%">';
		foreach($datas as $key=>$val){
			if ($start)$start_name=$key;
			$tmp.='<tr><td'
						.(!is_array($val)?
							' colspan="'.count($datas).'" class=""'
						:
							($start?' class="bg-warning"':' class="bg-warning"')
						)
						.' style="'
						.(is_array($val)?
							'text-weight:bold;font-size:1.5rem;'
						:'')
						.'text-align:center;vertical-align:middle">'
						.(!is_array($val)?
							'<div class="btn-group btn-group-justified" role="group" title="'.(!empty($parent)?$parent.'/'.$key:$key).'">'
							.'<div class="btn-group" role="group">'
							.'<button type="submit" class="btn btn-warning fab fa-json" >  '
						:'')
						.$key
						.(!is_array($val)?
							'</button>'
							// .'<button type="submit" class="btn btn-danger fas fa-trash" '
							// 							.'onclick="if (confirm(\'Êtes-vous sure de vouloir supprimer cette interco '.$start_name.' ?\')) {document.getElementById(\'delete-interco\').value=\''.$start_name.'/'.(!empty($parent)?$parent.'/'.$key:$key).'\';'
							// 							.'document.getElementById(\'form-delete-interco\').submit();}"'
							// .'></button>'
							.'</div></div>'
						:'')
						.'</td>'
						.(is_array($val)?
							'<td>'.self::diagram($val, $theme, false, (!empty($parent)?$parent.'/'.$key:$key), $start_name).'</td>'
						:'')
						.'</tr>';
		}
		return $tmp.'</table>';
	}
	
	/**
	 * afficher les alertes bootstrap.
	 * 
	 * @return string
	 */
	private function __alert($ms=5000){
		$result='';
		if ($this->session->get('alert-success')) {
			$result.=$this->html->alert('success', $this->session->get('alert-success'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-success', false);
		}
		if ($this->session->get('alert-error')) {
			$result.=$this->html->alert('danger', $this->session->get('alert-error'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-error', false);
		}
		if ($this->session->get('alert-warning')) {
			$result.=$this->html->alert('warning', $this->session->get('alert-warning'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-warning', false);
		}
		return $result;
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->html->head(
					$this->payload('datas/output/head')
				), 
				$this->html->body(
					$this->payload('datas/output/body'), 
					'dark'
				),
				false
			)
		);
	}

}

?>

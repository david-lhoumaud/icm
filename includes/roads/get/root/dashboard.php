<?php
/**
 * Classe de routage GET de la page de login.
 * 
 * @return stdClass
 */
class GetRootDashboard extends Controller {
	
	/** 
	 * Permission du routage 
	 * @var array|string $this->for 
	 */
	protected $for=['root'];
	
	/** 
	 * Classe Bootstrap pour la sortie en HTML
	 * @var string $this->html
	 */
	private $html;
	
	/**
	 * Initialise la permission de la page avec $this->for
	 * @return void
	 */
	public function __construct(){
		$this->html=Helpers::load('bootstrap');
		$this->js=Helpers::load('js');
		$this->menu=Views::load('root/menu', $this->html);
		$this->footer=Views::load('root/footer', $this->html);
	}
	
	/**
	 * Initialisation de la route.
	 * 
	 * @return void
	 */
	public function __init(){
		$this->session->set('error-location','dashboard');
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->payload('datas/output/head', 
			$this->html->title('Dashboard')
			.$this->html->load()
		);
		$this->session->access(true);
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		$content=self::__alert()
			.$this->html->container(
				$this->menu->managers('dashboard')
				.$this->html->p(
					$this->html->img(
						'../includes/statics/imgs/ICM.png',
						array(
							'style'	=>'width:100px;',
							'alt'		=>'ICM Framework'
						)
					), 
					array('class'=>'h1 text-light text-center', 'style'=>'margin-top:5%')
				)
				.$this->html->p('ICM Framework', array('class'=>'h6 text-light text-center', 'style'=>'margin-bottom:5%'))
				.self::card(
					'Créer un routage', 
					'direction', 
					$this->html->form(
						'POST', '',
						$this->html->input(
								'hidden',
								'action-'.$this->session->access(),
								'create_routing_class'
						)
						.$this->html->formGroup(
							$this->html->select(
								'method-'.$this->session->access(),
								array(
									['Routage GET', 		'get'],
									['Routage POST', 		'post'],
									['Routage PUT', 		'put'],
									['Routage DELETE', 	'delete']
								),
								array(
									'id'		=>'method',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'inchtml-'.$this->session->access(),
								array(
									['Aucune mise en forme', 	'none'],
									['Inclure Html', 					'html'],
									['Inclure Bootstrap', 		'bootstrap']
								),
								array(
									'id'		=>'inchtml',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'inchjs-'.$this->session->access(),
								array(
									['Ne pas inclure Javascript', 	'none'],
									['Inclure Javascript', 					'js']
								),
								array(
									'id'		=>'inchjs',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'incview-'.$this->session->access(),
								$this->html->option('none', 'Aucune classe de vue global')
								.self::options_file_view(),
								array(
									'id'		=>'view',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'for-'.$this->session->access(),
								self::options_users(),
								array(
									'id'		=>'for-road',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->input(
								'text',
								'path-'.$this->session->access(),
								'',
								array(
									'class'				=>"form-control",
									'placeholder'	=>"Chemin de la classe",
									'id'					=>"path"
								) 
							)
						)
						.$this->html->p(
							$this->html->submit('primary', 'Créer'),
							array('class'=>'text-light text-center')
						)
					)
				)
				.self::card('Créer un mappage', 'maps', 
					$this->html->form(
						'POST', '',
						$this->html->input(
								'hidden',
								'action-'.$this->session->access(),
								'create_mapping_class'
						)
						.$this->html->formGroup(
							$this->html->select(
								'type-'.$this->session->access(),
								array(
									['Mappage Input', 		'input'],
									['Mappage Process', 	'process'],
									['Mappage Output', 		'output']
								),
								array(
									'id'		=>'type',
									'class'	=>'form-control',
									'onchange'	=> 
										$this->js->display('informat', 'none','=','',true)
										.$this->js->value('informat', 'none','=','',true)
										.$this->js->display('outformat', 'none','=','',true)
										.$this->js->value('outformat', 'payload','=','',true)
										.$this->js->if("this.value","==","'input'",
											$this->js->display('informat', 'block')
										).$this->js->elif("this.value","==","'output'",
											$this->js->display('outformat', 'block')
										)
								)
							).$this->html->p()
							.$this->html->select(
								'format-'.$this->session->access(),
								array(
									['Données Json', 							'json'],
									['Données Dat', 							'dat'],
									['Données Xml', 							'xml'],
									['Données Csv ";"', 					'csv_pv'],
									['Données Csv ","', 					'csv_v'],
									['Données Csv "Tabulation"', 	'csv_tab'],
									['Données Csv "|"', 					'csv_b'],
									['Données Csv "_"', 					'csv_u'],
									['Données Csv "-"', 					'csv_t']
								),
								array(
									'id'			=>'format',
									'class'		=>'form-control',
									'onchange'	=> $this->js->if(
										"this.value!='json' && ", "this.value!='dat' && ", "this.value!='xml'",
										$this->js->display('enclosure', 'block')
									).$this->js->else(
										$this->js->display('enclosure', 'none')
									)
								)
							).$this->html->p()
							.$this->html->select(
								'enclosure-'.$this->session->access(),
								array(
									['Encadrement des colonnes CSV avec &quot;&nbsp;&quot; (guillemets)', 	'&quot;'],
									['Encadrement des colonnes CSV avec &apos;&nbsp;&apos; (apostrophes)', 	'&apos;'],
									['Aucun caractère d&apos;encadrement', 																	'']
								),
								array(
									'id'		=>'enclosure',
									'style'	=>'display:none;',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'informat-'.$this->session->access(),
								array(
									['Aucun flux d\'entrée', 												'none'],
									['Flux POST "icmraws"', 												'raws'],
									['Flux fichier défini par l\'entête "icmif"', 	'file'],
									['Flux URL défini par l\'entête "icmif"', 			'url']
								),
								array(
									'id'		=>'informat',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'outformat-'.$this->session->access(),
								array(
									['Sortie payload', 																																														'payload'],
									['Sortie fichier defini par l\'entête "icmof"(ex: files/input or output/public or guest or root/filename)"', 	'file']
								),
								array(
									'id'		=>'outformat',
									'class'	=>'form-control',
									'style'	=>'display:none;'
								)
							).$this->html->p()
							.$this->html->select(
								'file_match-'.$this->session->access(),
								$this->html->option('', 'Aucun tableau de matching')
								.self::options_file_match(),
								array(
									'id'		=>'file_match',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'for-'.$this->session->access(),
								self::options_users(),
								array(
									'id'		=>'for-map',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->input(
								'text',
								'path-'.$this->session->access(),
								'',
								array(
									'class'				=>"form-control",
									'placeholder'	=>"Chemin de la classe",
									'id'					=>"path"
								) 
							)
						)
						.$this->html->p(
							$this->html->submit('primary', 'Créer'),
							array('class'=>'text-light text-center')
						)
					)
				)
				.self::card(
					'Créer un tableau de matching', 
					'matchs', 
					$this->html->form(
						'POST','',
						$this->html->input(
								'hidden',
								'action-'.$this->session->access(),
								'create_matching_file'
						)
						.$this->html->formGroup(
							$this->html->tag('label', 'Import à générer', array('for'=>'import', 'class'=>'form-control'))
							.$this->html->input(
								'file',
								'import-'.$this->session->access(),
								'',
								array(
									'id'		=>'import',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'format-import-'.$this->session->access(),
								array(
									['Format JSON', 						'json'],
									['Format Csv ";"', 					';'],
									['Format Csv ","', 					','],
									['Format Csv "|"', 					'|'],
									['Format Csv "@"', 					'@'],
									['Format Csv "_"', 					'_'],
									['Format Csv "-"', 					'-'],
									['Format Csv Espace', 			' '],
									['Format Csv Tabulation', 	'	']
								),
								array(
									'id'		=>'format-import',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->select(
								'dest-match-'.$this->session->access(),
								array(
									['From', 'from'],
									['To', 'To']
								),
								array(
									'id'		=>'dest-match',
									'class'	=>'form-control'
								)
							).$this->html->p()
							.$this->html->input(
								'text',
								'path-'.$this->session->access(),
								'',
								array(
									'class'				=>"form-control",
									'placeholder'	=>"Chemin du matching",
									'id'					=>"path"
								) 
							)
						)
						.$this->html->p(
							$this->html->submit('primary', 'Ouvrir'),
							array('class'=>'text-light text-center')
						),
						array('enctype'=>'multipart/form-data')
					)
				)
				.self::card(
					'Editer un tableau de matching', 
					'matchs-edit', 
					$this->html->form(
						'POST','',
						$this->html->input(
								'hidden',
								'action-'.$this->session->access(),
								'edit_matching_file'
						)
						.$this->html->formGroup(
							$this->html->select(
								'file_match-'.$this->session->access(),
								self::options_file_match(),
								array(
									'id'		=>'file_match',
									'class'	=>'form-control'
								)
							)
						)
						.$this->html->p(
							$this->html->submit('primary', 'Ouvrir'),
							array('class'=>'text-light text-center')
						)
					)
				)
		)
		.$this->footer->full();
		$this->payload('datas/output/body', $content);
	}
	
	private function __alert($ms=5000){
		$result='';
		if ($this->session->get('alert-success')) {
			$result.=$this->html->alert('success', $this->session->get('alert-success'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-success', false);
		}
		if ($this->session->get('alert-error')) {
			$result.=$this->html->alert('danger', $this->session->get('alert-error'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-error', false);
		}
		if ($this->session->get('alert-warning')) {
			$result.=$this->html->alert('warning', $this->session->get('alert-warning'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-warning', false);
		}
		return $result;
	}
	
	private function options_file_match(){
		$result='';
		foreach($this->ls('includes/matchs') as $file){
			if (preg_match('/_schema_/',$file) || !preg_match('/\.json$/',$file)) continue;
			$result.=$this->html->option(
				$file, 
				str_replace(['includes/matchs/', '.json'],'',$file)
			);
		}
		return $result;
	}
	
	private function options_file_view(){
		$result='';
		foreach($this->ls('includes/views') as $file){
			if (!preg_match('/\.php$/',$file)) continue;
			$result.=$this->html->option(
				str_replace(['includes/views/', '.php'],'',$file), 
				str_replace(['includes/views/', '.php'],'',$file)
			);
		}
		return $result;
	}
	
	private function options_users(){
		$result='';
		foreach($this->getReferencedUser() as $usr){
			$result.=$this->html->option($usr, 'Permission "'.$usr.'"');
		}
		$result.=$this->html->option('public', 'Accès publique');
		return $result;
	}
	
	private function card($title, $img='check-mark', $content='') {
		return $this->html->div(
			$this->html->div(
				$this->html->img(
					'../includes/statics/imgs/icons/card/'.$img.'.png',
					array(
						'style'	=>'width:100px;',
						'alt'		=>'title'
					)
				)
				.$this->html->div(
					$this->html->tag('h5',$title, array('class'=>'card-title text-center')).$content,
					array('class'=>'card-body')
				),
				array('class'=>'text-center')
			),
			array(
				'class'=>'card theme-dark',
				'style'=>'width:46%;margin:2%;display:inline-flex'
			)
		);
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->html->head(
					$this->payload('datas/output/head')
				), 
				$this->html->body(
					$this->payload('datas/output/body'), 
					'dark'
				),
				false
			)
		);
	}

}

?>

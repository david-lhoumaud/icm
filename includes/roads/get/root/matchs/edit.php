<?php
/**
 * Classe de routage GET GetRootMatchsEdit.
 * 
 * @return stdClass
 */
class GetRootMatchsEdit extends Controller {
	
	/** 
		 * Permission du routage.
		 * root, public, ...
		 * @var array|string $for 
		 */
	protected $for='root';
	
	/** 
	 * Classe Bootstrap pour la sortie en HTML
	 * @var string $this->html
	 */
	private $html;
	
	/**
	 * Initialise la permission de la page avec $this->for
	 * @return void
	 */
	public function __construct(){
		$this->html=Helpers::load('bootstrap');
		$this->js=Helpers::load('js');
		$this->menu=Views::load('root/menu', $this->html);
		
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->session->set('error-location','edit');
		$this->payload('datas/output/head',
			$this->html->title('ICM - Matchs edit')
			.$this->html->load()
		);
		$this->session->access(true);
		$this->payload('datas/input', $this->json($this->session->get('file_match')));
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		$count=count($this->payload('datas/input'));
		$result='<p class="h1 text-center text-light">Modification des correspondances</p>
		<div class="row justify-content-center">'
		.'<div style="position:fixed;top:1%;left:1%;width:98%;">'
		.'<form class="form-group" method="post" id="form-add-col-match" style="display:inline-block;float:left;">'
	.'<div class="btn-group btn-group-justified" role="group" >'
		.'<div class="btn-group" role="group">'
		.'<input type="hidden" name="action-'.$this->session->access().'" value="add-col-match">'
		.'<input type="hidden" id="add-scroll" name="scroll-'.$this->session->access().'" value="0">'
		.'<input type="number" min="1" max="'.($count+1).'" style="border-radius:5px 0px 0px 5px!important;border:none!important;" class="form-control rounded-none" size="3" id="add-position" name="position-'.$this->session->access().'" value="'.($count+1).'">'
		.'<input type="text" style="border-radius:0px!important;border:none!important;" class="form-control rounded-none" placeholder="Colonne d\'origine" id="add-origin" name="origin-'.$this->session->access().'" value="">'
		.'<input type="text" style="border-radius:0px!important;border:none!important;" class="form-control rounded-none" placeholder="Colonne de destination" id="add-dest" name="dest-'.$this->session->access().'" value="">'
		.$this->html->submit('success fas fa-plus-circle', '')
	.'</div></div></form>'
	.$this->menu->group('', '../', 'float:right')
	.'</div>'
	.'<p class="text-light">'.str_replace(['includes/matchs/', '.json'],'',$this->session->get('file_match')).'</p><br>'
	.'<div class="table-responsive">
	<table class="table table-dark table-striped table-hover">
		<thead>
			<tr >
				<th scope="col">Ordre</th>
				<th>Colonne d\'origine</th>
				<th>Colonne de destination</th>
				<th class="text-center">Actions</th>
			</tr>
		</thead>
		<tbody style="padding-top:48px;">';
		$position=0;
		$js='';
		foreach ($this->payload('datas/input') as $origin=>$dest){
			$result.='<tr>'
							.'<th scope="row" ><a name="'.(++$position).'"></a><input type="number" size="3" min="1" max="'.($count).'" class="form-control" id="input_position-'.$position.'" value="'.$position.'"' 
							.$this->js->onchange(
									$this->js->if('this.value>=1 && this.value<='.($count), ' && ', "this.value!='$position'", 
										$this->js->value('cpos', $position, '=', null, true)
										.$this->js->value('current', $origin, '=', null, true)
										.$this->js->value('origin').'='.$this->js->value('input_origin-'.$position, null, '=', null, true)
										.$this->js->value('dest').'='.$this->js->value('input_dest-'.$position, null, '=', null, true)
										.$this->js->value('position').'='.$this->js->value('input_position-'.$position, null, '=', null, true)
										.$this->js->submit('form-edit-match', true)
									)
								)
							.'></th>'
							.'<td><input type="text" class="form-control" id="input_origin-'.$position.'" value="'.$origin.'"></td>'
							.'<td><input type="text" class="form-control" id="input_dest-'.$position.'" value="'.str_replace('"','&quot;',$dest).'"></td>'
							.'<td class="text-center">'
							.$this->html->button('success fas fa-save', '', 
								$this->js->onclick(
									$this->js->value('cpos', $position, '=', null, true)
									.$this->js->value('current', $origin, '=', null, true)
									.$this->js->value('origin').'='.$this->js->value('input_origin-'.$position, null, '=', null, true)
									.$this->js->value('dest').'='.$this->js->value('input_dest-'.$position, null, '=', null, true)
									.$this->js->value('position').'='.$this->js->value('input_position-'.$position, null, '=', null, true)
									.$this->js->submit('form-edit-match', true)
								)
							)
							.'</td>'
							.'</tr>';
			$js.='';
		}
		
		
		$result.='</tbody></table></div></div>';
		$result.='<form method="post" id="form-edit-match" style="display:inline-block">'
							.'<input type="hidden" name="action-'.$this->session->access().'" value="edit-col-match">'
							.'<input type="hidden" id="scroll" name="scroll-'.$this->session->access().'" value="0">'
							.'<input type="hidden" id="cpos" name="cpos-'.$this->session->access().'" value="">'
							.'<input type="hidden" id="position" name="position-'.$this->session->access().'" value="'.$position.'">'
							.'<input type="hidden" id="current" name="current-'.$this->session->access().'" value="">'
							.'<input type="hidden" id="origin" name="origin-'.$this->session->access().'" value="">'
							.'<input type="hidden" id="dest" name="dest-'.$this->session->access().'" value="">'
							.'</form>';
		$result.=$this->html->js('window.addEventListener("scroll", (event) => {
			document.getElementById("scroll").value=this.scrollY;
			document.getElementById("add-scroll").value=this.scrollY;
		});
		window.scrollTo(0,'.($_SESSION['scroll-y']??0).');');
		if (isset($_SESSION['scroll-y']))unset($_SESSION['scroll-y']);
		$this->payload(
			'datas/output/body', 
			$this->html->container(self::__alert().$result)
		);
	}
	
	/**
	 * afficher les alertes bootstrap.
	 * 
	 * @return string
	 */
	private function __alert($ms=5000){
		$result='';
		if ($this->session->get('alert-success')) {
			$result.=$this->html->alert('success', $this->session->get('alert-success'), 'style="z-index:10000;position:fixed;width:25%;top:1%;left:38.5%;"', $ms);
			$this->session->set('alert-success', false);
		}
		if ($this->session->get('alert-error')) {
			$result.=$this->html->alert('danger', $this->session->get('alert-error'), 'style="z-index:10000;position:fixed;width:25%;top:1%;left:38.5%;"', $ms);
			$this->session->set('alert-error', false);
		}
		if ($this->session->get('alert-warning')) {
			$result.=$this->html->alert('warning', $this->session->get('alert-warning'), 'style="z-index:10000;position:fixed;width:25%;top:1%;left:38.5%;"', $ms);
			$this->session->set('alert-warning', false);
		}
		return $result;
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->html->head(
					$this->payload('datas/output/head')
				), 
				$this->html->body(
					$this->payload('datas/output/body'), 
					'dark'
				),
				false
			)
		);
	}

}

?>

<?php
/**
 * Classe de routage GET GetRootRoadsManager.
 * 
 * @return stdClass
 */
class GetRootMatchsManager extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	public function __construct(){
		$this->html=Helpers::load('bootstrap');
		$this->menu=Views::load('root/menu', $this->html);
	}
	
	/**
	 * Initialisation de la route.
	 * 
	 * @return void
	 */
	public function __init(){
		$this->session->set('error-location','manager');
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->payload('datas/output/head',
			$this->html->title('ICM - Matchs')
			.$this->html->load()
		);
		$this->session->access(true);
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		self::roads_list();
		$content=self::__alert()
						.$this->html->container(
							'</p><p><p class="h1 text-center text-light" >ICM - Matchs</p></p><p>'
							.$this->menu->managers('matchs', '../')
							.self::diagram($this->payload('datas/process'))
							.'<form method="post" action="../dashboard" id="form-edit">
							<input type="hidden" name="action-'.$this->session->access().'" value="edit_matching_file">
							<input type="hidden" id="file-match-edit" name="file_match-'.$this->session->access().'" value=""></form>'
							.'<form method="post" id="form-delete-match">
							<input type="hidden" name="action-'.$this->session->access().'" value="delete-match">
							<input type="hidden" id="delete-match" name="delete-match-'.$this->session->access().'" value=""></form>'
						);
		$this->payload('datas/output/body', $content);
	}
	
	/**
	 * Récupère l'architecture des fichiers de matching.
	 * 
	 * @return void
	 */
	private function roads_list(){
		$this->payload['datas']['process']=array(
			'from'=>array(),
			'to'=>array(),
			'SCHEMA'=>array(),
		);
		foreach($this->ls('includes/matchs') as $file){
			if (!preg_match('/\.json$/',$file)) continue;
			$path=str_replace(['includes/matchs/', '.json'],'',$file);
			$p=explode('/', $path);
			$o='';
			foreach ($p as $index => $value) {
				$o.='/'.$value;
				if (!$this->payload_exists('datas/process'.$o)){
					if ($index==count($p)-1) $this->payload('datas/process'.$o, true);
					else $this->payload('datas/process'.$o, array());
				}
			}
		}
	}
	
	/**
	 * Génère un tableau html pour afficher l'arborescence des données.
	 * 
	 * @return string retourne le tableau HTML
	 */
	private function diagram($datas, $theme='dark', $start=true, $parent=''){
		if (!is_array($datas)) return '';
		$tmp='<table class="table table-striped table-'.$theme.' table-bordered" style="width:100%">';
		foreach($datas as $key=>$val){
			$tmp.='<tr><td'
						.(!is_array($val)?
							' colspan="'.count($datas).'" class=""'
						:
							($start?
								' class="bg-success"'
							:
								' class="bg-warning"'
							)
						)
						.' style="'
						.(is_array($val)?
							'text-weight:bold;font-size:1.5rem;'
						:'')
						.'text-align:center;vertical-align:middle">'
						.(!is_array($val)?
							'<div class="btn-group btn-group-justified" role="group" title="'.$parent.'/'.$key.'">'
							.'<div class="btn-group" role="group">'
							.'<button type="submit" class="btn btn-success fas fa-cog" '
											.'onclick="document.getElementById(\'file-match-edit\').value=\'includes/matchs/'.$parent.'/'.$key.'.json\';document.getElementById(\'form-edit\').submit()"'
							.'">&nbsp;'
						:'')
						.$key
						.(!is_array($val)?
							'</button>'
							.'<button type="submit" class="btn btn-danger fas fa-trash" '
								.'onclick="if (confirm(\'Êtes-vous sure de vouloir supprimer ce matching?\')) {document.getElementById(\'delete-match\').value=\''.(!empty($parent)?$parent.'/'.$key:$key).'\';'
													.'document.getElementById(\'form-delete-match\').submit();}"'
							.'></button>'
							.'</div></div>'
						:'')
						.'</td>'
						.(is_array($val)?
							'<td>'.self::diagram($val, $theme, false, (!empty($parent)?$parent.'/'.$key:$key)).'</td>'
						:'')
						.'</tr>';
		}
		return $tmp.'</table>';
	}
	
	/**
	 * afficher les alertes bootstrap.
	 * 
	 * @return string
	 */
	private function __alert($ms=5000){
		$result='';
		if ($this->session->get('alert-success')) {
			$result.=$this->html->alert('success', $this->session->get('alert-success'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-success', false);
		}
		if ($this->session->get('alert-error')) {
			$result.=$this->html->alert('danger', $this->session->get('alert-error'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-error', false);
		}
		if ($this->session->get('alert-warning')) {
			$result.=$this->html->alert('warning', $this->session->get('alert-warning'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-warning', false);
		}
		return $result;
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->html->head(
					$this->payload('datas/output/head')
				), 
				$this->html->body(
					$this->payload('datas/output/body'), 
					'dark'
				),
				false
			)
		);
	}

}

?>

<?php
/**
 * Classe de routage GET de la page de login.
 * 
 * @return stdClass
 */
class GetRootLogin extends Controller {
	
	/** 
	 * Permission du routage 
	 * @var string $this->for 
	 */
	protected $for='public';
	
	/** 
	 * Classe Bootstrap pour la sortie en HTML
	 * @var string $this->html
	 */
	private $html;
	
	/**
	 * Initialise la permission de la page avec $this->for
	 * @return void
	 */
	public function __construct(){
		$this->html=Helpers::load('bootstrap');
		$this->footer=Views::load('root/footer', $this->html);
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$tmp=$this->html->title('Identification')
				.$this->html->load();
		$this->payload('datas/output/head', $tmp);
		$this->session->access(true);
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		if ($this->session->exists('error')) {
			switch($this->session->get('error/code')) {
				case 401: case 403: $alert=$this->html->alert('danger', 'Echec de l\'authentification', '', 5000); break;
				default: $alert=$this->html->alert('danger', $this->session->get('error/message'), '', 5000);
			}
			$this->session->unset('error');
		}
		$result=$this->html->container('<div class="row justify-content-center" style="margin-top:10%;">
		<div class="jumbotron col-4 ">
			<p class="h3 text-center">
			'.$this->html->img(
						'../includes/statics/imgs/ICM.png',
						array(
							'style'	=>'width:100px;',
							'alt'		=>'ICM Framework'
						)
					).$this->html->br().'
			Identifiez-vous</p><p></p>
			<form method="post">
				<div class="form-group">
					<label for="usr">Utilisateur</label>
					<input type="text" class="form-control" id="usr" name="usr-'.$this->session->access().'">
				</div>
				<div class="form-group">
					<label for="pwd">Mot de passe</label>
					<input type="password" class="form-control" id="pwd" name="pwd-'.$this->session->access().'">
				</div>
				<p class="text-light text-center"><button type="submit" class="btn btn-primary">Connexion</button></p>
			</form>
		</div>
	</div>'
		)
		.$this->footer->mini();
		$this->payload('datas/output/body', ($alert??'').$result);
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->html->head(
					$this->payload('datas/output/head')
				), 
				$this->html->body(
					$this->payload('datas/output/body'), 
					'dark'
				),
				false
			)
		);
	}

}

?>

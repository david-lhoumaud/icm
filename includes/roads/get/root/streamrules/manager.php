<?php
/**
 * Classe de routage GET GetRootStreamrulesManager.
 * 
 * @return stdClass
 */
class GetRootStreamrulesManager extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	public function __construct(){
		$this->html=Helpers::load('bootstrap');
		$this->js=Helpers::load('js');
		$this->menu=Views::load('root/menu', $this->html);
	}
	
	/**
	 * Initialisation de la route.
	 * 
	 * @return void
	 */
	public function __init(){
		$this->session->set('error-location','manager');
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->payload('datas/output/head',
			$this->html->title('ICM - Stream Rules')
			.$this->html->load()
		);
		$this->session->access(true);
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		$content=self::__alert()
						.$this->html->container(
							'</p><p><p class="h1 text-center text-light" >ICM - Stream Rules</p></p><p>'
							.$this->menu->managers('streamrules', '../')
							.'<form method="post" id="form-delete-streamrules" enctype="multipart/form-data">'
							.$this->html->formGroup(
								$this->html->div(
									$this->html->tag('label', '1er CSV', array('for'=>'csv1', 'class'=>'form-control  col-2'))
									.$this->html->input(
										'file',
										'csv1-'.$this->session->access(),
										'',
										array(
											'id'		=>'csv1',
											'class'	=>'form-control'
										)
									),
									'class="input-group" role="group"'
								).$this->html->p()
								.$this->html->div(
									$this->html->tag('label', 'Séparateur', array('id'=>'sep-csv1-label', 'for'=>'sep-csv1', 'class'=>'form-control col-2'))
									.$this->html->select(
										'sep-csv1-'.$this->session->access(),
										array(
											['Point virgule ";"', 					';'],
											['Virgule ","', 					','],
											['Barre vertical "|"', 					'|'],
											['Arobase "@"', 					'@'],
											['Underscore "_"', 					'_'],
											['Tiret "-"', 					'-'],
											['Espace', 			' '],
											['Tabulation', 	'	']
										),
										array(
											'id'		=>'sep-csv1',
											'class'	=>'form-control col-3'
										)
									)
									,'class="input-group" role="group"'
								).$this->html->p()
								.$this->html->div(
									$this->html->tag('label', 'Action', array('id'=>'action-label', 'for'=>'action', 'class'=>'form-control col-2'))
									.$this->html->select(
										'action-'.$this->session->access(),
										array(
											['Fusionner 2 CSV', 	'match-column'],
											['Remplacer', 				'replace-all'],
										),
										array(
											'id'		=>'action',
											'class'	=>'form-control col-3',
											'onchange'=>'action_import(this)'
										)
									)
									,'class="input-group" role="group"'
								).$this->html->p()
								.$this->html->div(
									$this->html->div(
										$this->html->tag('label', '2eme CSV', array('id'=>'csv2-label', 'for'=>'csv2', 'class'=>'form-control col-2'))
										.$this->html->input(
											'file',
											'csv2-'.$this->session->access(),
											'',
											array(
												'id'		=>'csv2',
												'class'	=>'form-control'
											)
										),
										'class="input-group" role="group"'
									).$this->html->p()
									.$this->html->div(
									$this->html->tag('label', 'Séparateur', array('id'=>'sep-csv2-label', 'for'=>'sep-csv2', 'class'=>'form-control col-2'))
									.$this->html->select(
										'sep-csv2-'.$this->session->access(),
										array(
											['Point virgule ";"', 					';'],
											['Virgule ","', 					','],
											['Barre vertical "|"', 					'|'],
											['Arobase "@"', 					'@'],
											['Underscore "_"', 					'_'],
											['Tiret "-"', 					'-'],
											['Espace', 			' '],
											['Tabulation', 	'	']
										),
										array(
											'id'		=>'sep-csv2',
											'class'	=>'form-control col-3'
										)
									)
									,'class="input-group" role="group"'
									).$this->html->p()
									.$this->html->tag('label', 'Correspondances des colonnes (numéro des colonnes)', array('class'=>'form-control'))
									.$this->html->div(
										$this->html->tag('label', 'Colonne 1er CSV', array('id'=>'match-col-csv1-label', 'for'=>'match-col-csv1', 'class'=>'form-control col-2'))
										.$this->html->input(
											'text',
											'match-col-csv1-'.$this->session->access(),
											'1',
											array(
												'id'		=>'match-col-csv1',
												'class'	=>'form-control'
											)
										)
										.$this->html->tag('label', 'Colonne 2er CSV', array('id'=>'match-col-csv2-label', 'for'=>'match-col-csv2', 'class'=>'form-control col-2'))
										.$this->html->input(
											'text',
											'match-col-csv2-'.$this->session->access(),
											'1',
											array(
												'id'		=>'match-col-csv2',
												'class'	=>'form-control'
											)
										)
										,'class="input-group" role="group"'
									).$this->html->p()
									.$this->html->tag('label', 'Récupération des colonnes ( num colonne=nouvelle position ex(1=1,2=3,4=2) )', array('class'=>'form-control'))
									.$this->html->div(
										$this->html->tag('label', 'Colonnes 1er CSV', array('id'=>'get-cols-csv1-label', 'for'=>'get-cols-csv1', 'class'=>'form-control col-2'))
										.$this->html->input(
											'text',
											'get-cols-csv1-'.$this->session->access(),
											'1=1',
											array(
												'id'		=>'get-cols-csv1',
												'class'	=>'form-control'
											)
										)
										.$this->html->tag('label', 'Colonnes 2er CSV', array('id'=>'get-cols-csv2-label', 'for'=>'get-cols-csv2', 'class'=>'form-control col-2'))
										.$this->html->input(
											'text',
											'get-cols-csv2-'.$this->session->access(),
											'1=2',
											array(
												'id'		=>'get-cols-csv2',
												'class'	=>'form-control'
											)
										)
										,'class="input-group" role="group"'
									).$this->html->p()
									,'id="match-column-div"'
								)
								.$this->html->div(
									$this->html->div(
										$this->html->tag('label', 'Remplacer', array('for'=>'str1', 'class'=>'form-control col-1'))
										.$this->html->input(
											'text',
											'str1-'.$this->session->access(),
											'',
											array(
												'id'		=>'str1',
												'class'	=>'form-control'
											)
										)
										,'class="input-group" role="group"'
									)
									.$this->html->div(
										$this->html->tag('label', 'Par', array('for'=>'str2', 'class'=>'form-control col-1'))
										.$this->html->input(
											'text',
											'str2-'.$this->session->access(),
											'',
											array(
												'id'		=>'str2',
												'class'	=>'form-control'
											)
										)
										,'class="input-group" role="group"'
									)
									,'id="replace-div" style="display:none;"'
								)
							)
							.'<input type="hidden" id="scroll" name="scroll-'.$this->session->access().'" value="0">
							<input type="submit" class="btn btn-success" value="Générer"></form>'
							.self::scriptJS()
						);
		$this->payload('datas/output/body', $content);
	}
	
	private function scriptJS() {
		return $this->html->js(
			$this->js->function('action_import','item',
				$this->js->display('match-column-div', 'none').';'
				.$this->js->display('replace-div', 'none').';'
				.$this->js->if('item.value', '==', '"match-column"',
					$this->js->display('match-column-div', 'block').';'
				)
				.$this->js->elif('item.value', '==', '"replace-all"',
					$this->js->display('replace-div', 'block').';'
				)
			)
		);
	}
	
	
	/**
	 * afficher les alertes bootstrap.
	 * 
	 * @return string
	 */
	private function __alert($ms=5000){
		$result='';
		if ($this->session->get('alert-success')) {
			$result.=$this->html->alert('success', $this->session->get('alert-success'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-success', false);
		}
		if ($this->session->get('alert-error')) {
			$result.=$this->html->alert('danger', $this->session->get('alert-error'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-error', false);
		}
		if ($this->session->get('alert-warning')) {
			$result.=$this->html->alert('warning', $this->session->get('alert-warning'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-warning', false);
		}
		return $result;
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->html->head(
					$this->payload('datas/output/head')
				), 
				$this->html->body(
					$this->payload('datas/output/body'), 
					'dark'
				),
				false
			)
		);
	}

}

?>

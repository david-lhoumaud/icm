<?php
/**
 * Classe de routage GET GetRootRoadsManager.
 * 
 * @return stdClass
 */
class GetRootMapsManager extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	public function __construct(){
		$this->html=Helpers::load('bootstrap');
		$this->menu=Views::load('root/menu', $this->html);
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->session->set('error-location','manager');
		$this->payload('datas/output/head',
			$this->html->title('ICM - Maps')
			.$this->html->load()
		);
		$this->session->access(true);
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		if ($this->session->exists('error')) {
			switch($this->session->get('error/code')) {
				case 401: case 403: $alert=$this->html->alert('danger', 'Echec de l\'authentification', '', 5000); break;
				default: $alert=$this->html->alert('danger', $this->session->get('error/message'), '', 5000);
			}
			$this->session->unset('error');
		}
		self::roads_list();
		$content=self::__alert()
						.$this->html->container(
							'</p><p><p class="h1 text-center text-light" >ICM - Maps</p></p><p>'
							.$this->menu->managers('maps', '../')
							.self::diagram($this->payload('datas/process'))
							.'<form method="post" action="../dashboard" id="form-edit">
							<input type="hidden" name="action-'.$this->session->access().'" value="edit_matching_file">
							<input type="hidden" id="file-match-edit" name="file_match-'.$this->session->access().'" value=""></form>'
							.'<form method="post" id="form-update-match">
							<input type="hidden" name="action-'.$this->session->access().'" value="update-match">
							<input type="hidden" id="select-map" name="select-map-'.$this->session->access().'" value="">
							<input type="hidden" id="old-path-match" name="old-path-match-'.$this->session->access().'" value="">
							<input type="hidden" id="new-path-match" name="new-path-match-'.$this->session->access().'" value=""></form>'
							.'<form method="post" id="form-delete-map">
							<input type="hidden" name="action-'.$this->session->access().'" value="delete-map">
							<input type="hidden" id="delete-map" name="delete-map-'.$this->session->access().'" value=""></form>'
							.'<form method="post" id="form-launch-map">
							<input type="hidden" name="action-'.$this->session->access().'" value="launch-map">
							<input type="hidden" id="launch-map" name="launch-map-'.$this->session->access().'" value=""></form>'
						);
		$this->payload('datas/output/body', ($alert??'').$content);
	}
	
	/**
	 * Récupère l'architecture des scripts de mappage.
	 * 
	 * @return void
	 */
	private function roads_list(){
		$this->payload['datas']['process']=array(
			'input'=>array(),
			'process'=>array(),
			'output'=>array(),
		);
		foreach($this->ls('includes/maps') as $file){
			if (!preg_match('/\.php$/',$file)) continue;
			$path=str_replace(['includes/maps/', '.php'],'',$file);
			$p=explode('/', $path);
			$o='';
			foreach ($p as $index => $value) {
				$o.='/'.$value;
				if (!$this->payload_exists('datas/process'.$o)){
					if ($index==count($p)-1) $this->payload('datas/process'.$o, true);
					else $this->payload('datas/process'.$o, array());
				}
			}
		}
		//ksort($this->payload['datas']['process']);
	}
	
	
	/**
	 * Génère un tableau html pour afficher l'arborescence des données.
	 * 
	 * @return string retourne le tableau HTML
	 */
	private function diagram($datas, $theme='light', $start=true, $parent=''){
		if (!is_array($datas)) return '';
		$tmp='<table class="table table-striped table-'.$theme.' table-bordered" style="width:100%">';
		foreach($datas as $key=>$val){
			$has_map=false;
			$error_map=false;
			$map_name='';
			$map_path='';
			if (!is_array($val)){
				$fc=$this->file('includes/maps/'.(!empty($parent)?$parent.'/'.$key:$key).'.php');
				if (preg_match('/\$this\-\>mapping\(.*\)/', $fc)){
					$has_map=true;
					$map_name=str_replace(array('"',"'"), '', $this->Parsing->between('$this->mapping(', ');', $fc));
					$map_path='includes/matchs/'.$map_name.'.json';
					if (!file_exists($map_path)) $error_map=true;
				}
			}
			$id_hash=sha1($parent.'/'.$key);
			switch ( preg_replace('/^([a-z]+)\/.*/', '\1', $parent) ) {
				case 'input':
					$prefix='I'.preg_replace('/^[a-z]+\/(.*)/', '\1', $parent);
				break;
				case 'process':
					$prefix='P'.preg_replace('/^[a-z]+\/(.*)/', '\1', $parent);
				break;
				case 'output':
					$prefix='O'.preg_replace('/^[a-z]+\/(.*)/', '\1', $parent);
				break;
				default:
					$prefix='';
				break;
			}
			$tmp.='<tr><td'
						.(!is_array($val)?
							' colspan="'.count($datas).'" class=""'
						:
							($start?' class="bg-info"':' class="bg-warning"')
						)
						.' style="'
						.(is_array($val)?
							'text-weight:bold;font-size:1.5rem;'
						:'')
						.'text-align:center;vertical-align:middle">'
						.(!is_array($val)?
							'<div class="btn-group btn-group-justified" role="group" title="icm -'.$prefix.'/'.$key.'" >'
							.'<div class="btn-group" role="group">'
							.'<button type="submit" class="btn btn-info fas fa-map-marker-alt" style="color:white!important;cursor:unset;" '
								.'onclick="if (confirm(\'Êtes-vous sure de vouloir lancer cette map?\')) {document.getElementById(\'launch-map\').value=\''.$prefix.':'.$key.'\';'.'document.getElementById(\'form-launch-map\').submit();}"'
							.'>  '
						:'')
						.$key
						.(!is_array($val)?'</button>'
							.($has_map?'<button type="submit" class="btn btn-'.($error_map?'danger':'success').' fas fa-clipboard-list" title="'.$map_name.'" '
								.($error_map?'disabled':'onclick="document.getElementById(\'file-match-edit\').value=\''.$map_path.'\';document.getElementById(\'form-edit\').submit()"')
							.'></button>':'')
							.($has_map?
								'<select id="path-match-'.$id_hash.'" name="path-match-'.$this->session->access().'" style="font-size:10px;border-radius:0px!important;border:none!important;width:28%;" '
										.'onchange="if (this.value!=\'\'){document.getElementById(\'select-map\').value=\''.(!empty($parent)?$parent.'/'.$key:$key).'\';'
														.'document.getElementById(\'old-path-match\').value=\''.$map_name.'\';'
														.'document.getElementById(\'new-path-match\').value=this.value;'
														.'document.getElementById(\'form-update-match\').submit();}"'
								.'>'
									.'<option value="" selected>'.$map_name.'</option>'
									.self::options_file_match($map_name)
								.'</select>'
							:'')
							.'<button type="submit" class="btn btn-danger fas fa-trash" '
								.'onclick="if (confirm(\'Êtes-vous sure de vouloir supprimer cette map?\')) {document.getElementById(\'delete-map\').value=\''.(!empty($parent)?$parent.'/'.$key:$key).'\';'.'document.getElementById(\'form-delete-map\').submit();}"'
							.'></button>'
							.'</div></div>'
						:'')
						.'</td>'
						.(is_array($val)?
							'<td>'.self::diagram($val, $theme, false, (!empty($parent)?$parent.'/'.$key:$key)).'</td>'
						:'')
						.'</tr>';
		}
		return $tmp.'</table>';
	}
	
	private function options_file_match($map_name){
		$result='';
		foreach($this->ls('includes/matchs') as $file){
			if (preg_match('/_schema_/',$file) || !preg_match('/\.json$/',$file)) continue;
			$f=str_replace(['includes/matchs/', '.json'],'',$file);
			if ($f==$map_name)continue;
			$result.='<option value="'.$f.'">'.$f.'</option>';
		}
		return $result;
	}
	
	/**
	 * Afficher les alertes bootstrap.
	 * 
	 * @return string
	 */
	private function __alert($ms=5000){
		$result='';
		if ($this->session->get('alert-success')) {
			$result.=$this->html->alert('success', $this->session->get('alert-success'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-success', false);
		}
		if ($this->session->get('alert-error')) {
			$result.=$this->html->alert('danger', $this->session->get('alert-error'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-error', false);
		}
		if ($this->session->get('alert-warning')) {
			$result.=$this->html->alert('warning', $this->session->get('alert-warning'), 'style="z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;"', $ms);
			$this->session->set('alert-warning', false);
		}
		return $result;
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->html->head(
					$this->payload('datas/output/head')
				), 
				$this->html->body(
					$this->payload('datas/output/body'), 
					'dark'
				),
				false
			)
		);
	}

}

?>

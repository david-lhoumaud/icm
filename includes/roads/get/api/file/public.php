<?php
/**
 * Classe de routage GET GetApiFilePublic.
 * 
 * @return stdClass
 */
class GetApiFilePublic extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='public';
	
	public function __construct(){}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$fn=$this->payload('client/host').'/files/output/public/'.$this->payload('request/params/get/path');
		$fn=$this->str_securize($fn, array('html', 'accent', 'bom', '..', '0', '%', 'return', 'space', 'ponc'));
		$comment='';
		switch($this->payload('client/ip')) {
			case '109.0.20.9':
			case '86.201.250.51':
			case '80.15.167.197':
				$comment="\tEcolands";
			break;
			case '54.38.41.204':
				$comment="\tecolands-shop.com";
			break;
			case '93.93.44.202':
				$comment="\tCGLI";
			break;
			case '78.120.3.227':
				$comment="\tDavid";
			break;
			case '52.47.34.91':
			case '35.181.33.27':
			case '35.181.72.94':
			case '35.181.127.59':
				$comment="\tManomano";
			break;
			case '51.255.70.99':
				$comment="\tStats";
			break;
			default:
				$comment="\tInconnu";
		}
		$ua=$_SERVER['HTTP_USER_AGENT']??'Inconnu';
		file_put_contents('/home/robot/server/logs/get-file-public.log', date("Y-m-d H:i:s")."\t'".$this->payload('client/ip')."'\t".$fn.$comment."\t(".$ua.")\n", FILE_APPEND);
		if ($this->payload_exists('request/params/get/dl')) $this->Output->contentDispositionFilename($fn);
		// echo $fn; exit;
		if (file_exists($fn)){
			if (preg_match('/\.json$/i', $fn)) 			$this->Output->standard($this->file($fn), 'application/json');
			else if (preg_match('/\.csv$/i', $fn)) 	$this->Output->csv($this->file($fn), $fn);
			else if (preg_match('/\.xml$/i', $fn)) 	$this->Output->standard($this->file($fn), 'application/xml');
			else if (preg_match('/\.bin$/i', $fn)) 	$this->Output->standard($this->file($fn), 'application/octet-stream');
			else if (preg_match('/\.7z$/i', $fn)) 	$this->Output->standard($this->file($fn), 'application/x-7z-compressed');
			else if (preg_match('/\.zip$/i', $fn)) 	$this->Output->standard($this->file($fn), 'application/zip');
			else if (preg_match('/\.bz$/i', $fn)) 	$this->Output->standard($this->file($fn), 'application/x-bzip');
			else if (preg_match('/\.bz2$/i', $fn)) 	$this->Output->standard($this->file($fn), 'application/x-bzip2');
			else if (preg_match('/\.pdf$/i', $fn)) 	$this->Output->standard($this->file($fn), 'application/pdf');
			else if (preg_match('/\.epub$/i', $fn)) $this->Output->standard($this->file($fn), 'application/epub+zip');
			else if (preg_match('/\.js$/i', $fn)) 	$this->Output->standard($this->file($fn), 'application/javascript');
			else if (preg_match('/\.ico$/i', $fn)) 	$this->Output->standard($this->file($fn), 'image/x-icon');
			else if (preg_match('/\.jpg$/i', $fn)) 	$this->Output->standard($this->file($fn), 'image/jpeg');
			else if (preg_match('/\.jpeg$/i', $fn)) $this->Output->standard($this->file($fn), 'image/jpeg');
			else if (preg_match('/\.png$/i', $fn)) 	$this->Output->standard($this->file($fn), 'image/png');
			else if (preg_match('/\.webp$/i', $fn)) $this->Output->standard($this->file($fn), 'image/webp');
			else if (preg_match('/\.gif$/i', $fn)) 	$this->Output->standard($this->file($fn), 'image/gif');
			else if (preg_match('/\.bmp$/i', $fn)) 	$this->Output->standard($this->file($fn), 'image/bmp');
			else if (preg_match('/\.svg$/i', $fn)) 	$this->Output->standard($this->file($fn), 'image/svg+xml');
			else if (preg_match('/\.css$/i', $fn)) 	$this->Output->standard($this->file($fn), 'text/css');
			else if (preg_match('/\.txt$/i', $fn)) 	$this->Output->standard($this->file($fn), 'text/css');
			else $this->Output->standard($this->file($fn));
		} else {
			$this->Output->error(502);
		}
	}

}

?>

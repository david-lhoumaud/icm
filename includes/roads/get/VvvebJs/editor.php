<?php
/**
 * Classe de routage GET GetEditor.
 * 
 * @return stdClass
 */
class GetVvvebJsEditor extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	public $html;
	
	public function __construct(){
		$this->html=Helpers::load('bootstrap');
		chdir('VvvebJs/');
		exec('../craft serve localhost:4444');
		header('Location: http://localhost:4444/editor.php');
		exit(0);
	}
	
	/**
	 * Initialisation de la route.
	 * 
	 * @return void
	 */
	public function __init(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		
	}
	
	
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->html(
			$this->html->template(
				$this->html->head(
					$this->payload('datas/output/head')
				), 
				$this->html->body(
					$this->payload('datas/output/body'), 
					'dark'
				),
				false
			)
		);
	}

}

?>

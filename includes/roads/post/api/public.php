<?php
/**
 * Classe de routage GET de l'API publique.
 * 
 * @return stdClass
 */
class PostApiPublic extends Controller {
	
	/** 
		 * Permission du routage 
		 * @var string $this->for 
		 */
	protected $for='public';
	/**
	 * Initialise la permission de la page avec $this->for
	 * @return void
	 */
	public function __construct(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->Output->json($this->payload());
	}

}

?>

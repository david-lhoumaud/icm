<?php
/**
 * Classe de routage GET de l'API publique.
 * 
 * @return stdClass
 */
class PostRootLogin extends Controller {
	
	/** 
		 * Permission du routage 
		 * @var string $this->for 
		 */
	protected $for='public';
	
	/**
	 * Initialise la permission de la page avec $this->for
	 * @return void
	 */
	public function __construct(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->session->set('error-location','login');
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		if (
			isset($_POST['usr-'.$this->session->access()]) &&
			isset($_POST['pwd-'.$this->session->access()])
		){
			$h=sha1($_POST['usr-'.$this->session->access()].'::'.$_POST['pwd-'.$this->session->access()]);
			$this->session->set('alert-success', 'Welcome '.$_POST['usr-'.$this->session->access()]);
			$this->session->set('icmauth', $h);
			header('Location: dashboard'); 
			exit;
		} else {
			sleep(30);
		}
		$this->session->set('salt-error', 'Access denied');
		
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->session->clean_access();
		header('Location: login'); 
		exit;
	}

}

?>

<?php
/**
 * Classe de routage POST PostRootRoadsManager.
 * 
 * @return stdClass
 */
class PostRootStreamrulesManager extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	public function __construct(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->session->set('error-location','manager');
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		$action=$_POST['action-'.$this->session->access()]??'not-action';
		switch($action) {
			case 'match-column':
				self::match_column();
				self::__output();
			break;
			case 'replace-all':
				self::replace();
				self::__output();
			break;
			case 'replace-by-line':
				self::replace();
				self::__output();
			break;
			case 'not-action':
				$this->session->set('alert-error', 'Aucune action défini');
				$this->Output->error(502);
			break;
			default:$this->session->set('alert-error', 'Erreur inconnu');
				$this->Output->error(502);
		}
	}
	
	private function replace() {
		$csv=$_FILES['csv1-'.$this->session->access()]??null;
		$str1=($_POST['str1-'.$this->session->access()]??'');
		$str2=($_POST['str2-'.$this->session->access()]??'');
		$this->payload('datas/output',$this->Parsing->replace($str1, $str2, file_get_contents($csv['tmp_name']??$csv['name'])));
		unlink($csv['tmp_name']??$csv['name']);
	}
	
	private function match_column() {
		$this->csv1=$_FILES['csv1-'.$this->session->access()]??null;
		$this->sep_csv1=$_POST['sep-csv1-'.$this->session->access()]??';';
		$this->match_col_csv1=((int)$_POST['match-col-csv1-'.$this->session->access()]??1)-1;
		$tmp_get_cols_csv1=explode(',',$_POST['get-cols-csv1-'.$this->session->access()]??array());
		$this->get_cols_csv1=array();
		foreach ($tmp_get_cols_csv1 as $cols) {
			$tmp=explode('=', $cols);
			$this->get_cols_csv1[(string)((int)$tmp[1]-1)]=((int)$tmp[0]-1);
		}
		if(!is_null($this->csv1) && !empty($this->csv1['name']??'')){
			$this->payload('datas/input/csv1',$this->csv($this->csv1['tmp_name']??$this->csv1['name'], $this->sep_csv1,'"',false));
			unlink($this->csv1['tmp_name']??$this->csv1['name']);
		}
		
		$this->csv2=$_FILES['csv2-'.$this->session->access()]??null;
		$this->sep_csv2=$_POST['sep-csv2-'.$this->session->access()]??';';
		$this->match_col_csv2=((int)$_POST['match-col-csv2-'.$this->session->access()]??1)-1;
		$tmp_get_cols_csv2=explode(',',$_POST['get-cols-csv2-'.$this->session->access()]??array());
		$this->get_cols_csv2=array();
		foreach ($tmp_get_cols_csv2 as $cols) {
			$tmp=explode('=', $cols);
			$this->get_cols_csv2[(string)((int)$tmp[1]-1)]=((int)$tmp[0]-1);
		}
		if(!is_null($this->csv2) && !empty($this->csv2['name']??'')){
			$this->payload('datas/input/csv2',$this->csv($this->csv2['tmp_name']??$this->csv2['name'], $this->sep_csv2,'"',false));
			unlink($this->csv2['tmp_name']??$this->csv2['name']);
		}
		
		if(!is_null($this->csv1) && !is_null($this->csv2) ) {
			$tmp_new_csv=array();
			$tmp_csv1=array();
			foreach ($this->payload('datas/input/csv1') as $index => $datas) {
				$tmp_csv1[(string)$datas[$this->match_col_csv1]]=$datas;
			}
			$tmp_csv2=array();
			foreach ($this->payload('datas/input/csv2') as $index => $datas) {
				$tmp_csv2[(string)$datas[$this->match_col_csv2]]=$datas;
			}
			
			$i=0;
			foreach ($tmp_csv1 as $key => $datas) {
				if (isset($tmp_csv2[$key])){
					foreach ($this->get_cols_csv1 as $k => $index) {
						$tmp_new_csv[$i][(string)$k]=$datas[$index];
					}
					foreach ($this->get_cols_csv2 as $k => $index) {
						$tmp_new_csv[$i][(string)$k]=$tmp_csv2[$key][$index];
					}
					ksort($tmp_new_csv[$i]);
					$i++;
				}
			}
			
		}
		
		$this->payload('datas/output',$this->toCsv($tmp_new_csv,';', "\n", false));
	}
	
	
	
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->session->clean_access();
		$this->Output->csv($this->payload('datas/output'),'icm-stream-rules.csv');
		header('Location: manager'); 
		exit;
	}

}

?>

<?php
/**
 * Classe de routage POST PostRootMatchsEdit.
 * 
 * @return stdClass
 */
class PostRootMatchsEdit extends Controller {
	
	/** 
		 * Permission du routage.
		 * root, public, ...
		 * @var array|string $for 
		 */
	protected $for='root';
	public function __construct(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->session->set('error-location','edit');
		$this->action=$_POST['action-'.$this->session->access()]??'not-define';
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		
		switch($this->action){
			case 'add-col-match':
				self::add();
			break;
			case 'edit-col-match':
				self::edit();
			break;
			case 'not-define':
				$this->session->set('alert-error', 'Aucune action défini');
				$this->Output->error(502);
			break;
			default:$this->session->set('alert-error', 'Erreur inconnu');
			$this->Output->error(502);
		}
	}
	
	private function add(){
		$f=$this->session->get('file_match');
		$cp=$_POST['cpos-'.$this->session->access()];
		$p=$_POST['position-'.$this->session->access()]-1;
		if ($cp>$p)$p--;
		$o=$_POST['origin-'.$this->session->access()];
		if(empty($o)){
			$this->session->set('alert-error', 'Le nom de la colonne est vide');
			$this->Output->error(502);
		}
		$d=str_replace('&quot;','"', $_POST['dest-'.$this->session->access()]);
		$json=$this->json($f);
		if (isset($json[$o])){
			$this->session->set('alert-error', 'La colonne existe déjà "'.$o.'"');
			$this->Output->error(502);
		} else {
			$json[$o]=$d;
		}
		$j=array();
		$i=0;
		$id_add=false;
		foreach($json as $origin=>$dest) {
			if ($i==$p) {
				$j[$o]=$d;
				$id_add=true;
				$j[$origin]=$dest;
			} else {
				$j[$origin]=$dest;
			}
			$i++;
		}
		if (!$is_add) {
			$j[$o]=$d;
		}
		//$this->toFile($f, $this->toJson($j));
		$this->toFile($f, json_encode($j, JSON_HEX_QUOT|JSON_PRESERVE_ZERO_FRACTION));
		$this->session->set('alert-success', 'La colonne "'.$o.'" a bien été ajouté.');
	}

	private function edit(){
		$f=$this->session->get('file_match');
		$cp=$_POST['cpos-'.$this->session->access()];
		$p=$_POST['position-'.$this->session->access()];
		if ($cp>$p)$p--;
		$c=$_POST['current-'.$this->session->access()];
		$o=$_POST['origin-'.$this->session->access()];
		$d=str_replace('&quot;','"', $_POST['dest-'.$this->session->access()]);

		$json=$this->json($f);
		if (isset($json[$c])){
			if ($o!=$c && !empty($c)) {
				$p--;
				unset($json[$c]);
			}
			if (!empty($o)) $json[$o]=$d;
		}
		$j=array();
		$i=0;
		$id_add=false;
		foreach($json as $origin=>$dest) {
			if(empty($origin) || empty($c))continue;
			if ($i==$p) {
				if (!empty($o)) $j[$o]=$d;
				$id_add=true;
				if ($o!=$origin)$j[$origin]=$dest;
			} else {
				if ($o!=$origin)$j[$origin]=$dest;
			}
			$i++;
		}
		if (!$is_add && !empty($o)) {
			$j[$o]=$d;
		}
		$this->toFile($f, json_encode($j, JSON_HEX_QUOT|JSON_PRESERVE_ZERO_FRACTION));
		$this->session->set('alert-success', 'La colonne a bien été modifié.');
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->session->set('scroll-y', $_POST['scroll-'.$this->session->access()]);
		$this->session->clean_access();
		header('Location: edit'); 
		exit;
	}

}

?>

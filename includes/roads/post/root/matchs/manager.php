<?php
/**
 * Classe de routage POST PostRootMatchsManager.
 * 
 * @return stdClass
 */
class PostRootMatchsManager extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	public function __construct(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->session->set('error-location','manager');
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		$action=$_POST['action-'.$this->session->access()]??'not-action';
		if ($action=='delete-match'){
			self::delete_match();
			self::__output();
		}
		switch ($action) {
			case 'not-action':
				$this->session->set('alert-error', 'Aucune action défini');
				$this->Output->error(502);
			break;
			default:$this->session->set('alert-error', 'Erreur inconnu');
			$this->Output->error(502);
		}
	}
	
	private function delete_match() {
		$dr=$_POST['delete-match-'.$this->session->access()]??'';
		if (empty($dr)) {
			$this->session->set('alert-error', 'Aucun matching à supprimer');
			$this->Output->error(502);
		} else {
			$fr='includes/matchs/'.$dr.'.json';
			if (file_exists($fr)){
				unlink($fr);
				if (!file_exists($fr))$this->session->set('alert-success', 'Le matching "'.$dr.'" a bien été supprimé.');
				else {
					$this->session->set('alert-error', 'Le matching "'.$dr.'" n\'a pas pu être supprimé.');
					$this->Output->error(502);
				}
			} else {
				$this->session->set('alert-error', 'Le matching "'.$dr.'" n\'existe pas');
				$this->Output->error(502);
			}
		}
	}
	
	
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->session->clean_access();
		header('Location: manager'); 
		exit;
	}

}

?>

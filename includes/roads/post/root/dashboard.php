<?php
/**
 * Classe de routage GET de l'API publique.
 * 
 * @return stdClass
 */
class PostRootDashboard extends Controller {
	
	/** 
	 * Permission du routage 
	 * @var array|string $this->for 
	 */
	protected $for=['root'];
	public function __construct(){}
	
	public function __init(){
		$this->session->set('error-location','login');
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		if (isset($_POST['action-'.$this->session->access()])){
			$this->use_bootstrap=false;
			switch($_POST['action-'.$this->session->access()]){
				case 'create_mapping_class':
					$this->type=$_POST['type-'.$this->session->access()];
					$this->path=$_POST['path-'.$this->session->access()];
					$this->class_for=$_POST['for-'.$this->session->access()]??'root';
					$this->rootpath='includes/maps/'.$this->type;
					$this->ClassName=Loader::classpath_to_classname($this->path, $this->type);
					$this->file_destination=$this->rootpath.'/'.$this->path.'.php';
					$this->template='includes/templates/map.tpl';
					$this->st='Le mappage';
				break;
				case 'create_routing_class':
					$this->inc_html=$_POST['inchtml-'.$this->session->access()]??false;
					$this->use_bootstrap=($this->inc_html!='none');
					$this->inc_js=$_POST['incjs-'.$this->session->access()]??false;
					$this->inc_view=$_POST['incview-'.$this->session->access()]??false;
					$this->method=$_POST['method-'.$this->session->access()];
					$this->path=$_POST['path-'.$this->session->access()];
					$this->class_for=$_POST['for-'.$this->session->access()]??'root';
					$this->rootpath='includes/roads/'.$this->method;
					$this->ClassName=Loader::classpath_to_classname($this->path, $this->method);
					$this->file_destination=$this->rootpath.'/'.$this->path.'.php';
					$this->template='includes/templates/road.tpl';
					$this->st='Le routage';
				break;
				case 'create_matching_file':
					$this->template='includes/templates/match.json';
					$this->dm=$_POST['dest-match-'.$this->session->access()];
					$this->fm=$_POST['format-import-'.$this->session->access()]??'json';
					$this->generate_file=$_FILES['import-'.$this->session->access()]??null;
					if(!is_null($this->generate_file) && !empty($this->generate_file['name']??'')){
						if($this->fm!='json'){
								$this->payload('datas/input',$this->csv($this->generate_file['tmp_name']??$this->generate_file['name'], $this->fm));
								$tmp=array();
								$ii=0;
								foreach	($this->payload('datas/input')[0]??$this->payload('datas/input') as $key=>$value){
									if ($this->dm=='from') $tmp[(string)$key]='';
									else $tmp['__'.($ii++).'__']=$key;
								}
								$this->payload('datas/input',$tmp);
								$this->template='';
						} else {
								$this->payload('datas/input',$this->json($this->generate_file['tmp_name']??$this->generate_file['name']));
								$tmp=array();
								$ii=0;
								foreach	($this->payload('datas/input')[0]??$this->payload('datas/input') as $key=>$value){
									if ($this->dm=='from') $tmp[(string)$key]='';
									else $tmp['__'.($ii++).'__']=$key;
								}
								$this->payload('datas/input',$tmp);
								$this->template='';

						}
					}
					$pp=$_POST['path-'.$this->session->access()]??'';
					if (empty($pp)) {
						$this->session->clean_access();
						$this->session->set('alert-warning', 'Aucun chemin de matching défini');
						header('Location: dashboard');
						exit;
					}
					$this->path=$this->dm.'/'.$pp;
					$this->rootpath='includes/matchs';
					$this->ClassName='mapping(\''.$this->path.'\')';
					$this->file_destination=$this->rootpath.'/'.$this->path.'.json';
					
					$this->st='La correspondance';
				break;
				case 'edit_matching_file':
					$this->file=$_POST['file_match-'.$this->session->access()];
				break;
			}
		} else {
			$this->session->set('alert-error', 'Aucune action de défini');
			header('Location: dashboard');
			exit;
		}
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		if (isset($_POST['action-'.$this->session->access()]) && $_POST['action-'.$this->session->access()]=='edit_matching_file'){
			$this->session->set('file_match', $this->file);
			$this->session->clean_access();
			header('Location: matchs/edit');
			exit;
		}
		if (!file_exists($this->file_destination)){
			if (!file_exists(dirname($this->file_destination))) {
				mkdir(dirname($this->file_destination), 0777, true);
			}
			$ex=explode('/',dirname($this->path));
			$Perms=Helpers::load('perms');
			$Perms->set(0777,$this->rootpath);
			foreach($ex as $d){
				$this->rootpath.='/'.$d;
				$Perms->set(0777,$this->rootpath);
			}
			$Perms->set(0777,$this->rootpath);
			
			$this->toFile(
				$this->file_destination, 
				self::use_view(
					self::use_js(
						self::use_bootstrap(
							self::inject_vars()
						)
					)
				)
			);
			$Perms->set(0666,$this->file_destination);
			$this->session->set('alert-success', $this->st.' '.$this->ClassName.' a bien été créé');
		} else {
			$this->session->set('alert-warning', $this->st.' '.$this->ClassName.' existe déjà');
		}
	}
	
	private function inject_vars() {
		$format=$_POST['format-'.$this->session->access()]??'json';
		$enclosure_csv=$_POST['enclosure-'.$this->session->access()]??'"';
		$INformat=$_POST['informat-'.$this->session->access()]??'none';
		$OUTformat=$_POST['outformat-'.$this->session->access()]??'payload';
		if ($INformat=='raws'){
			$din="\$this->payload('raws')";
			$isf='false';
		} else if ($INformat=='file'){
			$din="'files/input/'.\$this->payload('files/input')";
			$isf='true';
		} else if ($INformat=='url'){
			$din="\$this->urlStream(\$this->payload('files/input'))";
			$isf='false';
		} else {
			$din="''";
			$isf='false';
		}
		$sep_csv=';';
		if (preg_match('/csv/', $format)){
			if (preg_match('/_v/', $format)) $sep_csv=',';
			else if (preg_match('/_tab/', $format)) $sep_csv='	';
			else if (preg_match('/_b/', $format)) $sep_csv='|';
			else if (preg_match('/_u/', $format)) $sep_csv='_';
			else if (preg_match('/_t/', $format)) $sep_csv='-';
			$format='csv';
		}
		$file_match=preg_replace(array('/^includes\/matchs\//', '/\.json$/'),'',($_POST['file_match-'.$this->session->access()]??($this->type=='output'?'to/':'from/')));
		return str_replace(
			[
				'/**TYPE*/', 
				'/**METHOD*/', 
				'/**type*/', 
				'/**method*/', 
				'/**ClassName*/', 
				'/**for*/',
				'/**use_datas*/',
				'/**end_datas*/',
			],
			[
				strtoupper($this->type??'').' '.$this->ClassName, 
				strtoupper($this->method??'').' '.$this->ClassName, 
				$this->type??'', 
				$this->method??'', 
				$this->ClassName, 
				$this->class_for,
				($this->type=='input'?
					"\$this->payload(
			'datas/input', 
			\$this->".$format."(
				$din"
				.($format=='csv'?",\n\t\t\t\t'".$sep_csv."',\n\t\t\t\t'".$enclosure_csv."',\n\t\t\t\ttrue":'')
				.($format=='json'?",\n\t\t\t\tfalse":'')
				.",\n\t\t\t\t".$isf."
			)
		);"
				:
					"\$map=\$this->map('".$file_match."');
		\$datas=array();
		foreach (\$this->payload('datas/".($this->type=='output'?'process':'input')."') as \$index => \$cols) {
			\$object=\$this->match(\$cols, \$map);
			\$datas[]=\$object;
		}
		\$this->payload('datas/".($this->type??'output')."', \$datas);"
				),
				($this->type=='output' && $OUTformat=='file'?
					"\n\t\t\$this->toFile(
			'files/output/'.\$this->payload('files/output'), 
			\$this->to".ucfirst($format)."(
				\$this->payload('datas/output')"
				.($format=='csv'?",\n\t\t\t\t'$sep_csv'":'')."
			)
		);"
				:''),
			],
			(!empty($this->template)?file_get_contents($this->template):$this->toJson($this->payload('datas/input')))
		);
	}
	
	private function use_bootstrap($tpl) {
		if ($this->use_bootstrap){
			if ($this->inc_html=='html'){
				$result=self::use_html($tpl);
			} else {
				$result=str_replace(
					['/**bootstrap*/', '/**bootstrap_head*/', '/**bootstrap_body*/', '/**bootstrap_alert*/', '/**OUTPUT*/', '/**bootstrap_init*/'],
					[
						"\n\t\t\$this->html=Helpers::load('bootstrap');\n\t",
						"\$this->payload('datas/output/head',\n\t\t\t\$this->html->title('ICM - ".$this->ClassName."')\n\t\t\t.\$this->html->load()\n\t\t);\n\t\t\$this->session->access(true);",
						"\$content=self::__alert()
							.\$this->html->container(\n".self::tab(7)."'Contenu de ".$this->ClassName." ...'\n".self::tab(6).");\n".self::tab(2)."\$this->payload('datas/output/body', \$content);",
						"/**
	* afficher les alertes bootstrap.
	* 
	* @return string
	*/
	private function __alert(\$ms=5000){
		\$result='';
		if (\$this->session->get('alert-success')) {
			\$result.=\$this->html->alert('success', \$this->session->get('alert-success'), 'style=\"z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;\"', \$ms);
			\$this->session->set('alert-success', false);
		}
		if (\$this->session->get('alert-error')) {
			\$result.=\$this->html->alert('danger', \$this->session->get('alert-error'), 'style=\"z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;\"', \$ms);
			\$this->session->set('alert-error', false);
		}
		if (\$this->session->get('alert-warning')) {
			\$result.=\$this->html->alert('warning', \$this->session->get('alert-warning'), 'style=\"z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;\"', \$ms);
			\$this->session->set('alert-warning', false);
		}
		return \$result;
	}",
						"\$this->Output->html(
				\$this->html->template(
					\$this->html->head(
						\$this->payload('datas/output/head')
					), 
					\$this->html->body(
						\$this->payload('datas/output/body'), 
						'light'
					),
					false
				)
			);",
			"\n\t\t\$this->session->set('error-location','".basename($this->path)."');\n\t"
					],
					$tpl
				);
			}
		} else {
			$result=str_replace(
				['/**bootstrap*/', '/**bootstrap_head*/', '/**bootstrap_body*/', '/**bootstrap_alert*/', '/**OUTPUT*/', '/**bootstrap_init*/'],
				[
					'','','', '', "\t\t\$this->Output->json(\$this->payload());", ''
				],
				$tpl
			);
		}
		return $result;
	}
	
	private function use_html($tpl) {
			$result=str_replace(
				['/**bootstrap*/', '/**bootstrap_head*/', '/**bootstrap_body*/', '/**bootstrap_alert*/', '/**OUTPUT*/', '/**bootstrap_init*/'],
				[
					"\n\t\t\$this->html=Helpers::load('html');\n\t",
					"\$this->payload('datas/output/head',\n\t\t\t\$this->html->title('ICM - ".$this->ClassName."')\n\t\t);\n\t\t\$this->session->access(true);",
					"\$content=\$this->html->div(\n".self::tab(3)."'Contenu de ".$this->ClassName." ...'\n".self::tab(2).");\n".self::tab(2)."\$this->payload('datas/output/body', \$content);",
					"",
					"\$this->Output->html(
			\$this->html->template(
				\$this->payload('datas/output/head'), 
				\$this->payload('datas/output/body')
			)
		);",
		"\n\t\t\$this->session->set('error-location','".basename($this->path)."');\n\t"
				],
				$tpl
			);
		return $result;
	}
	
	private function use_js($tpl) {
		if ($this->inc_js!='none'){
			$result=str_replace(
				['/**js*/'],
				[($this->use_bootstrap?"\t":"\n\t\t")."\$this->js=Helpers::load('js');\n\t"],
				$tpl
			);
		} else {
			$result=str_replace(
				['/**js*/'],
				[''],
				$tpl
			);
		}
		return $result;
	}
	
	private function use_view($tpl) {
		if ($this->inc_view!='none' && $this->use_bootstrap){
			$result=str_replace(
				['/**view*/'],
				["\t\$this->view=Views::load('".str_replace(['includes/views/', '.php'],'',$this->inc_view)."', \$this->html);\n\t"],
				$tpl
			);
		} else {
			$result=str_replace(
				['/**view*/'],
				[''],
				$tpl
			);
		}
		return $result;
	}
	
	
	/**
	 * Retourne un nombre voulu de tabulation
	 * 
	 * @param integer $num nombre de tabulation
	 * @return string
	 */
	private function tab($num=1){
		$tab='';
		for ($i=0; $i<$num; $i++) $tab.="\t";
		return $tab;
	}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->session->clean_access();
		header('Location: dashboard'); 
		exit;
	}

}

?>

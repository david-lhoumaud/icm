<?php
/**
 * Classe de routage POST PostRootMapsManager.
 * 
 * @return stdClass
 */
class PostRootMapsManager extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	public function __construct(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->session->set('error-location','manager');
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		$action=$_POST['action-'.$this->session->access()]??'not-action';
		if ($action=='delete-map'){
			self::delete_map();
			self::__output();
		}
		$sm=$_POST['select-map-'.$this->session->access()]??'';
		$op=$_POST['old-path-match-'.$this->session->access()]??'';
		$np=$_POST['new-path-match-'.$this->session->access()]??'';
		if (empty($sm)) $action='no-map';
		else if (empty($op) || empty($np)) $action='void-data';
		else if ($op==$np) $action='not-changed';
		switch ($action) {
			case 'not-changed':self::__output();
			case 'update-match':
				$fm='includes/maps/'.$sm.'.php';
				if (file_exists($fm)){
					$this->toFile($fm, str_replace($op, $np, $this->file($fm)));
					$this->session->set('alert-success', 'La mise à jour du matching a bien été effectué.');
				} else {
					$this->session->set('alert-error', 'La map n\'existe pas');
					$this->Output->error(502);
				}
			break;
			case 'no-map':
				$this->session->set('alert-error', 'Aucune map à modifier');
				$this->Output->error(502);
			break;
			case 'void-data':
				$this->session->set('alert-error', 'Aucune donnée à traiter');
				$this->Output->error(502);
			break;
			case 'not-action':
				$this->session->set('alert-error', 'Aucune action défini');
				$this->Output->error(502);
			break;
			default:$this->session->set('alert-error', 'Erreur inconnu');
			$this->Output->error(502);
		}
	}
	
	private function delete_map() {
		$dm=$_POST['delete-map-'.$this->session->access()]??'';
		if (empty($dm)) {
			$this->session->set('alert-error', 'Aucune map à supprimer');
			$this->Output->error(502);
		} else {
			$fm='includes/maps/'.$dm.'.php';
			if (file_exists($fm)){
				unlink($fm);
				if (!file_exists($fm))$this->session->set('alert-success', 'La map a bien été supprimée.');
				else {
					$this->session->set('alert-error', 'La map n\'a pas pu être supprimée.');
					$this->Output->error(502);
				}
			} else {
				$this->session->set('alert-error', 'La map n\'existe pas');
				$this->Output->error(502);
			}
		}
	}
	
	
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->session->clean_access();
		header('Location: manager'); 
		exit;
	}

}

?>

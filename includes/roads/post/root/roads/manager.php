<?php
/**
 * Classe de routage POST PostRootRoadsManager.
 * 
 * @return stdClass
 */
class PostRootRoadsManager extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	public function __construct(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		$this->session->set('error-location','manager');
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		$action=$_POST['action-'.$this->session->access()]??'not-action';
		if ($action=='delete-road'){
			self::delete_road();
			self::__output();
		}
		switch ($action) {
			case 'not-action':
				$this->session->set('alert-error', 'Aucune action défini');
				$this->Output->error(502);
			break;
			default:$this->session->set('alert-error', 'Erreur inconnu');
			$this->Output->error(502);
		}
	}
	
	private function delete_road() {
		$dr=$_POST['delete-road-'.$this->session->access()]??'';
		if (empty($dr)) {
			$this->session->set('alert-error', 'Aucune route à supprimer');
			$this->Output->error(502);
		} else {
			$fr='includes/roads/'.$dr.'.php';
			if (file_exists($fr)){
				unlink($fr);
				if (!file_exists($fr))$this->session->set('alert-success', 'La route a bien été supprimée.');
				else {
					$this->session->set('alert-error', 'La route n\'a pas pu être supprimée.');
					$this->Output->error(502);
				}
			} else {
				$this->session->set('alert-error', 'La route n\'existe pas');
				$this->Output->error(502);
			}
		}
	}
	
	
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		$this->session->clean_access();
		header('Location: manager'); 
		exit;
	}

}

?>

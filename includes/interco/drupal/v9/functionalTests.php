<?php
class DrupalV9FunctionalTests {

    public function __construct($params=array()) {
    $this->d9								= Infra::load('drupal/v9/preprod');
		$this->requests					= Seeder::load('drupal/v9/requests');
    $this->canonical_origin	= $this->d9['url'];
    $this->canonical				= $this->canonical_origin;
		$this->Curl							= ($params[0]??false)?$params[0]:Helpers::load('curl');
    $this->Curl->setUserAgent('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0');
		$this->Log							= ($params[1]??false)?$params[1]:Helpers::load('log', 'cmd');
		$this->Parse						= ($params[2]??false)?$params[2]:Helpers::load('parsing');
		$this->ws								= Interco::load('drupal/v9/getDatas', [$this->Curl, $this->Log, $this->Parse]);
    }

    public function set_canonical_origin($url, $modify_canonical=false) {
        $this->canonical_origin=$url;
        if ($modify_canonical) self::set_canonical($url);
    }

    public function set_canonical($url) {
        $this->canonical=$url;
    }

	private function delete($form_id) {
		$url=$this->canonical.'/node/'.$this->node_id.'/delete';
		if ($this->ws->isc) $this->Log->d($url);
		$request=$this->requests['delete_node'];
		$request['form_build_id']=$this->ws->get_form_build_id($url);
		$request['form_token']=$this->ws->get_form_token();
		$request['form_id']=$form_id;
		$this->Curl->post($url, http_build_query($request));
	}

	private function check_add_edit_node($url, $request, $title, $get_canonical=false) {
		if ($this->ws->isc) $this->Log->d($url);
		$request['form_build_id']=$this->ws->get_form_build_id($url);
		$request['form_token']=$this->ws->get_form_token();
		$request['changed']=$this->ws->get_changed();
		$request['created[0][value][date]']=date('Y-m-d');
		$request['created[0][value][time]']=date("H:i:s");
		$datas=$this->Curl->post($url,http_build_query($request));
		if (preg_match('/'.$request['title[0][value]'].'/', $datas['content'])){
			$result=['state'=>true];
			if ($this->ws->isc) $this->Log->o($title);
		} else {
			$result=['state'=>false, 'code'=>$datas['status']];
			if ($this->ws->isc) $this->Log->e($title);
		}
		if ($get_canonical) $this->node_id=$this->ws->get_node_id($datas['content']);
		return $result;
	}

	private function add_node($path, $req, $title) {
		$url=$this->canonical.'/node/add/'.$path;
		$request=$this->requests[$req];
		$result=self::check_add_edit_node($url, $request, $title, true);
		return $result;
	}

	private function edit_node($req, $form_id, $title) {
		$url=$this->canonical.'/node/'.$this->node_id.'/edit';
		$request=$this->requests[$req];
		$request['title[0][value]'].=' Modified';
		$request['form_id']	=$form_id;
		$result=self::check_add_edit_node($url, $request, $title, false);
        return $result;
    }

	public function is_cmd(){
		$this->ws->isc=true;
	}

	public function index() {
        $url=$this->canonical.'/blog';
        return $this->ws->get($url, 'Index', 200);
    }

	public function forbidden() {
        $url=$this->canonical.'/node/add/blog';
		return $this->ws->get($url, 'Forbidden', 403);
    }

	public function author() {
        $url=$this->canonical.'/blog/auteurs/1';
		return $this->ws->get($url, 'Author', 200);
    }

    public function login() {
        $url=$this->canonical.'/user/login';
		$request=$this->requests['login'];
		$request['name']=$this->d9['site-user'];
		$request['pass']=$this->d9['site-pass'];
		$request['form_build_id']=$this->ws->get_form_build_id($url);
		$this->Curl->post($url, http_build_query($request));
        return $this->ws->get($this->canonical.'/user/1?check_logged_in=1', 'Login', 200);
    }

    public function add_node_blog() {
		return self::add_node('blog', 'add_node_blog', 'Add Node Blog');
    }

    public function edit_node_blog() {
        return self::edit_node('add_node_blog', 'node_blog_edit_form', 'Edit Node Blog');
    }

    public function add_image_node_blog() {
        $url=$this->canonical.'/edit?element_parents=field_thumbmail_media/widget/0&destination=/admin/content&ajax_form=1&_wrapper_format=drupal_ajax';
        $request=$this->requests['add_node_blog'];
		$request['form_build_id']=$this->ws->get_form_build_id($this->canonical.'/edit');
		$request['form_token']=$this->ws->get_form_token();
		$request['changed']=$this->ws->get_changed();
		$image_file='files/input/image_inject.png';
        $result=['state'=>false];
		if (file_exists($image_file)){
			$info=pathinfo($image_file);
			$mime = mime_content_type($image_file);
			unset($request['op']);
			$request['files[field_thumbmail_media_0]']=new CURLFile($image_file, $mime, $info['basename']);
			$request['field_thumbmail_media[0][display]']=1;
			$request['revision']=1;
			$request['_triggering_element_name']='field_thumbmail_media_0_upload_button';
			$request['_triggering_element_value']='Upload';
			$request['_drupal_ajax']=1;
			$request['ajax_page_state[theme]']='eurecia_back';
			$request['ajax_page_state[theme_token]']='-rQaOQD8iEZFWUZ1IoFA360jef7auoK1npJQ6qd8Hyc';
			$request['ajax_page_state[libraries]']='admin_toolbar/toolbar.tree,admin_toolbar/toolbar.tree.hoverintent,admin_toolbar_tools/toolbar.icon,ckeditor/drupal.ckeditor,core/drupal.active-link,core/drupal.autocomplete,core/drupal.collapse,core/drupal.date,core/drupal.dialog.ajax,core/drupal.states,core/internal.jquery.form,core/normalize,datalayer/behaviors,devel/devel-toolbar,editor/drupal.editor,eu_cookie_compliance/eu_cookie_compliance_default,eurecia_back/css,eurecia_back/scripts,file/drupal.file,filter/drupal.filter,menu_ui/drupal.menu_ui,node/drupal.node,node/form,path/drupal.path,seven/classy.image-widget,seven/classy.messages,seven/drupal.nav-tabs,seven/global-styling,seven/node-form,system/admin,system/base,text/drupal.text,token/token,toolbar/toolbar,toolbar/toolbar.escapeAdmin,user/drupal.user.icons';
			$datas=$this->Curl->post($url,http_build_query($request));
			if ($datas['status']==200) {
				$result=['state'=>true];
				if ($this->ws->isc) $this->Log->o('Add Image Node Blog');
			} else {
				$result=['state'=>false, 'code'=>$datas['status'], 'image'=>$request['files[field_thumbmail_media_0]']];
				if ($this->ws->isc) $this->Log->e('Add Image Node Blog: error '.$datas['status']);
			}
		}
        return $result;
    }

    public function delete_node_blog() {
		self::delete('node_blog_delete_form');
        return $this->ws->get($this->canonical, 'Delete Node Blog', 404);
    }

    public function add_node_bloc_wysiwyg() {
        $this->canonical=$this->canonical_origin;
        return self::add_node('bloc_wysiwyg', 'add_node_bloc_wysiwyg', 'Add Node Bloc Wysiwyg');
    }

	public function edit_node_bloc_wysiwyg() {
        return self::edit_node('add_node_bloc_wysiwyg', 'node_bloc_wysiwyg_edit_form', 'Edit Node Bloc Wysiwyg');
    }

    public function delete_node_bloc_wysiwyg() {
		self::delete('node_bloc_wysiwyg_delete_form');
        return $this->ws->get($this->canonical, 'Delete Node Bloc Wysiwyg', 404);
    }

    public function add_node_page() {
        $this->canonical=$this->canonical_origin;
        return self::add_node('page', 'add_node_page', 'Add Node Page');
    }

	public function edit_node_page() {
        return self::edit_node('add_node_page', 'node_page_edit_form', 'Edit Node Page');
    }

    public function delete_node_page() {
		self::delete('node_page_delete_form');
        return $this->ws->get($this->canonical, 'Delete Node Page', 404);
    }
}
?>
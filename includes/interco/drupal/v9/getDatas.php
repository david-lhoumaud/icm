<?php
class DrupalV9GetDatas {

    public function __construct($params=array()) {
		$this->isc		= false;
		$this->Curl		= $params[0]??false?$params[0]:Helpers::load('curl');
        $this->Curl->setUserAgent('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0');
		$this->Log		= $params[1]??false?$params[1]:Helpers::load('log', 'cmd');
		$this->Parse	= $params[2]??false?$params[2]:$this->Parse=Helpers::load('parsing');
    }

    public function get_form_build_id($url=null) {
		if (!is_null($url)) $this->html=$this->Curl->get($url);
		$value=$this->Parse->middle('name="form_build_id" value="', '" />', $this->html['content']??'');
		if (empty($value)){
			$tests_results['form_build_id'][]=['state'=>false, 'url'=>$url];
			if ($this->isc) $this->Log->e('Form Build ID: '.$url);
		}
		return $value;
	}

	public function get_form_token($url=null) {
		if (!is_null($url)) $this->html=$this->Curl->get($url);
		$value=$this->Parse->middle('name="form_token" value="', '" />', $this->html['content']??'');
		if (empty($value)){
			$tests_results['form_token'][]=['state'=>false, 'url'=>$url];
			if ($this->isc) $this->Log->e('Form Token: '.$url);
		}
		return $value;
	}

	public function get_changed($url=null) {
		if (!is_null($url)) $this->html=$this->Curl->get($url);
		$value=$this->Parse->middle('name="changed" value="', '" />', $this->html['content']??'');
		if (empty($value)){
			$tests_results['changed'][]=['state'=>false, 'url'=>$url];
			if ($this->isc) $this->Log->e('Changed: '.$url);
		}
		return $value;
	}

	public function get_canonical($flux) {
		$value=$this->Parse->middle('<link rel="canonical" href="', '" />', $flux);
		if (empty($value)){
			$tests_results['canonical'][]=['state'=>false];
			if ($this->isc) $this->Log->e('Canonical');
		}
		return $value;
	}

	public function get_node_id($flux) {
		$value=$this->Parse->middle('is-active" data-drupal-link-system-path="node/', '">View', $flux);
		if (empty($value)){
			$tests_results['node_id'][]=['state'=>false];
			if ($this->isc) $this->Log->e('Node ID');
		}
		if ($this->isc) $this->Log->o('Node ID: '.$value);
		return $value;
	}

	public function get($url, $title, $code) {
		$datas=$this->Curl->get($url);
		if ($datas['status']==$code) {
			$result=['state'=>true];
			if ($this->isc) $this->Log->o($title);
		} else {
			$result=['state'=>false, 'code'=>$datas['status']];
			if ($this->isc) $this->Log->e($title.': error '.$datas['status']);
		}
        return $result;
	}

}
?>
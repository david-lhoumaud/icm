<?php
/**
 * Chargeur de classe
 */
class Loader {
	/**
	 * Récupère une classe
	 * @param string $class_name nom de la class à charger
	 * @param string $classPath chemin d'accès de la classe
	 * @param mixed|null $params paramètre envoyé à la classe
	 * @param string $rootpath repertoire racine de la classe
	 * @return stdClass|false Renvoie la classe ou faux si elle n'existe pas
	 */
	static function load_class($class_name, $classPath, $params=NULL, $rootpath='') {
		if (!class_exists($class_name)) {
			$root_path=__DIR__.'/'.(!empty($rootpath)?$rootpath.'/':'');
			if (!file_exists($root_path.$classPath.'.php')) return false;
			require_once $root_path.$classPath.'.php';
		}
		if (class_exists($class_name)) {
			if (!is_null($params)) return new $class_name($params);
			else return new $class_name();
		}
		return false;
	}
	
	/**
	 * Transforme un chemin d'accès en nom de classe
	 * @param string $classPath chemin d'accès de la classe
	 * @param string $prefix prefix à ajouter au nom de la classe
	 * @return string Nom de la classe
	 */
	static function classpath_to_classname($classPath, $prefix='') {
		$array_name=explode('/', $classPath);
		if (is_array($array_name)) {
			$class_name=ucfirst($prefix);
			for ($i=0; $i<count($array_name); $i++) {
				$class_name.=ucfirst($array_name[$i]);
			}
		} else {
			$class_name=ucfirst($prefix).ucfirst($array_name);
		}
		return $class_name;
	}

}

?>

<?php
require_once 'loader.class.php';
/**
 * Charge les classes de requête
 */
class Queries extends Loader {
	/**
	 * Charge une classe de requete avec un chemin relatif au répertoire de la classe courante et sans extention de nom de fichier.
	 * Le nom de la classe, doit avoir le prefix 'Queries'. \
	 * exemple : QueriesClassname \
	 * \
	 * Si la classe est dans un sous dossier, son nom devra comporter le nom du repertoire parent. \
	 * exemple : QueriesDirnameClassname
	 * 
	 * @param string $classPath chemin d'accès de la classe
	 * @param mixed|null $params paramètre envoyé à la classe
	 * @return stdClass|false Renvoie la classe ou faux si elle n'existe pas
	 */
	static function load($classPath, $params=NULL){
		return self::load_class(
			self::classpath_to_classname($classPath, 'queries'),
			$classPath, 
			$params, 
			'queries'
		);
	}

}

?>

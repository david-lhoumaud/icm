<?php
class ViewsRootFooter {
	private $html;
	public function __construct($html) {
		$this->html=$html;
	}
	
	public function mini($disable='', $root='') {
		return '<footer class="bg-warning text-white text-center text-lg-start" style="position:absolute;bottom:0px;left:0px;width:100%">
		<!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
    © '.date('Y').' Copyright:
    <a class="text-white" href="https://idfol.fr/">ICM Framework</a>
  </div>
  <!-- Copyright -->
</footer>';
	}
	
	public function full($disable='', $root='', $style='') {
		return '<footer class="bg-warning text-white text-center text-lg-start" style="">
  <!-- Grid container -->
  <div class="container p-4">
    <!--Grid row-->
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
        <h5 class="text-uppercase">Informations général</h5>
        <p>
					ICM est un Framework PHP qui permet de mapper des donnée de façon organiser. <br>
					Un flux payload est disponible tout au long de l\'execution.<br>
					Ce flux sera tansmis à 3 classes de mappages de cette manière: Input&#10143;Process&#10143;Output
        </p>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Les Bases</h5>
        <ul class="list-unstyled mb-0">
          <li>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/docs/using.html" target="_blank" class="text-white">Utilisation</a>
          </li>
          <li>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/docs/permissions.html" target="_blank" class="text-white">Permission</a>
          </li>
          <li>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/docs/payload.html" target="_blank" class="text-white">Payload</a>
          </li>
          <li>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/docs/map.html" target="_blank" class="text-white">Mappage</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase mb-0">Les Classes</h5>
        <ul class="list-unstyled  mb-0">
          <li>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/docs/icm/index.html" target="_blank" class="text-white">Classe ICM</a>
          </li>
          <li>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/docs/maps/index.html" target="_blank" class="text-white">Classe de Mappage</a>
          </li>
          <li>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/docs/roads/index.html" target="_blank" class="text-white">Classe de Routage</a>
          </li>
          <li>
            <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/docs/helpers/index.html" target="_blank" class="text-white">Classe Helpers</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->
    </div>
    <!--Grid row-->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
    © '.date('Y').' Copyright:
    <a class="text-white" href="https://idfol.fr/">ICM Framework</a>
  </div>
  <!-- Copyright -->
</footer>';
	}
	
}
?>
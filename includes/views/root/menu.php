<?php
class ViewsRootMenu {
	
	private $html;
	public function __construct($html) {
		$this->html=$html;
	}
	
	public function managers($disable='', $root='') {
		return $this->html->div(
			self::group($disable, $root),
			'style="position:fixed;top:1%;right:1%;z-index:10000;"'
		);
	}
	
	public function group($disable='', $root='', $style='') {
		return $this->html->div(
				$this->html->div(
					$this->html->a('secondary fas fa-toolbox', 				'', $root. 'dashboard'			.($disable=='dashboard'?'" style="pointer-events:none;background-color:rgba(0,0,0,0);color: grey;':''))
					.$this->html->a('primary fas fa-road', 						'', $root. 'roads/manager'	.($disable=='roads'?		'" style="pointer-events:none;background-color:rgba(0,0,0,0);color: grey;':''))
					.$this->html->a('info fas fa-map-marked-alt', 		'', $root. 'maps/manager'		.($disable=='maps'?			'" style="pointer-events:none;background-color:rgba(0,0,0,0);color: grey;':''))
					.$this->html->a('success fas fa-clipboard-list', 	'', $root. 'matchs/manager'	.($disable=='matchs'?		'" style="pointer-events:none;background-color:rgba(0,0,0,0);color: grey;':''))
					.$this->html->a('warning fas fa-unlock', 					'', $root. 'infras/manager'	.($disable=='infras'?		'" style="pointer-events:none;background-color:rgba(0,0,0,0);color: grey;':''))
					.$this->html->a('danger fas fa-network-wired', 		'', $root. 'interco/manager'.($disable=='interco'?	'" style="pointer-events:none;background-color:rgba(0,0,0,0);color: grey;':''))
					.$this->html->a('danger fas fa-space-shuttle', 		'', $root. 'streamrules/manager'.($disable=='streamrules'?	'" style="pointer-events:none;background-color:rgba(0,0,0,0);color: grey;':''))
					.$this->html->a('primary fas fa-info', 		'', $root.'../docs" target="_blank"'),
					'class="btn-group" role="group"'
				),
				'class="btn-group btn-group-justified" role="group"'.(!empty($style)?' style="'.$style.'"':'')
			);
	}
	
}
?>
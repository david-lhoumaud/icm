<?php
require_once 'loader.class.php';
/**
 * Charge les classes de routage HTTP
 */
class Roads extends Loader {
	/**
	 * Charge une classe de routage HTTP avec un chemin relatif au répertoire de la classe courante, le sous dossier roads/ et sans extention de nom de fichier.
	 * Si la classe est dans un sous dossier le nom de la classe devra comporter le nom du repertoire parent. \
	 * exemple : DirnameClassname
	 * 
	 * @param string $classPath chemin d'accès de la classe
	 * @param mixed|null $params paramètre envoyé à la classe
	 * @return stdClass|false Renvoie la classe ou faux si elle n'existe pas
	 */
	static function load($classPath, $params=NULL){
		return self::load_class(
			self::classpath_to_classname($classPath),
			$classPath, 
			$params, 
			'roads'
		);
	}

}

?>

<?php
require_once 'loader.class.php';
/**
 * Charge les classes de mappage
 */
class Maps extends Loader {
	/**
	 * Charge une classe de mappage avec un chemin relatif au répertoire de la classe courante, le sous dossier maps/ et sans extention de nom de fichier.
	 * Si la classe est dans un sous dossier le nom de la classe devra comporter le nom du repertoire parent. \
	 * exemple : DirnameClassname
	 * 
	 * @param string $classPath chemin d'accès de la classe
	 * @param mixed|null $params paramètre envoyé à la classe
	 * @return stdClass|false Renvoie la classe ou faux si elle n'existe pas
	 */
	static function load($classPath, $params=NULL, $origin_class='') {
		$classname=self::classpath_to_classname($classPath);
		if (class_exists($classname) && $origin_class==$classname) {
			Helpers::load('output')->error(888, 'Infinite Mapping Loop'); 
		}
		return self::load_class(
			$classname,
			$classPath, 
			$params, 
			'maps'
		);
	}

}

?>

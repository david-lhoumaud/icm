<?php

// ini_set("max_execution_time", 999999);
// ini_set('memory_limit', '15360M');

require_once 'includes/helpers.class.php';

if (function_exists('shmop_open')){
    // Dans la logique de traitement de votre classe de mappage (input, process, output)
    if (!Helpers::load(
        'rateLimiter', 
        [Helpers::load('config', [
            'config'=>[],
            'used'=>[]
        ])->get(), 
        Helpers::load('memory')]
    )->ok()) {
        // Le rate limite a été atteint, renvoyez une réponse d'erreur
        Helpers::load('output')->error(429);
    }
}


?>
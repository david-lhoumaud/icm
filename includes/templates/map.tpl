<?php
/**
 * Classe de mappage /**TYPE*/.
 * 
 * @return stdClass
 */
class /**ClassName*/ extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='/**for*/';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas//**type*/
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas//**type*/.
	 * 
	 * @return array
	 */
	public function __use(){
		/**use_datas*//**end_datas*/
		return $this->payload();
	}
	
	/**
	 * Post-traitement du payload datas//**type*/.
	 * 
	 * @param array $payload Payload transitoire
	 * @return array
	 */
	public function __end($payload) {
		$this->payload = $payload;
		return $this->payload();
	}
	
}

?>

<?php
/**
 * Classe de routage /**METHOD*/.
 * 
 * @return stdClass
 */
class /**ClassName*/ extends Controller {
	
	/** 
	 * Permission du routage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='/**for*/';
	
	public function __construct(){/**bootstrap*//**js*//**view*/}
	
	/**
	 * Initialisation de la route.
	 * 
	 * @return void
	 */
	public function __init(){/**bootstrap_init*/}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){
		/**bootstrap_head*/
	}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){
		/**bootstrap_body*/
	}
	
	/**bootstrap_alert*/
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		/**OUTPUT*/
	}

}

?>

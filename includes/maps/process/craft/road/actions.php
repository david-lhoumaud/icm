<?php
/**
 * Classe de mappage PROCESS ProcessCraftRoadActions.
 * 
 * @return stdClass
 */
class ProcessCraftRoadActions extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/process
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas/process.
	 * 
	 * @return array
	 */
	public function __use(){
		switch($this->payload('request/params/get/action')){
			case 'add':
				$this->add();
				$this->payload('datas/process', $this);
			break;
			case 'del':
				$this->payload('datas/process', 'del');
			break;
		}
		return $this->payload();
	}

	private function add() {
		$this->inc_html=$this->payload('request/params/get/inchtml');
		$this->use_bootstrap=($this->inc_html!='none');
		$this->inc_js=$this->payload('request/params/get/incjs');
		$this->inc_view=$this->payload('request/params/get/incview');
		$this->method=$this->payload('request/params/get/method');
		$this->path=$this->payload('request/params/get/path');
		$this->class_for=$this->payload('request/params/get/for');
		$this->rootpath='includes/roads/'.$this->method;
		$this->ClassName=Loader::classpath_to_classname($this->path, $this->method);
		$this->file_destination=$this->rootpath.'/'.$this->path.'.php';
		$this->template='includes/templates/road.tpl';
		$this->st='Le routage';
		if (!file_exists($this->file_destination)){
			if (!file_exists(dirname($this->file_destination))) {
				mkdir(dirname($this->file_destination), 0777, true);
			}
			$ex=explode('/',dirname($this->path));
			$Perms=Helpers::load('perms');
			$Perms->set(0777,$this->rootpath);
			foreach($ex as $d){
				$this->rootpath.='/'.$d;
				$Perms->set(0777,$this->rootpath);
			}
			$Perms->set(0777,$this->rootpath);
			
			$this->toFile(
				$this->file_destination, 
				self::use_view(
					self::use_js(
						self::use_bootstrap(
							self::inject_vars()
						)
					)
				)
			);
			$Perms->set(0666,$this->file_destination);
			$this->payload('datas/output', $this->st.' '.$this->ClassName.' a bien été créé');
		} else {
			$this->payload('datas/output', $this->st.' '.$this->ClassName.' existe déjà');
		}
	}

	private function inject_vars() {
		$format='json';
		$enclosure_csv='"';
		$INformat='none';
		$OUTformat='payload';
		if ($INformat=='raws'){
			$din="\$this->payload('raws')";
			$isf='false';
		} else if ($INformat=='file'){
			$din="'files/input/'.\$this->payload('files/input')";
			$isf='true';
		} else if ($INformat=='url'){
			$din="\$this->urlStream(\$this->payload('files/input'))";
			$isf='false';
		} else {
			$din="''";
			$isf='false';
		}
		$sep_csv=';';
		if (preg_match('/csv/', $format)){
			if (preg_match('/_v/', $format)) $sep_csv=',';
			else if (preg_match('/_tab/', $format)) $sep_csv='	';
			else if (preg_match('/_b/', $format)) $sep_csv='|';
			else if (preg_match('/_u/', $format)) $sep_csv='_';
			else if (preg_match('/_t/', $format)) $sep_csv='-';
			$format='csv';
		}
		$file_match=preg_replace(array('/^includes\/matchs\//', '/\.json$/'),'',($this->type=='output'?'to/':'from/'));
		return str_replace(
			[
				'/**TYPE*/', 
				'/**METHOD*/', 
				'/**type*/', 
				'/**method*/', 
				'/**ClassName*/', 
				'/**for*/',
				'/**use_datas*/',
				'/**end_datas*/',
			],
			[
				strtoupper($this->type??'').' '.$this->ClassName, 
				strtoupper($this->method??'').' '.$this->ClassName, 
				$this->type??'', 
				$this->method??'', 
				$this->ClassName, 
				$this->class_for,
				($this->type=='input'?
					"\$this->payload(
			'datas/input', 
			\$this->".$format."(
				$din"
				.($format=='csv'?",\n\t\t\t\t'".$sep_csv."',\n\t\t\t\t'".$enclosure_csv."',\n\t\t\t\ttrue":'')
				.($format=='json'?",\n\t\t\t\tfalse":'')
				.",\n\t\t\t\t".$isf."
			)
		);"
				:
					"\$map=\$this->map('".$file_match."');
		\$datas=array();
		foreach (\$this->payload('datas/".($this->type=='output'?'process':'input')."') as \$index => \$cols) {
			\$object=\$this->match(\$cols, \$map);
			\$datas[]=\$object;
		}
		\$this->payload('datas/".($this->type??'output')."', \$datas);"
				),
				($this->type=='output' && $OUTformat=='file'?
					"\n\t\t\$this->toFile(
			'files/output/'.\$this->payload('files/output'), 
			\$this->to".ucfirst($format)."(
				\$this->payload('datas/output')"
				.($format=='csv'?",\n\t\t\t\t'$sep_csv'":'')."
			)
		);"
				:''),
			],
			(!empty($this->template)?file_get_contents($this->template):$this->toJson($this->payload('datas/input')))
		);
	}

	private function use_bootstrap($tpl) {
		if ($this->use_bootstrap){
			if ($this->inc_html=='html'){
				$result=self::use_html($tpl);
			} else {
				$result=str_replace(
					['/**bootstrap*/', '/**bootstrap_head*/', '/**bootstrap_body*/', '/**bootstrap_alert*/', '/**OUTPUT*/', '/**bootstrap_init*/'],
					[
						"\n\t\t\$this->html=Helpers::load('bootstrap');\n\t",
						"\$this->payload('datas/output/head',\n\t\t\t\$this->html->title('ICM - ".$this->ClassName."')\n\t\t\t.\$this->html->load()\n\t\t);\n\t\t\$this->session->access(true);",
						"\$content=self::__alert()
		.\$this->html->container(\n".self::tab(3)."'Contenu de ".$this->ClassName." ...'\n".self::tab(2).");\n".self::tab(2)."\$this->payload('datas/output/body', \$content);",
						"/**
	* afficher les alertes bootstrap.
	* 
	* @return string
	*/
	private function __alert(\$ms=5000){
		\$result='';
		if (\$this->session->get('alert-success')) {
			\$result.=\$this->html->alert('success', \$this->session->get('alert-success'), 'style=\"z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;\"', \$ms);
			\$this->session->set('alert-success', false);
		}
		if (\$this->session->get('alert-error')) {
			\$result.=\$this->html->alert('danger', \$this->session->get('alert-error'), 'style=\"z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;\"', \$ms);
			\$this->session->set('alert-error', false);
		}
		if (\$this->session->get('alert-warning')) {
			\$result.=\$this->html->alert('warning', \$this->session->get('alert-warning'), 'style=\"z-index:10000;position:fixed;width:75%;top:1%;left:12.5%;\"', \$ms);
			\$this->session->set('alert-warning', false);
		}
		return \$result;
	}",
						"\$this->Output->html(
			\$this->html->template(
				\$this->html->head(
					\$this->payload('datas/output/head')
				), 
				\$this->html->body(
					\$this->payload('datas/output/body'), 
					'light'
				),
				false
			)
		);",
			"\n\t\t\$this->session->set('error-location','".basename($this->path)."');\n\t"
					],
					$tpl
				);
			}
		} else {
			$result=str_replace(
				['/**bootstrap*/', '/**bootstrap_head*/', '/**bootstrap_body*/', '/**bootstrap_alert*/', '/**OUTPUT*/', '/**bootstrap_init*/'],
				[
					'','','', '', "\t\t\$this->Output->json(\$this->payload());", ''
				],
				$tpl
			);
		}
		return $result;
	}

	private function use_html($tpl) {
		$result=str_replace(
			['/**bootstrap*/', '/**bootstrap_head*/', '/**bootstrap_body*/', '/**bootstrap_alert*/', '/**OUTPUT*/', '/**bootstrap_init*/'],
			[
				"\n\t\t\$this->html=Helpers::load('html');\n\t",
				"\$this->payload('datas/output/head',\n\t\t\t\$this->html->title('ICM - ".$this->ClassName."')\n\t\t);\n\t\t\$this->session->access(true);",
				"\$content=\$this->html->div(\n".self::tab(3)."'Contenu de ".$this->ClassName." ...'\n".self::tab(2).");\n".self::tab(2)."\$this->payload('datas/output/body', \$content);",
				"",
				"\$this->Output->html(
		\$this->html->template(
			\$this->payload('datas/output/head'), 
			\$this->payload('datas/output/body')
		)
	);",
	"\n\t\t\$this->session->set('error-location','".basename($this->path)."');\n\t"
			],
			$tpl
		);
		return $result;
	}

	private function use_js($tpl) {
		if ($this->inc_js!='none'){
			$result=str_replace(
				['/**js*/'],
				[($this->use_bootstrap?"\t":"\n\t\t")."\$this->js=Helpers::load('js');\n\t"],
				$tpl
			);
		} else {
			$result=str_replace(
				['/**js*/'],
				[''],
				$tpl
			);
		}
		return $result;
	}
	
	private function use_view($tpl) {
		if ($this->inc_view!='none' && $this->use_bootstrap){
			$result=str_replace(
				['/**view*/'],
				["\t\$this->view=Views::load('".str_replace(['includes/views/', '.php'],'',$this->inc_view)."', \$this->html);\n\t"],
				$tpl
			);
		} else {
			$result=str_replace(
				['/**view*/'],
				[''],
				$tpl
			);
		}
		return $result;
	}

	/**
	 * Retourne un nombre voulu de tabulation
	 * 
	 * @param integer $num nombre de tabulation
	 * @return string
	 */
	private function tab($num=1){
		$tab='';
		for ($i=0; $i<$num; $i++) $tab.="\t";
		return $tab;
	}
	
	/**
	 * Post-traitement du payload datas/process.
	 * 
	 * @param array $payload Payload transitoire
	 * @return array
	 */
	public function __end($payload) {
		$this->payload = $payload;
		return $this->payload();
	}
	
}

?>

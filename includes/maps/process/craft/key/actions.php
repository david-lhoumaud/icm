<?php
/**
 * Classe de mappage PROCESS ProcessCraftKeyActions.
 * 
 * @return stdClass
 */
class ProcessCraftKeyActions extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/process
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas/process.
	 * 
	 * @return array
	 */
	public function __use(){
		$map=$this->map('from/drupal/v9/articles');
		$datas=array();
		foreach ($this->payload('datas/input') as $index => $cols) {
			$object=$this->match($cols, $map);
			$datas[]=$object;
		}
		$this->payload('datas/process', $datas);
		return $this->payload();
	}
	
	/**
	 * Post-traitement du payload datas/process.
	 * 
	 * @param array $payload Payload transitoire
	 * @return array
	 */
	public function __end($payload) {
		$this->payload = $payload;
		return $this->payload();
	}
	
}

?>

<?php
/**
 * Classe de mappage PROCESS ProcessDrupalV9.
 * 
 * @return stdClass
 */
class ProcessDrupalV9FunctionalTests extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/process
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload 	= $payload;
		parent::__construct();
		$this->ws		= Interco::load('drupal/v9/functionalTests', [$this->Curl, $this->Log, $this->Parse]);
	}
	
	/**
	 * Lance le chargement du payload datas/process.
	 * 
	 * @return array
	 */
	public function __use(){
		if ($this->payload('request/method')=='cmd') {
			$this->ws->is_cmd();
			$this->Log->i('Functional tests for Drupal 9');		
		}

		$this->payload('datas/process',[
			'index'						=> $this->ws->index(),
			'forbidden'					=> $this->ws->forbidden(),
			'author'					=> $this->ws->author(),
			'login'						=> $this->ws->login(),
			'add_node_blog'				=> $this->ws->add_node_blog(),
			'edit_node_blog'			=> $this->ws->edit_node_blog(),
			'add_image_node_blog'		=> $this->ws->add_image_node_blog(),
			'delete_node_blog'			=> $this->ws->delete_node_blog(),
			'add_node_bloc_wysiwyg'		=> $this->ws->add_node_bloc_wysiwyg(),
			'edit_node_bloc_wysiwyg'	=> $this->ws->edit_node_bloc_wysiwyg(),
			'delete_node_bloc_wysiwyg'	=> $this->ws->delete_node_bloc_wysiwyg(),
			'add_node_page'				=> $this->ws->add_node_page(),
			'edit_node_page'			=> $this->ws->edit_node_page(),
			'delete_node_page'			=> $this->ws->delete_node_page()
		]);

		if ($this->payload('request/method')=='cmd') {
			$success=0;
			$errors=0;
			$total=count($this->payload('datas/process'));
			foreach ($this->payload('datas/process') as $key => $value){
				if ($value['state']??false) $success++;
				else $errors++;
			}
			$this->Log->i(
				'Success: '.$this->Log::TEXT_SUCCESS.$success.$this->Log::TEXT_INFO.'/'.$this->Log::TEXT_DEBUG.$total
				.$this->Log::TEXT_INFO
				.' Errors: '.$this->Log::TEXT_ERROR.$errors.$this->Log::TEXT_INFO.'/'.$this->Log::TEXT_DEBUG.$total
			);
			exit();
		}
		
		return $this->payload();
	}

	/**
	 * Post-traitement du payload datas/process.
	 * 
	 * @param array $payload Payload transitoire
	 * @return array
	 */
	public function __end($payload) {
		$this->payload = $payload;
		return $this->payload();
	}
	
}

?>

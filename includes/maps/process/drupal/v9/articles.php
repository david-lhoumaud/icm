<?php
/**
 * Classe de mappage PROCESS ProcessDrupalV9.
 * 
 * @return stdClass
 */
class ProcessDrupalV9Articles extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/process
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload 	= $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas/process.
	 * 
	 * @return array
	 */
	public function __use(){
		$list=preg_split(
			'/<\/a> <a href="(.*)" class="card ">/', 
			preg_replace(
				'/<a href="(.*)" class="card ">/', 
				'<a href="\1" class="card "><url>\1</url>', 
				explode(
					'<div class="display-edit-link" data-uuid="ab097ad6-3306-4659-81c6-a5ae58e627c3">', 
					explode(
						'<div class="cards-container">', 
						$this->payload('datas/input')
					)[1]??''
				)[0]
			)
		);
		$final=[];
		foreach ($list as $index => $value) {
			$title=explode('</h2>', explode('<h2 class="medium-title">', $value)[1]??'')[0];
			$final[$title]=[
				'url' => explode('</url>', explode('<url>', $value)[1]??'')[0],
				'img' => explode('" alt="">', explode('<img loading="lazy" src="', $value)[1]??'')[0],
				'desc' => explode("</p>\n</div>\n", explode('<p class="desc">', $value)[1]??'')[0],
				'read-time' => explode("\n</p>\n<h2", explode('<p class="read-time">', $value)[1]??'')[0],
			];
		}
		$this->payload('datas/process', $final);

		
		return $this->payload();
	}

	/**
	 * Post-traitement du payload datas/process.
	 * 
	 * @param array $payload Payload transitoire
	 * @return array
	 */
	public function __end($payload) {
		$this->payload = $payload;
		return $this->payload();
	}
	
}

?>

<?php
/**
 * Classe de mappage Output pour une sortie du flux standard.
 * 
 * @return stdClass
 */
class OutputStandard extends Payloader {

	/** 
	 * Permission du mappage 
	 * @var string $for 
	 */
	public $for='public';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/output
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}

	/**
	 * Lance le chargement du payload datas/output.
	 * 
	 * @return arra Retourne le Payload
	 */
	public function __use(){
		$this->payload('datas/output',$this->payload('datas/process'));
		return $this->payload;
	}
	
	/**
	 * Post-traitement du payload datas/output.
	 * 
	 * @param array $payload Payload transitoire
	 * @return array Retourne le Payload
	 */
	public function __end($payload) {
		$this->payload = $payload;
		return $this->payload;
	}
}

?>

<?php
Helpers::load('vendor');
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Style\Border;
// use PhpOffice\PhpSpreadsheet\Style\Fill;
// use PhpOffice\PhpSpreadsheet\Style\Style;
// use PhpOffice\PhpSpreadsheet\Style\Alignment;
// use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as ReadXlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as WriteXlsx;

/**
 * Classe de mappage Output pour une sortie du flux standard.
 * 
 * @return stdClass
 */
class OutputTestComposer extends Payloader {

	/** 
	 * Permission du mappage 
	 * @var string $for 
	 */
	public $for='public';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/output
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}

	/**
	 * Lance le chargement du payload datas/output.
	 * 
	 * @return arra Retourne le Payload
	 */
	public function __use(){

		$inputFileName=$this->payload('request/host')."/files/input/cdiscount/template-update.xlsx";
		if (!file_exists($inputFileName)){
			echo 'no file';
			exit;
		}
		$reader = new Excel2007_NonStandardNamespacesWorkaround();
		// $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
		// $reader->setReadDataOnly(true);
		// $reader->setLoadAllSheets();
		// var_dump($reader->listWorksheetInfo($inputFileName));
		$spreadsheet = $reader->load($inputFileName);
		$sheet = $spreadsheet->getActiveSheet();
		// var_dump($sheet);

		$writer = new WriteXlsx($spreadsheet);
		//$writer->setPreCalculateFormulas(false);
		$writer->save(__DIR__.'/cdiscount.xlsx');
		$this->payload('datas/output', $sheet);
		return $this->payload;
	}
	
	/**
	 * Post-traitement du payload datas/output.
	 * 
	 * @param array $payload Payload transitoire
	 * @return array Retourne le Payload
	 */
	public function __end($payload) {
		$this->payload = $payload;
		return $this->payload;
	}
}

class Excel2007_NonStandardNamespacesWorkaround extends ReadXlsx
{
    public function __construct()
    {
        parent::__construct();
        $this->securityScanner->setAdditionalCallback([self::class, 'securityScan']);
    }

    public static function securityScan($xml)
    {
        return str_replace(
            [
                '<x:',
                '</x:',
                /*':x=',*/
                '<d:',
                '</d:'
                /*, ':d='*/
            ],
            [
                '<',
                '</',
                /*'=',*/
                '<',
                '</'
                /*, '='*/
            ],
            $xml
        );
    }
}

?>

<?php
/**
 * Classe de mappage INPUT InputUrlCsvbv.
 * 
 * @return stdClass
 */
class InputUrlCsvBV extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='public';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/input
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas/input.
	 * 
	 * @return array
	 */
	public function __use(){
		$this->payload(
			'datas/input', 
			$this->csv(
				utf8_encode($this->removeBOM($this->urlStream($this->payload('files/input')))),
				'|',
				'"',
				true,
				false
			)
		);
		return $this->payload();
	}
	

	
}

?>

<?php
/**
 * Classe de mappage INPUT InputUrlCsv.
 * 
 * @return stdClass
 */
class InputUrlJson extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='public';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/input
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas/input.
	 * 
	 * @return array
	 */
	public function __use(){
		$this->payload(
			'datas/input', 
			$this->json(
				$this->urlStream($this->payload('files/input')),
				false,
				false
			)
		);
		return $this->payload();
	}
	

	
}

?>

<?php
/**
 * Classe de mappage INPUT InputUrlCsv.
 * 
 * @return stdClass
 */
class InputUrlXlsx extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='public';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/input
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas/input.
	 * 
	 * @return array
	 */
	public function __use(){
		$fd='files/input/'.basename($this->payload('files/input'));
		if (!file_exists($fd)) mkdir($fd);
		$fn=$fd.'.zip';
		$this->toFile($fn, $this->urlStream($this->payload('files/input')));
		$zip = new ZipArchive;
		if ($zip->open($fn) === TRUE) {
				$zip->extractTo($fd.'/');
				$zip->close();
				unlink($fn);
				$this->payload('datas/input', $this->file($fd.'/xl/worksheets/sheet1.xml'));
		} else {
			$this->payload('code',7001);
			$this->payload('error/zip','Erreur à l\'extraction de l\'archive '.$fn);
		}
		return $this->payload();
	}
	

	
}

?>

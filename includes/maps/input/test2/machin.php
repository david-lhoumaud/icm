<?php
/**
 * Classe de mappage INPUT InputTest2Machin.
 * 
 * @return stdClass
 */
class InputTest2Machin extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/input
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas/input.
	 * 
	 * @return array
	 */
	public function __use(){
		$aaa= Matchs::load('drupal/v9/articles');
		$this->payload(
			'datas/input', 
			$this->json(
				'',
				false,
				false
			)
		);
		return $this->payload();
	}
	
	/**
	 * Post-traitement du payload datas/input.
	 * 
	 * @param array $payload Payload transitoire
	 * @return array
	 */
	public function __end($payload) {
		$this->payload = $payload;
		return $this->payload();
	}
	
}

?>

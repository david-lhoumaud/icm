<?php
/**
 * Classe de mappage INPUT InputRawsCsv.
 * 
 * @return stdClass
 */
class InputRawsCsv extends Payloader {

	/** 
	 * Permission du mappage.
	 * root, public, ...
	 * @var array|string $for 
	 */
	protected $for='root';
	
	/**
	 * Initialisation du payload.
	 * Le payload est datas/input
	 * 
	 * @param array $payload Payload transitoire
	 * @return void
	 */
	public function __construct($payload) {
		$this->payload = $payload;
		parent::__construct();
	}
	
	/**
	 * Lance le chargement du payload datas/input.
	 * 
	 * @return array
	 */
	public function __use(){
		$this->payload(
			'datas/input', 
			$this->csv(
				$this->payload('raws'),
				';',
				'"',
				true,
				false
			)
		);
		return $this->payload();
	}
	
}

?>

<?php
class HelpersCore {

	private $webservice_repository;
	private $log;

	public function __construct($webservice_repository='/API/'){
		$this->webservice_repository=$webservice_repository;
		$this->log="";
		return;
	}

	public function typeof($var) {
		return gettype($var);
	}

	public function setWebServiceRepository($webservice_repository='/API/'){
		$this->webservice_repository=$webservice_repository;
	}

	public function getWebServiceClassName(){
		return ucfirst(
			explode(
				'/',
				preg_replace(
					'#^'.$this->webservice_repository.'#',
					'',
					$_SERVER['REQUEST_URI']
				)
			)[1]
		);
	}

	public function getRawData() {
		return json_decode(
			base64_decode(
				file_get_contents("php://input")
			),
			true
		);
	}

	public function log($msg, $clean=false){
		$return="\033[0m";
		if ($clean) self::cleanLineInStdout();
		else $return="\033[0m\n";
		$this->log=$msg.$return;
		echo $this->log;
	}

	private function cleanLineInStdout(){
		for ($z=0; $z<strlen($this->log); $z++) echo chr(8);
	}

	public function dateCompare($date1_, $date2_=NULL){
		if (is_null($date2_)) $date2=strtotime('now');
		else $date2=strtotime($date2_);
		$date1=strtotime($date1_);
		if ($date1<$date2) {
			return [-1, $date2-$date1, 'lt'];
		}else if ($date1>$date2) {
			return [1, $date1-$date2, 'gt'];
		}
		return [0, 0, 'eq'];
	}

}
?>

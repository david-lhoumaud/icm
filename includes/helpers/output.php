<?php
/**
 * Helpers de gestion de la sortie HTTP
 * 
 * @return stdClass
 */
class HelpersOutput {
	/**
	 * Initialise la gestion de la sortie HTTP
	 * 
	 * @param boolean $display_error Afficher ou non les erreurs PHP
	 * @return void
	 */
	public function __construct($display_error=false) {
		ini_set('display_errors', ($display_error?1:0));
		error_reporting(($display_error?-1:0));
	}
	
	/**
	 * Affiche les erreurs PHP
	 * 
	 * @return void
	 */
	public function report($actif=true) {
		ini_set('display_errors', ($actif?1:0));
		error_reporting(($actif?-1:0));
	}
	
	public function contentDisposition($params=null){
		if (!is_null($params)){
			$contentDispositionField = 'Content-Disposition: '.$params;
			header($contentDispositionField);
		}
	}
	
	public function contentDispositionFilename($fn=''){
		$contentDispositionField = 'Content-Disposition: attachment; '
			. sprintf('filename="%s"; ', rawurlencode(basename($fn)))
			. sprintf("filename*=utf-8''%s", rawurlencode(basename($fn)));
		
		header($contentDispositionField);
	}

	/**
	 * Sortie commande
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function cmd($data='') {
		if (!empty($data)) echo $data;
		exit(0);
	}

	/**
	 * Sortie HTTP en JSON
	 * 
	 * @param mixed $data Donnée à afficher
	 * @param boolean $pretty Formatte le JSON en mode lisible
	 * @return void
	 */
	public function json($data, $pretty=false) {
		$show_header=false;
		if (is_string($data) || is_array($data)) {
			$_d=(is_string($data)?json_decode($data, true):$data);
			if ($_d['request']['method']!='cmd') $show_header=true;
			if (self::client_is_root($_d) && isset($_d['client']['limit-output'])) {
				$pretty=true;
			}
		}
		$data=self::clean_root_datas($data);
		if ($show_header) header('Content-Type: application/json');
		echo $pretty?json_encode($data, JSON_PRETTY_PRINT):json_encode($data);
		exit(0);
	}

	/**
	 * Sortie HTTP en Text
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function txt($data) {
		$data=self::clean_root_datas($data);
		if (is_array($data)) {
			header('Content-Type: application/json');
			echo self::json($data);
		} else {
			header('Content-Type: text/plain');
			echo $data;
		}
		exit(0);
	}

	/**
	 * Sortie HTTP en CSV Text
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function csv($data, $name='') {
		$data=self::clean_root_datas($data);
		if (is_array($data)) {
			header('Content-Type: application/json');
			echo self::json($data);
		} else {
			header('Content-Type: text/csv');
			if (!empty($name)){
				$this->contentDispositionFilename($name);
			}
			echo $data;
		}
		exit(0);
	}

	/**
	 * Sortie HTTP en Dat Text
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function dat($data) {
		$data=self::clean_root_datas($data);
		header('Content-Type: text/plain');
		echo serialize($data);
		exit(0);
	}

	/**
	 * Sortie HTTP en HTML
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function html($data) {
		$data=self::clean_root_datas($data);
		header('Content-Type: text/html');
		echo $data;
		exit(0);
	}
	
	/**
	 * Sortie HTTP en HTML
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function xml($data) {
		$data=self::clean_root_datas($data);
		header('Content-Type: application/xml');
		echo $data;
		exit(0);
	}

	/**
	 * Sortie HTTP en Base64 sans Entête
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function base64($data) {
		$data=self::clean_root_datas($data);
		header('Content-Type: text/plain');
		if (is_array($data)) {
			echo base64_encode(json_encode($data));
		}else echo base64_encode($data);
		exit(0);
	}

	/**
	 * Sortie HTTP en MD5 sans entête
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function md5($data) {
		$data=self::clean_root_datas($data);
		header('Content-Type: text/plain');
		if (is_array($data)) {
			echo md5(json_encode($data));
		}else echo md5($data);
		exit(0);
	}

	/**
	 * Sortie HTTP en SHA1 sans entête
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function sha1($data) {
		$data=self::clean_root_datas($data);
		header('Content-Type: text/plain');
		if (is_array($data)) {
			echo sha1(json_encode($data));
		}else echo sha1($data);
		exit(0);
	}

	/**
	 * Sortie HTTP en Text sinon en JSON si c'est $data est un tableau
	 * 
	 * @param mixed $data Donnée à afficher
	 * @return void
	 */
	public function standard($data, $ctype='') {
		$data=self::clean_root_datas($data);
		if (is_array($data)) {
			header('Content-Type: application/json');
			
			echo self::json($data);
		}else {
			if (!empty($ctype)) header('Content-Type: '.$ctype);
			echo $data;
		}
		exit(0);
	}

	/**
	 * Sortie HTTP des erreur HTTP
	 * 
	 * @param integer $code code HTTP de la réponse
	 * @param string $msg Message à afficher
	 * @param string $details ajout de détail à l'erreur
	 * @return void
	 */
	public function error($code, $msg=null, $details=null) {
		switch($code){
			case 400:
				$msg='Bad Request';
			break;
			case 401:
				$msg='Unauthorized';
			break;
			case 403:
				$msg='Forbidden';
			break;
			case 404:
				$msg='Not Found';
			break;
			case 429:
				$msg='Too Many Requests';
			break;
			case 498:
				$msg='Token expired/invalid';
			break;
			case 500:
				$msg='Internal Server Error';
			break;
			case 501:
				$msg='Not Implemented';
			break;
			case 502:
				$msg='Bad Gateway';
			break;
			case 503:
				$msg='Service Unavailable';
			break;
			case 504:
				$msg='Gateway Time-out';
			break;
			case 520:
				$msg='Unknown Error';
			break;
		}
		header("HTTP/1.1 $code $msg");
		if (isset($_SESSION) && isset($_SESSION['error-location']) && !empty($_SESSION['error-location'])){
			$error_location=$_SESSION['error-location'];
			unset($_SESSION['error-location']);
			$_SESSION['error']=array(
				'code'=>$code,
				'message'=>$msg
			);
			if ($code==401 || $code==403) $error_location=($_SERVER['REQUEST_SCHEME']??'http').'://'.$_SERVER['HTTP_HOST'].'/root/login';
			self::clean_access();
			header('Location: '.$error_location);
			exit($code);
		}
		header('Content-Type: application/json');
		$output=array(
				'error'=>$code,
				'message'=>$msg
		);
		if (!is_null($details))$output['details']=$details;
		echo json_encode($output);
		exit($code);
	}
	
	/**
	 * Supprime les variables ayant le token ACCESS dans leur clé.
	 * 
	 * @return void
	 */
	private function clean_access(){
		$ia=$_SESSION['__icm_access__']??'__none_icm_access__';
		foreach ($_GET as $key=>$value){
			if (preg_match('/'.$ia.'/',$key)) unset($_GET[$key]);
		}
		foreach ($_POST as $key=>$value){
			if (preg_match('/'.$ia.'/',$key)) unset($_POST[$key]);
		}
		foreach ($_COOKIE as $key=>$value){
			if (preg_match('/'.$ia.'/',$key)) unset($_COOKIE[$key]);
		}
		foreach ($_SESSION as $key=>$value){
			if (preg_match('/'.$ia.'/',$key)) unset($_SESSION[$key]);
		}
	}
	
	private function clean_root_datas($datas){
		if (is_string($datas) || is_array($datas)) {
			$_d=(is_string($datas)?json_decode($datas, true):$datas);
			if (is_array($_d)){
				if (isset($_d['config'])) unset($_d['config']);
				if (isset($_d['server'])) unset($_d['server']);
				if (isset($_d['maps'])) unset($_d['maps']);
				if (isset($_d['files'])) unset($_d['files']);
				if (isset($_d['raws'])) unset($_d['raws']);
				$is_cmd=self::client_is_cmd($_d);
				if (isset($_d['request'])) unset($_d['request']);
				if (!$is_cmd){
					//if (!self::client_is_root($_d) && isset($_d['raws'])) unset($_d['raws']);
					if (!self::client_is_root($_d) && isset($_d['datas']) && isset($_d['datas']['input'])) unset($_d['datas']['input']);
					if (!self::client_is_root($_d) && isset($_d['datas']) && isset($_d['datas']['process'])) unset($_d['datas']['process']);
				}
				
				if (isset($_d['client'])) {
					if (isset($_d['client']['limit-output'])) {
						if ($_d['client']['limit-output']=='input'){
							unset($_d['datas']['process']);
							unset($_d['datas']['output']);
						} else if ($_d['client']['limit-output']=='process'){
							unset($_d['datas']['input']);
							unset($_d['datas']['output']);
						} else if ($_d['client']['limit-output']=='output'){
							unset($_d['datas']['input']);
							unset($_d['datas']['process']);
						}
					}
					unset($_d['client']);
				}
				$datas=$_d;
			}
		}
		return $datas;
	}
	
	private function client_is_cmd($datas){
		if (isset($datas['request']) && isset($datas['request']['method']) && $datas['request']['method']=='cmd') return true;
		return false;
	}
	
	private function client_is_root($datas){
		if (isset($datas['client']) && isset($datas['client']['is']) && $datas['client']['is']=='root') {
			return true;
		}
		return false;
	}
}
?>

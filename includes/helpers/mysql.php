<?php
/**
 * Helpers de gestion de la base de donnée MYSQL
 * 
 * @return stdClass
 */
class HelpersMysql {

	const HOST	='';
	const USER	='';
	const PASS	='';
	const DBNAME='';

	private $host;
	private $user;
	private $pass;
	public $dbname;
	private $mysql;
	private $request;
	private $_request;
	public $params;
	public $tpl;

	public function __construct($params=NULL) {
		$this->host		=	(!is_null($params)?$params['host']:self::HOST);
		$this->user		=	(!is_null($params)?$params['user']:self::USER);
		$this->pass		=	(!is_null($params)?$params['pass']:self::PASS);
		$this->dbname	=	(!is_null($params)?$params['dbname']:self::DBNAME);
		$this->mysql	=	NULL;
		$this->request=	NULL;
		$this->_request=	NULL;
		$this->params	= array();
		$this->tpl=Helpers::load('mysql/template');
		if (isset($params['connect']) && !$params['connect']) return;
		if (!empty($this->host) && !empty($this->user) && !empty($this->pass)){
			$this->connect();
		}
		return;
	}

	private function connect($create=true) {
		try {
			$this->mysql = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname, $this->user, $this->pass);
		} catch (PDOException $e) {
			if (preg_match('/Unknown database \''.$this->dbname.'\'/',$e->getMessage())){
				try {
					$this->mysql = new PDO('mysql:host='.$this->host, $this->user, $this->pass);
				} catch (PDOException $e) {
					print "[Error] MYSQL - " . $e->getMessage() . "\n";
		    	die();
				}
				if ($create){
					self::query("CREATE DATABASE IF NOT EXISTS `".$this->dbname."` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;");
					self::connect(false);
				}
			} else {
				print "[Error] MYSQL - " . $e->getMessage() . "\n";
	    	die();
			}
		}
	}

	public function login(
		$host=self::HOST,
		$user=self::USER,
		$pass=self::PASS,
		$dbname=self::DBNAME,
		$create=true
	) {
		$this->host		=	$host;
		$this->user		=	$user;
		$this->pass		=	$pass;
		$this->dbname	=	$dbname;
		self::close();
		self::connect($create);
	}

	public function close() {
		$this->mysql = NULL;
	}

	public function fetch($request=NULL) {
		if (!is_null($request)) $this->request=$request;
		if (!is_null($this->request)){
			try {
	 			$result=$this->request->fetch(PDO::FETCH_ASSOC);
	 		} catch (PDOException $e) {
	     	print "[Error] MYSQL - " . $e->getMessage() . "\n";
	     	die();
	 		}
			return $result;
		}
	}

	public function fetchAll($request=NULL) {
		if (!is_null($request)) $this->request=$request;
		if (!is_null($this->request)) return $this->request->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function count($request=NULL) {
		if (!is_null($request)) $this->request=$request;
		if (!is_null($this->request)) return $this->request->rowCount();
	}
	
	public function colCount($request=NULL) {
		if (!is_null($request)) $this->request=$request;
		if (!is_null($this->request)) return $this->request->colCount();
	}
	
	public function code($request=NULL) {
		if (!is_null($request)) $this->request=$request;
		if (!is_null($this->request)) return $this->request->errorCode();
	}
	
	public function isError($request=NULL) {
		if (!is_null($request)) $this->request=$request;
		if (!is_null($this->request)) {
			if (!empty($this->code()) && $this->code() !='00000' && $this->code()!=0) {
				return true;
			}
		}
		return false;
	}
	
	
	
	public function error($request=NULL) {
		if (!is_null($request)) $this->request=$request;
		if (!is_null($this->request)) return $this->request->errorInfo();
	}
	
	public function lastInsertId(){
		return $this->mysql->lastInsertId();
	}

	public function query($query='', $params=NULL) {
		if (!is_null($params)) $this->params=$params;
		try {
			//echo str_replace(array('   ',' ', "\n", "\t"),array(' ',' ', '', ' '),$query)."\n";
			$this->request=$this->mysql->prepare(str_replace(array('   ',' ', "\n", "\t"),array(' ',' ', ' ', ' '),$query));
		} catch (PDOException $e) {
    	print "[Error] MYSQL Prepare - " . $e->getMessage() . "\n";
    	die();
		}
		try {
			if (count($this->params)>0)$this->request->execute($this->params);
			else $this->request->execute();

		} catch (PDOException $e) {
    	print "[Error] MYSQL Execute - " . $e->getMessage() . "\n";
    	die();
		}
		$this->_request=$this->request;
	}

	public function getParams() {
		return $this->params;
	}

	public function cleanParams() {
		$this->params	= array();
	}

	public function setParam($key, $value) {
		$this->params[':'.$key]=$value;
	}

	public function setParams($params=array()) {
		$this->params=$params;
	}

	/**
	 *  @desc TEMPLATE MYSQL REQUEST
	 */
	public function create($table_name='', $row=array(), $index=array()) {
		$this->query(
			$this->tpl->create($table_name, $row, $index)
		);
	}
	
	public function insert($table_name, $datas=array(), $where=array(), $others=array(), $limit=NULL) {
		$this->query(
			$this->tpl->insert($table_name, $datas, $where, $others, $limit)
		);
	}
	
	public function delete($table_name, $where=array(), $others=array(), $limit=NULL) {
		$this->query(
			$this->tpl->delete($table_name, $where, $others, $limit)
		);
	}

	public function select($table_name, $row=array(), $joint=array(), $where=array(), $others=array(), $limit=NULL) {
		$this->query(
			$this->tpl->select($table_name, $row, $joint, $where, $others, $limit)
		);
	}
}
?>

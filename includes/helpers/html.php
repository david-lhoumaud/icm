<?php
/**
 * Helpers de gestion d'affiche HTML standard
 * 
 * @return stdClass
 */
class HelpersHtml {

	public function __construct(){}
	
	protected function attributes($_att) {
		if (is_array($_att)){
			$_att_final='';
			foreach($_att as $a=>$v){
				$_att_final.=' '.$a.'="'.$v.'"';
			}
			return $_att_final;
		}
		return (!empty($_att)?' ':'').$_att;
	}

	public function html($content=''){
		return '<!DOCTYPE html><html>'.$content.'</html>';
	}

	public function head($content=''){
		return '<head>'.$content.'</head>';
	}

	public function title($content=''){
		return '<title>'.$content.'</title>';
	}

	public function body($content='', $attributes=''){
		return '<body'.self::attributes($attributes).'>'.$content.'</body>';
	}

	public function button($content='', $attributes=''){
		return '<button'.self::attributes($attributes).'>'.$content.'</button>';
	}
	
	public function div($content='', $attributes=''){
		return '<div'.self::attributes($attributes).'>'.$content.'</div>';
	}
	
	public function form($method='GET', $action='', $content='', $attributes=''){
		return '<form method="'.$method.'"'.(!empty($action)?' action="'.$action.'"':'').self::attributes($attributes).'>'.$content.'</form>';
	}
	
	public function p($content='', $attributes=''){
		return '<p'.self::attributes($attributes).'>'.$content.'</p>';
	}
	
	public function span($content='', $attributes=''){
		return '<span'.self::attributes($attributes).'>'.$content.'</span>';
	}
	
	public function b($content=''){
		return '<b>'.$content.'</b>';
	}
	
	public function strong($content=''){
		return '<strong>'.$content.'</strong>';
	}
	
	public function i($content=''){
		return '<i>'.$content.'</i>';
	}
	
	public function u($content=''){
		return '<u>'.$content.'</u>';
	}

	public function pre($content=''){
		return '<pre>'.$content.'</pre>';
	}
	
	public function ul($content='', $attributes=''){
		return '<ul'.self::attributes($attributes).'>'.$content.'</ul>';
	}
	
	public function li($content='', $attributes=''){
		return '<li'.self::attributes($attributes).'>'.$content.'</li>';
	}

	public function lis($content=[], $attributes=''){
		if (!is_array($content)) {
			return $this->li($content, $attributes);
		}

		$lis = '';

		foreach ($content as $li) {
			$lis .= $this->li($li, $attributes);
		}

		return $lis;
	}
	
	public function table($content='', $attributes=''){
		return '<table'.self::attributes($attributes).'>'.$content.'</table>';
	}
	
	public function tr($content='', $attributes=''){
		return '<tr'.self::attributes($attributes).'>'.$content.'</tr>';
	}
	
	public function td($content='', $attributes=''){
		return '<td'.self::attributes($attributes).'>'.$content.'</td>';
	}
	
	public function a($content='', $link=null, $attributes=''){
		return '<a'.(!is_null($link)?' href="'.$link.'"':'').self::attributes($attributes).'>'.$content.'</a>';
	}

	public function js($content=''){
		return '<script language="Javascript">'.$content.'</script>';
	}

	public function css($content=''){
		return '<style>'.$content.'</style>';
	}

	public function tag($tagname='p', $datas='', $attributes='', $unique=false){
		if ($unique) return '<'.$tagname.(!empty($datas)?' '.$datas:'').self::attributes($attributes).' />';
		else return '<'.$tagname.self::attributes($attributes).'>'.$datas.'</'.$tagname.'>';
	}

	public function input($type, $name='', $value='', $attributes=''){
		return '<input type="'.$type.'" name="'.$name.'" value="'.$value.'"'.self::attributes($attributes).' />';
	}

	public function select($name='', $options=array(), $attributes=''){
		$_opts='';
		if (is_array($options)){
			for($i=0;$i<count($options); $i++){
				$_opts.='<option value="'.($options[$i][1]??'').'"'.self::attributes($options[$i][2]??'').'>'.($options[$i][0]??'').'</option>';
			}
		} else {
			$_opts=$options;
		}
		return '<select name="'.$name.'"'.self::attributes($attributes).' >'
		.$_opts
		.'</select>';
	}
	
	public function option($val='', $content='', $attributes=''){
		return '<option value="'.$val.'"'.self::attributes($attributes).'>'.$content.'</option>';
	}
	
	
	public function img($src, $attributes=''){
		return '<img src="'.$src.'"'.self::attributes($attributes).' />';
	}
	
	public function image($src, $attributes=''){
		return self::img($src, $attributes);
	}

	public function br(){
		return '<br/>';
	}

	public function encode($content, $all=false){
		if (!$all) return htmlentities($content);
		else return htmlspecialchars($content);
	}

	public function decode($content){
		return html_entity_decode($content);
	}

	public function template($head, $body, $container=true){
		if ($container){
			return $this->html(
				$this->head(
					$head
				).
				$this->body(
					$body
				)
			);
		}
		return $this->html($head.$body);
	}

}
?>

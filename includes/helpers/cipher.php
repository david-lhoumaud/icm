<?php
/**
 * Helpers de chiffrement de donnée
 * 
 * @return stdClass
 */
class HelpersCipher {

	private $cipher='aes-128-gcm';
	private $key_size=2048;
	public $key;
	public $vector;
	public $tag;

	public function __construct($d=array('cipher'=>'aes-128-gcm', 'key_size'=>2048)){
		$this->cipher=$d['cipher'];
		$this->key_size=$d['key_size'];
		$this->key=$this->generate_key();
		$this->vector = $this->generate_vector();
	}
	
	public function generate_key($ks=null){
		if (is_null($ks)) return openssl_random_pseudo_bytes($this->key_size);
		else return openssl_random_pseudo_bytes($ks);
	}
	
	public function set_key($k){$this->key=$k;}
	
	public function generate_vector($c=null){
		if (!$this->cipher_exists()) return '';
		if (is_null($c)) $ivlen = openssl_cipher_iv_length($this->cipher);
		else $ivlen = openssl_cipher_iv_length($c);
		return openssl_random_pseudo_bytes($ivlen);
	}
	
	public function set_vector($v){$this->vector=$v;}
	
	public function set_tag($t){$this->tag=$t;}
	
	public function encrypt($plaintext='', $options=0){
		if (!$this->cipher_exists()) return '';
    $ret=openssl_encrypt($plaintext, $this->cipher, $this->key, $options, $this->vector, $tag);
		$this->tag=$tag;
		return $ret;
	}

	public function decrypt($ciphertext='', $options=0){
		if (!$this->cipher_exists()) return '';
		return openssl_decrypt($ciphertext, $this->cipher, $this->key, $options, $this->vector, $this->tag);
	}
	
	public function cipher_exists($c=null){
		if (is_null($c)) $cc=$this->cipher;
		else $cc=$c;
		if (in_array($cc, openssl_get_cipher_methods()))return true;
		return false;
	}

}

// $t1=new HelpersCipher();
// $text_encrypted=$t1->encrypt("salut le monde");
// $t=$t1->tag;
// $v=$t1->vector;
// $k=$t1->key;

// $t2=new HelpersCipher();
// $t2->tag=$t;
// $t2->vector=$v;
// $t2->key=$k;
// echo $t2->decrypt($text_encrypted);


?>

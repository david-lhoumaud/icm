<?php

class HelpersError {
	public $err;
	public function __construct(){
		$this->err=array();
	}

	public function error($errno, $errstr, $errfile, $errline){
		if (is_array($this->err)) 
		$this->err[$errfile][(string)$errline][(string)$errno][]=$errstr;
		else $this->err="$errstr à la ligne [$errline], du fichier $errfile : n°$errno";
	}

}
?>

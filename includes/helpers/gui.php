<?php
class HelpersGui {
	
	public $APP;
	public $APP_DIR;
	public $APP_LANG;

	public function __construct(){
		$this->APP_DIR = dirname(__DIR__);
		$this->APP_LANG='en-EN';
		return;
	}
	
	public function init($_APP){
		$this->APP=$_APP;
		return;
	}
	
	/**	
	 * array('click'=>array('call'=>'function','params'=>array))
	 */
	public function events($list=array()) {
		if (is_null($list)) return '';
		$evts='';
		foreach ($list as $event => $callback) {
			if (preg_match('/js::/', $event)) {
				$event=str_replace('js::', '', $event);
				$_callback=$callback[0];
				$script=str_replace(";\n", '; ', $this->APP->$_callback($callback[1]??array()));
				$evts.= "on".$event."=\"$script\" ";
			} else {
				$evts.= "on".$event."=\"__callback(this, '".$event."', '".$callback[0]."','".addslashes(json_encode($callback[1]??array()))."'); return false;\" ";
			}
		}
		return $evts;
	}
	
	public function start($title='Wui', $charset='utf-8', $lang='en-EN'){
		$this->APP_LANG=$lang;
		header('Content-Type: text/html; charset=utf-8');
		echo "\n";
		echo "<html lang=\"$this->APP_LANG\">".
			$this->head(
				$this->title($title).
				$this->charset($charset).
				$this->js(
					file_get_contents($this->APP_DIR.'/js/jquery-3.5.1.slim.min.js')."\n".
					file_get_contents($this->APP_DIR.'/js/popper.min.js')."\n".
					file_get_contents($this->APP_DIR.'/js/bootstrap.min.js')."\n".
					file_get_contents($this->APP_DIR.'/js/wui-events.js')
				).
				$this->css(
					file_get_contents($this->APP_DIR.'/css/bootstrap.min.css')."\n".
					file_get_contents($this->APP_DIR.'/css/wui-style.css')
				)
			).
			"<body>";
	}
	
	public function head($content=''){
		return "<head>".$content."</head>";
	}	
	
	public function title($content=''){
		return "<title>".$content."</title>";
	}	
	
	public function charset($charset='utf-8'){
		return "<meta charset=\"$charset\">";
	}
	
	public function css($content=''){
		return "<style>".$content."</style>";
	}	
	
	public function style($content=''){$this->css($content);}
	
	public function js($content=''){
		return "<script type=\"application/javascript\">".$content."</script>";
	}
	public function javascript($content=''){return $this->js($content);}
	
	public function div($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="container"';
		return "<div id=\"$id\" ".$this->events($handler)." $attr >".$content."</div>";
	}
	public function block($id, $handler=null, $content='', $attr=''){return $this->div($id, $handler, $content, $attr);}
	
	public function grid($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="container"';
		return "<div id=\"$id\" ".$this->events($handler)." $attr >".$content."</div>";
	}
	
	public function row($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="row"';
		return "<div id=\"$id\" ".$this->events($handler)." $attr >".$content."</div>";
	}
	
	public function col($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="col"';
		return "<div id=\"$id\" ".$this->events($handler)." $attr >".$content."</div>";
	}
	
	public function form($id, $method='post', $handler=null, $content='', $attr=''){
		return "<form id=\"$id\" method='$method' ".$this->events($handler)." $attr >".$content."</form>";
	}
	
	public function group($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-group"';
		return "<div id=\"$id\" ".$this->events($handler)." $attr >".$content."</div>";
	}
	
	public function button($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-default"';
		return "<button id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	public function btn($id, $handler=null, $content='', $attr=''){return $this->button($id, $handler, $content, $attr);}
	
	public function btnSuccess($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-success"';
		return "<button id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnPrimary($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-primary"';
		return "<button id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnInfo($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-info"';
		return "<button id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnWarning($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-warning"';
		return "<button id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnDanger($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-danger"';
		return "<button id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnLink($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-link"';
		return "<button id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function buttonForm($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-default"';
		return "<button type=\"submit\" id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	public function btnForm($id, $handler=null, $content='', $attr=''){return $this->buttonForm($id, $handler, $content, $attr);}
	
	public function btnFormSuccess($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-success"';
		return "<button type=\"submit\" id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnFormPrimary($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-primary"';
		return "<button type=\"submit\" id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnFormInfo($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-info"';
		return "<button type=\"submit\" id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnFormWarning($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-warning"';
		return "<button type=\"submit\" id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnFormDanger($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-danger"';
		return "<button type=\"submit\" id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnFormLink($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-link"';
		return "<button type=\"submit\" id=\"$id\" ".$this->events($handler)." $attr >".$content."</button>";
	}
	
	public function btnGroup($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn-group"';
		return "<div id=\"$id\" ".$this->events($handler)." $attr >".$content."</div>";
	}
	
	public function btnGroupVertical($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn-group-vertical"';
		return "<div id=\"$id\" ".$this->events($handler)." $attr >".$content."</div>";
	}
	
	public function nav($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="navbar w-100 navbar-default navbar-fixed-top bg-dark text-light" style="position:fixed;z-index:1000"';
		return "<nav id=\"$id\" ".$this->events($handler)." $attr ><div class=\"container-fluid\">".$content."</div></nav><div style=\"min-height:54px;\"></div>";
	}
	
	public function menu($id, $title='', $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="btn btn-link dropdown-toggle"';
		return "<div class=\"btn-group\" role=\"group\">".
				"<a id=\"$id\" ".$this->events($handler)." $attr data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">".
					"$title".
				"</a>".
				"<div class=\"dropdown-menu bg-dark text-light\" aria-labelledby=\"$id\">".
					"$content".
				"</div>".
			"</div>";
	}
	
	public function badge($id, $title='', $handler=null, $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="badge badge-primary badge-pill"';
		return "<span id=\"$id\" ".$this->events($handler)." $attr >$title</span>";
	}
	
	public function badgeSuccess($id, $title='', $handler=null, $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="badge badge-success badge-pill"';
		return "<span id=\"$id\" ".$this->events($handler)." $attr >$title</span>";
	}
	
	public function badgeSecondary($id, $title='', $handler=null, $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="badge badge-secondary badge-pill"';
		return "<span id=\"$id\" ".$this->events($handler)." $attr >$title</span>";
	}
	
	public function badgeWarning($id, $title='', $handler=null, $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="badge badge-warning badge-pill"';
		return "<span id=\"$id\" ".$this->events($handler)." $attr >$title</span>";
	}
	
	public function badgeDanger($id, $title='', $handler=null, $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="badge badge-danger badge-pill"';
		return "<span id=\"$id\" ".$this->events($handler)." $attr >$title</span>";
	}
	
	public function menuItem($id, $title='', $handler=null, $attr='', $link='#'){
		if (!preg_match('/class=/', $attr)) $attr.=' class="dropdown-item text-light"';
		return "<a id=\"$id\" ".$this->events($handler)." $attr href=\"$link\">$title</a>";
	}
	
	public function progress($id, $purcent='', $handler=null, $display=true, $bg=''){
		$attr=' class="progress-bar progress-bar-striped progress-bar-animated '.(!empty($bg)?'bg-'.$bg:'').'"';
		return "<div id=\"parent-".$id."\" class=\"progress\" ".($display?'':'style="display:none;"')."><div id=\"$id\" ".$this->events($handler)." $attr role=\"progressbar\" aria-valuenow=\"".$purcent."\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:".$purcent."%;\"></div></div>";
	}
	
	public function edit($id, $placeholder='', $handler=null, $value='',  $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<input type='text' name=\"$id\" id=\"$id\" placeholder='".$placeholder."' ".$this->events($handler)." value='".$value."' $attr >";
	}
	
	public function password($id, $placeholder='', $handler=null, $value='',  $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<input type='password' name=\"$id\" id=\"$id\" placeholder='".$placeholder."' ".$this->events($handler)." value='".$value."' $attr >";
	}
	
	public function tel($id, $placeholder='', $handler=null, $value='',  $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<input type='tel' name=\"$id\" id=\"$id\" placeholder='".$placeholder."' ".$this->events($handler)." value='".$value."' $attr >";
	}
	
	public function color($id, $placeholder='', $handler=null, $value='',  $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<input type='color' name=\"$id\" id=\"$id\" placeholder='".$placeholder."' ".$this->events($handler)." value='".$value."' $attr >";
	}
	
	public function date($id, $placeholder='', $handler=null, $value='',  $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<input type='date' name=\"$id\" id=\"$id\" placeholder='".$placeholder."' ".$this->events($handler)." value='".$value."' $attr >";
	}
	
	public function time($id, $placeholder='', $handler=null, $value='',  $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<input type='time' name=\"$id\" id=\"$id\" placeholder='".$placeholder."' ".$this->events($handler)." value='".$value."' $attr >";
	}
	
	public function email($id, $placeholder='', $handler=null, $value='',  $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<input type='email' name=\"$id\" id=\"$id\" placeholder='".$placeholder."' ".$this->events($handler)." value='".$value."' $attr >";
	}
	
	public function url($id, $placeholder='', $handler=null, $value='',  $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<input type='url' name=\"$id\" id=\"$id\" placeholder='".$placeholder."' ".$this->events($handler)." value='".$value."' $attr >";
	}
	
	public function hidden($id, $value=''){
		return "<input type='hidden' name=\"$id\" id=\"$id\" value='".$value."' >";
	}
	public function var($id, $value=''){
		return $this->hidden($id, $value);
	}
	
	public function checkbox($id, $label='', $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<div class=\"checkbox\"><label><input type='checkbox' name=\"$id\" id=\"$id\" ".$this->events($handler)." $attr > $content</label></div>";
	}
	
	public function radio($id, $label='', $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<div class=\"radio\"><label><input type='radio' name=\"$id\" id=\"$id\" ".$this->events($handler)." $attr > $content</label></div>";
	}
	
	public function label($id, $for='', $handler=null, $content='', $attr=''){
		return "<label for=\"$for\" ".$this->events($handler)." $attr >$content</label>";
	}
	
	public function small($id, $handler=null, $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-text text-muted"';
		return "<small ".$this->events($handler)." $attr >$content</small>";
	}
	
	public function text($id,  $handler=null,  $content='', $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<textarea name=\"$id\" id=\"$id\" ".$this->events($handler)." $attr >$content</textarea>";
	}
	
	public function img($id,  $src='', $handler=null, $attr=''){
		$ext_=explode('.', $src);
		$ext=$ext_[count($ext_)-1];
		return "<img src=\"data:image/".$ext.";base64, ".base64_encode(file_get_contents(getenv('PWD')."/app/".$src))."\" id=\"$id\" ".$this->events($handler)." $attr />";
	}
	public function image($id,  $src='', $handler=null, $attr=''){return $this->img($id,  $src, $handler, $attr);}
	
	public function file($id,  $title='', $handler=null, $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<div class=\"input-group\">
  <div class=\"input-group-prepend\">
    <span class=\"input-group-text\" id=\"file_wui_".$id."_title\" ".$this->events($handler)." ".$attr.">$title</span>
  </div>
	<div class=\"custom-file\">
		<input type=\"hidden\" name=\"file_wui_".$id."_value\" id=\"file_wui_".$id."_value\" value=\"\">
		<input type=\"text\" class=\"custom-file-input\" name=\"file_wui_".$id."_component\" id=\"file_wui_".$id."_component\" aria-describedby=\"file_wui_".$id."_title\" lang=\"".explode('-', $this->APP_LANG)[0]."\" onclick=\"file_component=document.getElementById('file_wui_".$id."_value');label_file_component=document.getElementById('file_wui_".$id."_label'); alert('dialog->file');\" >
    <label id=\"file_wui_".$id."_label\" class=\"custom-file-label\" for=\"file_wui_".$id."_component\" style=\"overflow:auto!important;\">Sélectionner un fichier</label>
  </div>
</div>";
	}
	
	public function directory($id,  $title='', $handler=null, $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<div class=\"input-group\">
  <div class=\"input-group-prepend\">
    <span class=\"input-group-text\" id=\"directory_wui_".$id."_title\" ".$this->events($handler)." ".$attr.">$title</span>
  </div>
	<div class=\"custom-file\">
		<input type=\"hidden\" name=\"directory_wui_".$id."_value\" id=\"directory_wui_".$id."_value\" value=\"\">
		<input type=\"text\" class=\"custom-file-input\" name=\"directory_wui_".$id."_component\" id=\"directory_wui_".$id."_component\" aria-describedby=\"directory_wui_".$id."_title\" lang=\"".explode('-', $this->APP_LANG)[0]."\" onclick=\"directory_component=document.getElementById('directory_wui_".$id."_value');label_directory_component=document.getElementById('directory_wui_".$id."_label'); alert('dialog->directory');\" >
    <label id=\"directory_wui_".$id."_label\" class=\"custom-file-label\" for=\"directory_wui_".$id."_component\" style=\"overflow:auto!important;\">Sélectionner un dossier</label>
  </div>
</div>";
	}
	public function folder($id,  $title='', $handler=null, $attr='') {return $this->directory($id,  $title, $handler, $attr);}
	
	public function save($id,  $title='', $handler=null, $attr=''){
		if (!preg_match('/class=/', $attr)) $attr.=' class="form-control"';
		return "<div class=\"input-group\">
  <div class=\"input-group-prepend\">
    <span class=\"input-group-text\" id=\"save_wui_".$id."_title\" ".$this->events($handler)." ".$attr.">$title</span>
  </div>
	<div class=\"custom-file\">
		<input type=\"hidden\" name=\"save_wui_".$id."_value\" id=\"save_wui_".$id."_value\" value=\"\">
		<input type=\"text\" class=\"custom-file-input\" name=\"save_wui_".$id."_component\" id=\"save_wui_".$id."_component\" aria-describedby=\"save_wui_".$id."_title\" lang=\"".explode('-', $this->APP_LANG)[0]."\" onclick=\"save_component=document.getElementById('save_wui_".$id."_value');label_save_component=document.getElementById('save_wui_".$id."_label'); alert('dialog->save');\" >
    <label id=\"save_wui_".$id."_label\" class=\"custom-file-label\" for=\"save_wui_".$id."_component\" style=\"overflow:auto!important;\">Sauvegarder un fichier</label>
  </div>
</div>";
	}
	
	public function slideshow($id,  $width='100', $handler=null,  $content=array(), $attr=''){
		$result= '<div id="'.$id.'" class="carousel slide" data-ride="carousel"  '.$this->events($handler).' '.$attr.' >
	<div id="'.$id.'_inner" class="carousel-inner">';
		$i=0;
		foreach ($content as $item) {
			$sid=$id.'_inner_item_'.$i;
			$sidi=$sid.'_img';
			$img=$item[0]??'';
			$caption=$item[1]??'';
			$shandler=$item[2]??null;
			$sattr=$item[3]??'';
			$ext_=explode('.', $img);
			$ext=$ext_[count($ext_)-1];
			$result.='<div id="'.$sid.'" class="carousel-item '.($i==0?'active':'').'">
			<img id="'.$sidi.'" src="data:image/'.$ext.';base64, '.base64_encode(file_get_contents(getenv('PWD').'/app/'.$img)).'" class="d-block w-'.$width.'" '.$this->events($shandler).' '.$sattr.'>
			<div class="carousel-caption d-none d-md-block">'.$caption.'</div>
		</div>';
			$i++;
		}
		$result.='</div>
		<a class="carousel-control-prev" href="#'.$id.'" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#'.$id.'" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
</div>';
		return str_replace(array("\n", '	'),'', $result);
	}
	
	public function accordion($id, $handler=null,  $content=array(), $attr=''){
		$result= '<div id="'.$id.'" class="accordion"  '.$this->events($handler).' '.$attr.' >';
		$i=0;
		foreach ($content as $item) {
			$sidcard=$id.'_card_'.$i;
			$sid=$sidcard.'_header';
			$sidc=$sid.'_collapse';
			$sidb=$sid.'_body';
			$caption=$item[0]??'';
			$shandler=$item[1]??null;
			$sattr=$item[2]??'';
			$isshow=(($item[3]??false)?'show':'');
			$shandlerb=$item[4]??null;
			$carbody=$item[5]??'';
			$sattrb=$item[6]??'';
			$result.='<div id="'.$sidcard.'" class="card">
			<div class="card-header" id="'.$sid.'" '.$this->events($shandler).' '.$sattr.'><h2 class="mb-0"><button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#'.$sidc.'" aria-expanded="true" aria-controls="'.$sidc.'">'.$caption.'</button></h2></div>
			<div id="'.$sidc.'" class="collapse '.$isshow.'" aria-labelledby="'.$sid.'" data-parent="#'.$id.'">
      <div id="'.$sidb.'" class="card-body" '.$this->events($shandlerb).' '.$sattrb.'>'.$carbody.'</div></div>
		</div>';
			$i++;
		}
		$result.='</div>';
		return str_replace(array("\n", '	'),'', $result);
	}
	
	public function separator(){return '<hr>';}
	public function hr(){return '<hr>';}
	public function return(){return '<br>';}
	public function br(){return '<br>';}
	
	public function end(){
		echo '</body></html>';
	}

}
?>

<?php
/**
 * Helpers de gestion d'affiche HTML standard
 * 
 * @return stdClass
 */
class HelpersJs {

	public function __construct(){}

	private function endline($end){
		if ($end) return ';';
		return '';
	}
	private function val($val=null, $pref='='){
		if (!is_null($val)) return "$pref'$val'";
		return '';
	}
	
	public function break(){
		return "break;";
	}
	
	public function continue(){
		return "continue;";
	}
	
	public function console($msg){
		if (is_array($msg)) return "console.log('".addslashes(json_encode($msg))."');";
		else return $this->script.="console.log('".addslashes($msg)."');";
	}
	
	public function alert($msg){
		if (is_array($msg)) return "alert('".addslashes(json_encode($msg))."');";
		else return "alert('".addslashes($msg)."');\n";
	}

	
	public function id($id, $var='', $end=false){
		if ($id=='this' ) return (!empty($var)?"var $var=":'')."this".self::endline($end);
		return (!empty($var)?"var $var=":'')."document.getElementById('$id')".self::endline($end);
	}
	
	public function submit($id, $end=false){
		if ($id=='this') return "this.submit()".self::endline($end);
		return "document.getElementById('$id').submit()".self::endline($end);
	}
	
	public function value($id, $value=null, $pref='=', $var='', $end=false){
		if ($id=='this') return (!empty($var)?"var $var=":'')."this.value".self::val($value,$pref).self::endline($end);
		return (!empty($var)?"var $var=":'')."document.getElementById('$id').value".self::val($value,$pref).self::endline($end);
	}
	
	public function display($id, $value=null, $pref='=', $var='', $end=false){
		if ($id=='this') return (!empty($var)?"var $var=":'')."this.style.display".self::val($value,$pref).self::endline($end);
		return (!empty($var)?"var $var=":'')."document.getElementById('$id').style.display".self::val($value,$pref).self::endline($end);
	}
	
	public function set($id, $att_value, $value, $pref='=', $end=false){
		if ($id=='this') return "this.".$att_value.self::val($value,$pref).self::endline($end);
		return "document.getElementById('$id').".$att_value.self::val($value,$pref).self::endline($end);
	}
	
	public function get($id, $att_value, $var='', $end=false){
		if ($id=='this') return (!empty($var)?"var $var=":'')."this.".$att_value.self::endline($end);
		return (!empty($var)?"var $var=":'')."document.getElementById('$id').".$att_value.self::endline($end);
	}
	
	public function getf($id, $att_value, $var='',$params='', $end=false){
		if ($id=='this') return (!empty($var)?"var $var=":'')."this.".$att_value.'('.$params.')'.self::endline($end);
		return (!empty($var)?"var $var=":'')."document.getElementById('$id').".$att_value.'('.$params.')'.self::endline($end);
	}
	
	public function onclick($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onclick="'.$content.'"'.self::endline($end);
	}
	
	public function onchange($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onchange="'.$content.'"'.self::endline($end);
	}
	
	public function onkeypress($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onkeypress="'.$content.'"'.self::endline($end);
	}
	public function onkeyup($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onkeyup="'.$content.'"'.self::endline($end);
	}
	
	public function onkeydown($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onkeydown="'.$content.'"'.self::endline($end);
	}
	
	public function onmouseover($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onmouseover="'.$content.'"'.self::endline($end);
	}
	
	public function onmousemove($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onmousemove="'.$content.'"'.self::endline($end);
	}
	
	public function onmouseout($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onmouseout="'.$content.'"'.self::endline($end);
	}
	
	public function onmousedown($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onmousedown="'.$content.'"'.self::endline($end);
	}
	
	public function onmouseup($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onmouseup="'.$content.'"'.self::endline($end);
	}
	
	public function onmouseenter($content='', $var='', $end=false){
		return (!empty($var)?"$var.":'').'onmouseenter="'.$content.'"'.self::endline($end);
	}
	
	public function if($v1, $comp=null, $v2=null, $content=''){
		return "if ($v1".(!is_null($comp)?$comp:'').(!is_null($v2)?$v2:'').") {".$content.'} ';
	}
	
	public function elif($v1, $comp=null, $v2=null, $content=''){
		return "else if ($v1".(!is_null($comp)?$comp:'').(!is_null($v2)?$v2:'').") {".$content.'} ';
	}
	
	public function elseif($v1, $comp=null, $v2=null, $content=''){
		return self::elif($v1, $comp, $v2, $content);
	}
	
	public function else($content=''){
		return "else {".$content.'} ';
	}
	
	public function func($name='', $params='', $content=''){
		return "function $name($params) {".$content.'} ';
	}
	
	public function function($name='', $params='', $content=''){
		return self::func($name, $params, $content);
	}

}
?>

<?php
class HelpersMysqlTemplate {

	public function __construct(){
		return;
	}

	/**
	 * @desc requête de création d'une table dans la base de donnée
	 * @param String $table_name nom de la table à créer
	 * @param Array $row(
	 *			$name String,
	 *			$type/$octets Array(String, Int/NULL),
	 *			$is_null Boolean,
	 *			$default String/Int/NULL,
	 *			$update String/NULL,
	 *			$auto Boolean
	 * ) information du champs à mettre dans la table
	 * @param Array $index(String, String) information des index/clé primaire ...etc
	 * @return String $request Requête généré
	 */
	public function create($table_name, $row=array(), $index=array()) {
		$table_name="`$table_name`";
		$request="CREATE TABLE IF NOT EXISTS ".$table_name." (\n";
		for ($i=0;$i<count($row);$i++) {
			$name="`".$row[$i][0]."`";
			$type=$row[$i][1][0];
			$octets=$row[$i][1][1];
			$is_null=($row[$i][2]?"":"NOT ")."NULL";
			$default=(is_string($row[$i][3])?" DEFAULT '".$row[$i][3]."'":(is_null($row[$i][3])?"":" DEFAULT".$row[$i][3]));
			$update=(!is_null($row[$i][4])?" ON UPDATE ".$row[$i][4]:"") ;
			$auto=($row[$i][5]?" AUTO_INCREMENT":"");
			$request.="$name $type".(!is_null($octets)?"($octets)":"")." ".$is_null.$default.$update.$auto;
			$request.=($i==(count($row)-1)?'':',')."\n";
		}
		for ($i=0;$i<count($index);$i++) {
			$request.=(isset($index[$i][0])?$index[$i][0]:"");
			$request.=(isset($index[$i][1])?" (`".$index[$i][1]."`)":"");
			$request.=($i==(count($params)-1)?'':',')."\n";
		}
		$request.=") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		return $request;
	}

	/**
	 * @desc requête de selection dans la base de donnée
	 * @param String $table_name nom de la table à créer
	 * @param Array $row(String) champs à recupérer
	 * @param Array $joint(String) Lien vers d'autre table à faire
	 * @param Array $where(String) Clause WHERE
	 * @param Array $others(String) Divers clause MYSQL (ORDER BY, GROUP BY...et)
	 * @param Integer $limit Clause LIMIT en MYSQL
	 * @return String $request Requête généré
	 * @example select('test', ['id', 'date', 'text'], '', 'id>0', '', 10);
	 * @example SELECT id, date, text FROM `test` WHERE id>0 LIMIT 10;
	 */
	public function select($table_name, $row=array(), $joint=array(), $where=array(), $others=array(), $limit=NULL) {
		$table_name="`$table_name`";
		$request="SELECT "
							.self::loopBasicArray($row, '', '*')
							." FROM $table_name "
							.self::loopBasicArray($joint)
							.self::loopBasicArray($where, ' WHERE ')
							.self::loopBasicArray($others)
							.(!is_null($limit)?' LIMIT '.$limit:'')
							.';';
		return $request;
	}
	
	/**
	 * @desc requête d'insertion dans la base de donnée
	 * @param String $table_name nom de la table à créer
	 * @param Array $datas(String, Mixed) champs à recupérer
	 * @param Array $where(String) Clause WHERE
	 * @param Array $others(String) Divers clause MYSQL (ORDER BY, GROUP BY...et)
	 * @param Integer $limit Clause LIMIT en MYSQL
	 * @return String $request Requête généré
	 * @example insert('test', [array('val1'=>15), array('val2'=>'salut')], 'id>0', '', 10);
	 * @example INSERT INTO `test` (`val1`,`val2`) VALUES (15, 'salut') WHERE id>0 LIMIT 10;
	 */
	public function insert($table_name, $datas=array(), $where=array(), $others=array(), $limit=NULL) {
		$table_name="`$table_name`";
		$dt=self::loopBasicObjectToArray0Keys1Values($datas);
		$request='INSERT INTO '.$table_name.' ('.$dt[0].') VALUES ('.$dt[1].')'
					.self::loopBasicArray($where, ' WHERE ')
					.self::loopBasicArray($others)
					.(!is_null($limit)?' LIMIT '.$limit:'')
					.';';
		return $request;
	}
	
	/**
	 * @desc requête de supression dans la base de donnée
	 * @param String $table_name nom de la table à créer
	 * @param Array $where(String) Clause WHERE
	 * @param Array $others(String) Divers clause MYSQL (ORDER BY, GROUP BY...et)
	 * @param Integer $limit Clause LIMIT en MYSQL
	 * @return String $request Requête généré
	 * @example delete('test',  'id>0', '', 10);
	 * @example DELETE FROM `test` WHERE id>0 LIMIT 10;
	 */
	public function delete($table_name, $where=array(), $others=array(), $limit=NULL) {
		$table_name="`$table_name`";
		$request='DELETE FROM '.$table_name
					.self::loopBasicArray($where, ' WHERE ')
					.self::loopBasicArray($others)
					.(!is_null($limit)?' LIMIT '.$limit:'')
					.';';
		return $request;
	}
	
	
	/**	
	 * ********************** PRIVATE METHOD FOR TEMPLATES METHODS **********************
	 */
	
	private function loopBasicObjectToArray0Keys1Values($array, $separator=',') {
		$keys='';
		$values='';
		$i=0;
		foreach ($array as $key => $value) {
			$keys.=($i==0?$separator.' ':'').'`'.$key.'`';
			$values.=($i==0?$separator.' ':'').(!is_string($value)?"'".$value."'":$value);
			$i++;
		}
		return [$keys,$values];
	}

	private function loopBasicArray($array, $start='', $else=false) {
		$request=$start;
		if (count($array)>0){
			for ($i=0;$i<count($array);$i++) {
				$request.=$array[$i];
				$request.=($i==(count($array)-1)?'':' ');
			}
		} else if (is_string($array))$request.=$array;
		else if ($else) $request.=$else;
		return $request;
	}

}
?>

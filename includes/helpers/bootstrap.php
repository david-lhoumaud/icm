<?php
require_once __DIR__.'/html.php';
/**
 * Helpers de gestion d'affiche HTML de bootstrap
 * 
 * @return stdClass
 */
class HelpersBootstrap extends HelpersHtml {

	public function __construct(){}

	/**
	 * affichage de la balise BODY
	 * 
	 * @return string
	 */
	public function body($content='', $theme='dark',  $attributes=''){
		return parent::body($content, 'class="bg-'.$theme.'" '.(!empty($attributes)?' '.$attributes:''));
	}

	/**
	 * Affichage d'un bouton
	 * 
	 * @return string
	 */
	public function button($type='primary', $content='',  $attributes=''){
		return parent::button($content, 'type="button" class="btn btn-'.$type.'" '.(!empty($attributes)?' '.$attributes:''));
	}
	
	/**
	 * Affichage d'un bouton
	 * 
	 * @return string
	 */
	public function a($type='primary', $content='', $link=null, $attributes=''){
		return parent::a($content, $link, 'class="btn btn-'.$type.'" '.(!empty($attributes)?' '.$attributes:''));
	}
	
	/**
	 * Affichage d'un bouton
	 * 
	 * @return string
	 */
	public function submit($type='primary', $content='',  $attributes=''){
		return parent::button($content, 'type="submit" class="btn btn-'.$type.'" '.(!empty($attributes)?' '.$attributes:''));
	}

	/**
	 * Affichage d'un bloc DIV group
	 * 
	 * @return string
	 */
	public function group($type='btn', $content='',  $attributes=''){
		return parent::tag('div', $content, 'class="'.$type.'-group" role="group" '.(!empty($attributes)?' '.$attributes:''));
	}
	
	/**
	 * Affichage d'un bloc DIV container
	 * 
	 * @return string
	 */
	public function container($content='',  $attributes=''){
		return parent::tag('div', $content, 'class="container" '.(!empty($attributes)?' '.$attributes:''));
	}

	/**
	 * Affichage d'un bloc DIV card seul
	 *
	 * @return string
	 */
	public function card($content='',  $attributes=''){
		$body = parent::tag('div', $content, 'class="card-body"');

		return parent::tag('div', $body, 'class="card" '.(!empty($attributes)?' '.$attributes:''));
	}

	/**
	 * Affichage d'un bloc DIV card avec header
	 *
	 * @return string
	 */
	public function cardWithHeader($title='', $content='',  $attributes=''){
		$header = parent::tag('div', $title, 'class="card-header"');
		$body = parent::tag('div', $content, 'class="card-body"');

		return parent::tag('div', $header.$body, 'class="card" '.(!empty($attributes)?' '.$attributes:''));
	}
	
	/**
	 * Affichage d'un bloc DIV form-group
	 * 
	 * @return string
	 */
	public function formGroup($content='',  $attributes=array()){
		if (is_array($attributes)) {
			return parent::tag('div', $content, array_merge($attributes,array('class'=>'form-group')));
		}
		else return parent::tag('div', $content, 'class="form-group" '.(!empty($attributes)?' '.$attributes:''));
	}
	
	/**
	 * Affichage d'un bloc INPUT form-control
	 * 
	 * @return string
	 */
	public function formControl($type, $name, $label='', $value='', $attributes=array()){
		$lbl = empty($label) ? '' : parent::tag('label', $label, 'for="'.$name.'"');

		return $lbl . parent::tag('input', 'type="'.$type.'" name="'.$name.'" value="'.$value.'"', array_merge($attributes,array('class'=>'form-control')), true);
	}

	/**
	 * Affichage d'un bloc DIV toolbar
	 * 
	 * @return string
	 */
	public function toolbar($type='btn', $content='',  $attributes=''){
		return parent::tag('div', $content, 'class="'.$type.'-toolbar" role="toolbar" '.(!empty($attributes)?' '.$attributes:''));
	}

	/**
	 * Affichage d'un bloc DIV alert
	 * 
	 * @return string
	 */
	public function alert($type='primary', $content='',  $attributes='', $delay=0){
		if ($delay>0) {
			$tmp_id=preg_replace('/(.*)?id="(.*)"(.*)?/', '$2', $attributes);
			if (empty(($tmp_id)) || !preg_match('/(.*)?id="(.*)"(.*)?/', $attributes)) {
				$id='alert-'.sha1(microtime());
				$attributes.=' id="'.$id.'"';
			}else $id=$tmp_id;
		}
		return parent::tag('div', $content, 'class="alert alert-'.$type.'"'.(!empty($attributes)?' '.$attributes:($delay>0?' id="'.$id.'"':'')))
		.($delay>0?parent::js('setTimeout(function () {document.getElementById("'.$id.'").style.display="none";}, '.$delay.');'):'');
	}
	
	public function load() {
		$count_subdirectory=count(explode('/',$_SERVER['REQUEST_URI']))-2;
		$dir='';
		for($i=0; $i<$count_subdirectory; $i++)$dir.='../';
		$dir.='includes/statics/css/fontawesome/webfonts/';
		$result=parent::css(
			file_get_contents(realpath('.').'/includes/statics/css/bootstrap.min.css').
			str_replace('../webfonts/',$dir,
				file_get_contents(realpath('.').'/includes/statics/css/fontawesome/all.min.css')
			)
		)
		.parent::js(
			file_get_contents(realpath('.').'/includes/statics/js/jquery-3.5.1.slim.min.js')
			.file_get_contents(realpath('.').'/includes/statics/js/popper.min.js')
			.file_get_contents(realpath('.').'/includes/statics/js/bootstrap.min.js')
		);
		return $result;
	}

}
?>

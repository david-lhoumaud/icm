<?php
/**
 * Class Main
 *
 * @var CONST<String> PROGRAM_NAME nom du programme
 * @var CONST<String> PROGRAM_AUTHOR nom du développeur
 * @var CONST<String> PROGRAM_VERSION version du programme
 * @var CONST<String> TEXT_SUCCESS texte en vert
 * @var CONST<String> TEXT_WARNING texte en jaune
 * @var CONST<String> TEXT_ERROR texte en rouge
 * @var CONST<String> TEXT_DEBUG texte en violet
 * @var CONST<String> TEXT_IMPORT texte en cyan
 * @var CONST<String> TEXT_BOLD texte en gras
 * @var CONST<String> TEXT_NONE texte nettoyer
 * @var Boolean $DEBUG Afficher ou nom les message de débuggage
 * @var Mixed $result résulat final du programme
 * @var String $filename nom du fichier à traiter
 * @var String $directory nom du répertoire à traiter
 * @method public __construct()	Constructeur de la classe
 * @method public result()			Résultat final
 * @method public log()					Afficher les message de debuggage
 * @method public error()				Gestion des erreurs
 * @method private __params()		Gestion des paramètres
 * @method private __import()		Import des classes nécessaire
 * @method private __init()			Initialisation des Objects et Variables
 * @method private __execute()	Execution du programme
 * @method private __help()			Afficher l'aide
 */
class HelpersEcbot {
	private const PROGRAM_NAME		=	'eCBot';
	private const PROGRAM_AUTHOR	=	'David Lhoumaau';
	private const PROGRAM_VERSION	=	'v0.0.1';
	private const TEXT_SUCCESS		=	"\033[32m";
	private const TEXT_WARNING		=	"\033[33m";
	private const TEXT_ERROR			=	"\033[31m";
	private const TEXT_DEBUG			=	"\033[35m";
	private const TEXT_IMPORT			=	"\033[36m";
	private const TEXT_BOLD				=	"\033[1m";
	private const TEXT_NONE				=	"\033[0m";
	private $DEBUG;
	private $result;
	private $filename;

	/**
	 * Constructeur principal
	 */
	public function __construct($options=array()){
		$this->result=array(
			'code'=>0,
			'error'=>null,
			'response'=>null
		);
		
		$this->cookieFile=tempnam(sys_get_temp_dir(), md5(__FILE__) . '-cURL-').".cookies";
		
		$this->DB_host		=	'';
		$this->DB_user		=	'';
		$this->DB_pass		=	'';
		$this->DB_dbname	=	'';
		$this->DB_params			= array();
		
		self::__params($options);
		self::__import();
		self::__init();
		self::__execute();
	}
	
	private function DB_connect($create=true) {
		try {
			$this->mysql = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname, $this->user, $this->pass);
		} catch (PDOException $e) {
			if (preg_match('/Unknown database \''.$this->dbname.'\'/',$e->getMessage())){
				try {
					$this->mysql = new PDO('mysql:host='.$this->host, $this->user, $this->pass);
				} catch (PDOException $e) {
					print "[Error] MYSQL - " . $e->getMessage() . "\n";
		    	die();
				}
				if ($create){
					self::DB_query("CREATE DATABASE IF NOT EXISTS `".$this->dbname."` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;");
					self::DB_connect(false);
				}
			} else {
				print "[Error] MYSQL - " . $e->getMessage() . "\n";
	    	die();
			}
		}
	}

	public function DB_login(
		$host='',
		$user='',
		$pass='',
		$dbname='',
		$create=true
	) {
		$this->host		=	$host;
		$this->user		=	$user;
		$this->pass		=	$pass;
		$this->dbname	=	$dbname;
		self::DB_close();
		self::DB_connect($create);
	}
	
	public function DB_query($query='', $params=NULL) {
		if (!is_null($params)) $this->DB_params=$params;
		try {
			//echo str_replace(array('   ',' ', "\n", "\t"),array(' ',' ', '', ' '),$query)."\n";
			$this->DB_request=$this->mysql->prepare(str_replace(array('   ',' ', "\n", "\t"),array(' ',' ', ' ', ' '),$query));
		} catch (PDOException $e) {
    	print "[Error] MYSQL Prepare - " . $e->getMessage() . "\n";
    	die();
		}
		try {
			if (count($this->DB_params)>0)$this->DB_request->execute($this->DB_params);
			else $this->DB_request->execute();

		} catch (PDOException $e) {
    	print "[Error] MYSQL Execute - " . $e->getMessage() . "\n";
    	die();
		}
	}
	
	public function DB_close() {
		$this->mysql = NULL;
	}
	
	public function DB_count($request=NULL) {
		if (!is_null($request)) $this->DB_request=$request;
		if (!is_null($this->DB_request)) return $this->DB_request->rowCount();
	}
	
	public function DB_lastInsertId(){
		return $this->mysql->lastInsertId();
	}
	public function DB_lastUpdateId(){
		return $this->mysql->lastUpdateId();
	}
	
	public function BD_setParam($key, $value) {
		$this->DB_params[':'.$key]=$value;
	}

	public function BD_setParams($params=array()) {
		$this->DB_params=$params;
	}
	
	public function DB_fetch($request=NULL) {
		if (!is_null($request) && !empty($request)) $this->DB_request=$request;
		if (!is_null($this->DB_request)) return $this->DB_request->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Retourne le resultat du programme
	 *
	 * @return Mixed résultat final du programme
	 */
	public function result(){
		return $this->result;
	}

	/**
	 * Gestion d'affiche des message de log
	 *
	 * @param String $message texte à afficher
	 * @param String $message_type Type de log à afficher (vert, rouge, jaune)
	 * @param Boolean $force_display Force l'affiche des log même si le debuggage n'est pas activé
	 * @param Boolean $return si true on return le message
	 * @return String si le paramètre $return est true alors on retourne $message
	 */
	public function log($message, $message_type='standard', $force_display=true, $return=false){
		$date=date("d-m-Y H:i:s");
		if ($this->DEBUG || $force_display){
			switch ($message_type) {
				case 'ok':
				case 'success':
					$code=($this->result['code']==0?'OK':$this->result['code']);
					$final_message	= self::TEXT_SUCCESS.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_SUCCESS.$message.self::TEXT_NONE;
				break;
				case 'warn':
				case 'warning':
					$code=($this->result['code']==0?'WARN':$this->result['code']);
					$final_message	= self::TEXT_WARNING.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_WARNING.$message.self::TEXT_NONE;
				break;
				case 'err':
				case 'error':
					$code=($this->result['code']==0?'ERR':$this->result['code']);
					$final_message	= self::TEXT_ERROR.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_ERROR.$message.self::TEXT_NONE;
				break;
				case 'dbg':
				case 'debug':
					$code=($this->result['code']==0?'DBG':$this->result['code']);
					$final_message	= self::TEXT_DEBUG.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_DEBUG.$message.self::TEXT_NONE;
				break;
				case 'info':
					$code=($this->result['code']==0?'INFO':$this->result['code']);
				case 'imp':
				case 'import':
					$code=$code??($this->result['code']==0?'IMP':$this->result['code']);
					$final_message	= self::TEXT_IMPORT.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t".self::TEXT_IMPORT.$message.self::TEXT_NONE;
				break;
				case 'code':
					$code=$code??($this->result['code']==0?'CODE':$this->result['code']);
					$final_message	= self::TEXT_IMPORT.self::TEXT_BOLD.$date."\t".$code.self::TEXT_NONE."\t\n".self::TEXT_IMPORT.$message.self::TEXT_NONE;
				break;
				default:
					$code=($this->result['code']==0?'LOG':$this->result['code']);
					if (!$this->DEBUG) $final_message	= $message;
					else $final_message	= $date."\t".$code."\t".$message;
				break;
			}
			if (!$return) echo $final_message."\n";
			else return $final_message."\n";
		}
	}
	
	/**
	 * Vérification des erreurs
	 *
	 * @param	Integer	Code l'ié à l'erreur
	 * @param Boolean Afficher ou non le message lié à l'erreur
	 * @param Boolean $use_color_bash Utilise les couleurs de sorti bash
	 * @param Boolean $exit quitte le programme si true
	 * @return Boolean
	 */
	public function error($code, $msg='', $display_error=true, $use_color_bash=true, $exit=true, $type='error'){
		$date=date("d-m-Y H:i:s");
		$this->result['erreur']=(!empty($msg)?$msg:'');
		switch ($code) {
			case 100:
				$this->result['erreur']="Impossible d'ouvrir le fichier ".$msg.'.';
			break;
			case 101:
				$this->result['erreur']="La ligne ne contient pas le même nombre de colonnes que le tableau d'en-têtes fourni : ".$msg.'.';
			break;
		}
		if ($display_error) {
			if ($use_color_bash) self::log($this->result['erreur'], $type);
			else self::log($this->result['erreur'], 'standard');
		}
		if ($exit) exit($code);
		else return true;
	}
	
	public static function removeWhiteSpace($text){
	    $text = htmlentities($text);
	    $text = str_replace("&nbsp;", " ", $text);
	    $text = preg_replace('/[\t\n\r\0\x0B]/', ' ', $text);
	    $text = preg_replace('/([\s])\1+/', ' ', $text);
	    $text = html_entity_decode($text);
	    $text = trim($text);
	    return $text;
	}

	public static function removeAccents($str, $charset = 'utf-8'){
		$str = htmlentities($str, ENT_NOQUOTES, $charset);
		$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
		return $str;
	}
	
	public function removeBOM($str){return str_replace("\xEF\xBB\xBF", '', $str);}

	/**
	 * Gestion des paramètres
	 */
	private function __params($options=array()){
		$shortopts 	=  'h'			// help
									.'d'			// debug
									.'p'			// preserve cookie file
									.'o::'		// json options
									.'f::';		// filename
		// $options 				= getopt($shortopts);
		$this->DEBUG		=	(isset($options['d'])?true:false);
		$this->preserver_cookie_file		=	(isset($options['p'])?true:false);
		$this->filename	=	(isset($options['f'])?$options['f']:false);
		parse_str($options['o']??'',$this->options);
		if (is_null($this->options))$this->options=array();
		self::__help((isset($options['h'])?true:false), 0);
	}

	/**
	 * Import des classes necessaires
	 */
	private function __import(){
		$this->log("Chargement des librairies", 'dbg', false);
	}

	/**
	 * Intialisation des objets et variables
	 */
	private function __init(){
		$this->log("Initialisation des Objects et des variables", 'dbg', false);
		if ($this->filename && file_exists($this->filename)){
			$this->json=json_decode(file_get_contents($this->filename),true);
		}
		$this->G=array();
	}
	
	private function dtab() {
		if ($this->no_tab) return '';
		$tab='';
		for($i=0;$i<$this->tab;$i++) {
			$tab.="\t";
		}
		return $tab;
	}

	/**
	 * Execution du programme
	 */
	private function __execute($is_started=true){
		$this->log("Execution du programme", 'dbg', false);
		$R=$this->json['requests']??array();
		$R_COUNT=count($R);
		$N=0;
		$OPT=$this->options;
		$LOOP_STOP=false;
		$LOOP=0;
		if ($is_started){
			$this->no_tab=false;
			$this->tab=0;
		}
		
		// Début de la liste des requetes
		if (isset($this->json['start'])){
			if (isset($this->json['start']['requires'])){
				$default_json=$this->filename;
				foreach ($this->json['start']['requires'] as $z => $y) {
					if (file_exists($y)) {
						$this->filename=$y;
						$this->json=json_decode(file_get_contents($this->filename),true);
						self::__execute(false);
					}
				}
				$this->filename=$default_json;
				$this->json=json_decode(file_get_contents($this->filename),true);
			}
			if (isset($this->json['start']['script'])){
				$script_eval=self::value_type($this->json['start']['script'])."\n";
				if (!empty($script_eval)) {
					$script_eval=str_replace(['$CR', '$NR', '$LR', '$G'],['$R[$IR]', '$R[$N]', '$R[$R_COUNT-1]', '$this->G'], $script_eval);
					$this->log($script_eval, 'code', false);
					try{
						eval($script_eval);
					}catch(Error $p){
						$this->log($p->getLine()." : ".$p->getMessage(), 'err', true);
						$this->log("\n".$script_eval, 'dbg', true);
						exit($p->getCode());
					}
				}
			}
		}
		// Liste des requetes
		
		for ($IR=0; $IR<$R_COUNT; $IR++){
			$CR=$R[$IR];
			$CR['__index__']=$IR;
			if (isset($R[$IR]['loop']) && $LOOP) {
				$N=$IR;
			} else {
				$N=$IR+1;
			}
			if (!isset($R[$IR]['method']) || empty($R[$IR]['method'])){
				$R[$IR]['method']='GET';
			}
			if (isset($R[$IR]['url']) && !empty($R[$IR]['url']) && strtolower($R[$IR]['method'])!='cmd'){
				$result=self::request($R[$IR]);
			} else {
				$result=array(
					'code'=>200,
					'response'=>'',
					'infos'=>array()
				);
			}
			if (isset($R[$IR]['response']) && isset($R[$IR]['response']['status-restart'])) {
				if (
					(is_int($R[$IR]['response']['status-restart']) && $result['code']==$R[$IR]['response']['status-restart']) || 
					(is_array($R[$IR]['response']['status-restart']) && in_array($result['code'],$R[$IR]['response']['status-restart']))
				 ) {
					if (isset($R[$IR]['response']['script-restart'])){
						$script_eval=str_replace(['$CR', '$NR', '$LR', '$G'],['$R[$IR]', '$R[$N]', '$R[$R_COUNT-1]', '$this->G'], self::value_type($R[$IR]['response']['script-restart'])).';';
						$this->log($script_eval, 'code', false);
						try{
							eval($script_eval);
						}catch(Error $p){
							$this->log($p->getLine()." : ".$p->getMessage(), 'err', true);
							$this->log("\n".$script_eval, 'dbg', true);
							exit($p->getCode());
						}
					}
					$IR--;
					continue;
				}
			}
			if (isset($R[$IR]['response']) && isset($R[$IR]['response']['status'])) {
				if ((is_int($R[$IR]['response']['status']) && $result['code']!=$R[$IR]['response']['status']) || (is_array($R[$IR]['response']['status']) && !in_array($result['code'],$R[$IR]['response']['status']))) {
					if (isset($R[$IR]['response']['script-error'])){
						$script_eval=str_replace(['$CR', '$NR', '$LR', '$G'],['$R[$IR]', '$R[$N]', '$R[$R_COUNT-1]', '$this->G'], self::value_type($R[$IR]['response']['script-error'])).';';
						$this->log($script_eval, 'code', false);
						try{
							eval($script_eval);
						}catch(Error $p){
							$this->log($p->getLine()." : ".$p->getMessage(), 'err', true);
							$this->log("\n".$script_eval, 'dbg', true);
							exit($p->getCode());
						}
					}
					exit($result['code']);
				}
			}
			$CODE=$result['code'];
			$RESPONSE=$result['response'];
			$INFOS=$result['infos'];
			if (isset($R[$IR]['response']['script'])){
				$script_eval=self::value_type($R[$IR]['response']['script'])."\n";
				if (!empty($script_eval)) {
					$script_eval=str_replace(['$CR', '$NR', '$LR', '$G'],['$R[$IR]', '$R[$N]', '$R[$R_COUNT-1]', '$this->G'], $script_eval);
					$this->log($script_eval, 'code', false);
					try{
						eval($script_eval);
					}catch(Error $p){
						$this->log($p->getLine()." : ".$p->getMessage(), 'err', true);
						$this->log("\n".$script_eval, 'dbg', true);
						exit($p->getCode());
					}
				}
			}
			if (isset($CR['loop']) && !$LOOP_STOP) {
				// echo $CR['__index__']." l:".($CR['loop']??'_')."  loop:".$LOOP."\n";
				if ($LOOP<$CR['loop']-1 || $CR['loop']==0){
					$LOOP++;
					if ($CR['__index__']==$IR-1) $IR--;
					else $IR-=1;
				}else {
					$IR=$CR['__index__'];
					$LOOP=0;
				}
			} elseif ($LOOP_STOP) {
				$LOOP=0;
			}
			
			$LOOP_STOP=false;
		}
		// Fin de la liste des requetes
		if (isset($this->json['end'])){
			if (isset($this->json['end']['script'])){
				$script_eval=self::value_type($this->json['end']['script'])."\n";
				if (!empty($script_eval)){ 
					$script_eval=str_replace(['$CR', '$NR', '$LR', '$G'],['$R[$IR]', '$R[$N]', '$R[$R_COUNT-1]', '$this->G'], $script_eval);
					$this->log($script_eval, 'code', false);
					try{
						eval($script_eval);
					}catch(Error $p){
						$this->log($p->getLine()." : ".$p->getMessage(), 'err', true);
						$this->log("\n".$script_eval, 'dbg', true);
						exit($p->getCode());
					}
				}
			}
			if (isset($this->json['end']['requires'])){
				$default_json=$this->filename;
				foreach ($this->json['end']['requires'] as $z => $y) {
					if (file_exists($y)) {
						$this->filename=$y;
						$this->json=json_decode(file_get_contents($this->filename),true);
						self::__execute(false);
					}
				}
				$this->filename=$default_json;
				$this->json=json_decode(file_get_contents($this->filename),true);
			}
		}
	}
	
	private function eval($eval) {
		
	}
	
	private function script_parse($SL) {
		//$PK=array_keys($SL)[0];
		$s='';
		$t='';
		foreach ($SL as $PK => $SA) {
			switch(true) {
				case preg_match('/log(-\d+)?$/i',$PK):
					$t=self::dtab().'$this->log('.self::value_type($SA[1]??'').', "'.($SA[0]??'info')."\");\n";
				break;
				case preg_match('/log-ok(-\d+)?$/i',$PK):
					$t=self::dtab().'$this->log('.self::value_type($SA??'').', "ok'."\");\n";
				break;
				case preg_match('/log-warn(-\d+)?$/i',$PK):
					$t=self::dtab().'$this->log('.self::value_type($SA??'').', "warn'."\");\n";
				break;
				case preg_match('/log-dbg(-\d+)?$/i',$PK):
					$t=self::dtab().'$this->log('.self::value_type($SA??'').', "dbg'."\");\n";
				break;
				case preg_match('/log-error(-\d+)?$/i',$PK):
					$t=self::dtab().'$this->log('.self::value_type($SA??'').', "error'."\");\n";
				break;
				case preg_match('/log-info(-\d+)?$/i',$PK):
					$t=self::dtab().'$this->log('.self::value_type($SA??'').', "info'."\");\n";
				break;
				case preg_match('/log-imp(-\d+)?$/i',$PK):
					$t=self::dtab().'$this->log('.self::value_type($SA??'').', "import'."\");\n";
				break;
				
				case preg_match('/echo(-\d+)?$/i',$PK):
					$t=self::dtab().'echo '.self::value_type($SA??'').";\n";
				break;
				case preg_match('/var_dump(-\d+)?$/i',$PK):
					$t=self::dtab().'var_dump('.self::value_type($SA??'').");\n";
				break;
				
				case preg_match('/print_r(-\d+)?$/i',$PK):
					$t=self::dtab().'print_r('.self::value_type($SA??'').");\n";
				break;
				
				case preg_match('/unset(-\d+)?$/i',$PK):
					$t=self::dtab().'unset('.self::value_type($SA??'').");\n";
				break;
				
				case preg_match('/flush(-\d+)?$/i',$PK):
					$t=self::dtab().'flush('.self::value_type($SA??'').");\n";
				break;
				
				case preg_match('/DB_login(-\d+)?$/i',$PK):
					$t=self::dtab().'self::DB_login('.self::value_type($SA['host']??($SA[0]??'')).','.self::value_type($SA['user']??($SA[1]??'')).','.self::value_type($SA['password']??($SA[2]??'')).','.self::value_type($SA['dbname']??($SA[3]??'')).");\n";
				break;
				
				case preg_match('/DB_query(-\d+)?$/i',$PK):
					$t=self::dtab().'self::DB_query('.self::value_type($SA??'').");\n";
				break;
				
				case preg_match('/DB_fetch(-\d+)?$/i',$PK):
					$t=self::dtab().'$DB_result=self::DB_fetch('.self::value_type($SA??null).");\n";
				break;
				
				
				case preg_match('/LOOP(-\d+)?(\.)?/',$PK):
				case preg_match('/LOOP_STOP(-\d+)?(\.)?/',$PK):
				case preg_match('/NR(-\d+)?(\.)?/',$PK):
				case preg_match('/LR(-\d+)?(\.)?/',$PK):
				case preg_match('/CR(-\d+)?(\.)?/',$PK):
				case preg_match('/R(-\d+)?(\.)?/',$PK):
				case preg_match('/G(-\d+)?(\.)?/',$PK):
				case preg_match('/IR(-\d+)?(\.)?/',$PK):
				case preg_match('/\$[a-zA-Z_\$]((->)?[_a-zA-Z0-9]+)?(-\d+)?(\.)?/',$PK):
					$t=self::dtab();
					foreach(explode('.', $PK) as $key=>$value) {
						if ($key==0){
							$suffixe=substr($value, strlen($value)-2, 2);
							switch (substr($value, strlen($value)-2, 2)){
									case '_=':case '+=':case '-=':case '*=':case '/=':case '++':case '--':
										$t.='$'.str_replace(['$',$suffixe],'',preg_replace('/-\d+/','', $value)).str_replace('_=','.=',$suffixe);
									break;
									default:
										if (substr($value, strlen($value)-1, 1)=="=") $t.='$'.str_replace(['$','='],'',preg_replace('/-\d+/','', $value)).'=';
										else $t.='$'.str_replace('$','',preg_replace('/-\d+/','', $value));
							}
						} else {
							if ($value==='[]' || empty($value)) $t.='[]';
							elseif ($value==='=') $t.='=';
							elseif ($value==='_=') $t.='.=';
							elseif ($value==='+=') $t.='+=';
							elseif ($value==='-=') $t.='-=';
							elseif ($value==='*=') $t.='*=';
							elseif ($value==='/=') $t.='/=';
							elseif ($value==='++') $t.='++';
							elseif ($value==='--') $t.='--';
							else {
								$suffixe=substr($value, strlen($value)-2, 2);
								switch (substr($value, strlen($value)-2, 2)){
									case '_=':case '+=':case '-=':case '*=':case '/=':case '++':case '--':
										$t.='['.self::value_type(str_replace($suffixe, '',$value)).']'.str_replace('_=','.=',$suffixe);
									break;
									default:
										if (substr($value, strlen($value)-1, 1)=="=") $t.='['.self::value_type(str_replace('=','',$value)).']=';
										else $t.='['.self::value_type($value).']';
								}
							}
						}
					}
					
					// if (is_array($SA)){
					// 	// var_dump($SA);exit;
					// 	$t.=self::script_parse($SA).";\n";
					// } else {
					if (!preg_match('/(\+\+|\-\-)$/',$t))$t.=self::value_type($SA, true).";\n";
					else $t.=";\n";
					//}
				break;
				
				
				
				
				case preg_match('/if(-\d+)?$/',$PK):
				case preg_match('/elseif(-\d+)?$/',$PK):
				case preg_match('/while(-\d+)?$/',$PK):
				case preg_match('/for(-\d+)?$/',$PK):
				case preg_match('/foreach(-\d+)?$/',$PK):
					$this->no_tab=false;
					$t=self::dtab().preg_replace('/-\d+?$/','', $PK).' (';
					if (isset($SA[0]) && is_array($SA[0])){
						foreach ($SA[0] as $iv=>$value) {
							if (preg_match('/for(-\d+)?$/',$PK)){
								$t.=($iv>0?'; ':'').$value;
							}else $t.=($iv>0?' ':'').$value;
						}
					} else {
						$t.=$SA[0]??false;
					}
					$t.=') {'."\n";
					if (isset($SA[1])){
						$this->tab++;
						$t.=self::value_type($SA[1]);
						if (!isset($SA[2])) {
							$this->tab--;
							$this->no_tab=false;
							$t.=self::dtab()."}\n";
						}
					}
					if (isset($SA[2])){
						$this->tab--;
						$this->no_tab=false;
						$t.=self::dtab()."} else {\n";
						$this->tab++;
						$t.=self::value_type($SA[2]);
						$this->tab--;
						$this->no_tab=false;
						$t.=self::dtab()."}\n";
					}
				break;
				
				case preg_match('/exit(-\d+)?/',$PK):
				case preg_match('/continue(-\d+)?/',$PK):
				case preg_match('/break(-\d+)?/',$PK):
					if (preg_match('/exit(-\d+)?/',$PK) && is_int($SA)){
						$t=self::dtab().preg_replace('/-\d+?$/','', $PK)."($SA);\n";
					} else {
						$t=self::dtab().preg_replace('/-\d+?$/','', $PK).";\n";
					}
				break;
				
				case preg_match('/array(-\d+)?/',$PK):
					if (is_array($SA)){
						$t=self::dtab()."array(";
						if (count($SA)>0){
							if (is_string(key($SA))){
								$ki=0;
								$cki=count($SA);
								foreach ($SA as $key => $value) {
									$t.=self::dtab()."\t'$key' => ".self::delete_return_line(self::value_type($value)).(++$ki<$cki?',':'')."\n";
								}
								$ki=0;
							} else {
								foreach ($SA as $key => $value) {
									$t.=self::dtab()."\t".self::delete_return_line(self::value_type($value)).(isset($SA[$key+1])?',':'')."\n";
								}
							}
						}
						$t.=self::dtab().")";
					}
				break;
				
				case preg_match('/\#(-\d+)?/',$PK):
					$t="/** ".$SA." */\n";
				break;
									
				default: 
					$_t=$PK;
					// echo $_t.'='.gettype($_t)."\t".self::value_type($SA)."\n";
					if (!is_string($_t) && is_numeric($_t) &&  !is_array($SA)){
						$t=self::dtab().self::value_type($SA);
					}elseif (!is_string($_t) && is_numeric($_t) && is_array($SA)){
						$t=self::value_type($SA);
					} else {
						$tk=preg_replace('/-\d+?$/','', preg_replace(['/;$/','/_$/','/_\+$/','/_\-$/','/_\*$/','/_\/$/'],'',$PK));
						if (empty($tk)){
							$t=self::value_type($SA, true);
						}else {
							$t=self::dtab().(
								$tk."(".
								(is_array($SA)?
									implode(',', array_map(function($item) {
										return self::value_type($item, true);
									}, $SA)):
									self::value_type($SA, true)
								).")"
							);
						}
					}
					if (preg_match('/;$/', $PK)) $t.=";\n";
					else if (preg_match('/_$/', $PK)) $t.=".";
					else if (preg_match('/_\+$/', $PK)) $t.="+";
					else if (preg_match('/_\-$/', $PK)) $t.="-";
					else if (preg_match('/_\*$/', $PK)) $t.="*";
					else if (preg_match('/_\/$/', $PK)) $t.="/";
				break;
			}
			$s.=$t;
		}
		return $s;
		
	}
	
	private function delete_return_line($s) {
		$l=strlen($s);
		if (substr($s, $l-1,1)=="\n"){
			return substr($s, 0, $l-1);
		}
		return $s;
	}
	
	private function r_array_k($v) {
		$s='';
		foreach ($v as $key => $value) {
			if ($value==='++' || $value==='--') {
				$s.='['.self::value_type($key).']'.$value.';';
			} else if ($key==='+' || empty($key)) {
				if (is_array($value)) $s.='[]'.self::r_array_k($value);
				else $s.='[]='.self::value_type($value).';';
			} else {
				if (is_array($value)) $s.='['.self::value_type($key).']'.self::r_array_k($value);
				else $s.='['.self::value_type($key).']='.self::value_type($value).';';
			}
		}
		return $s;
	}
	
	private function value_type($v, $no_tab=false) {
		$this->no_tab=$no_tab;
		if (is_array($v)) {
			return self::script_parse($v);
		} else if (is_null($v)) {
			return 'null';
		} else if (is_bool($v)) {
			return ($v?'true':'false');
		} else if (is_string($v)) {
			if (is_numeric($v)){
				return $v;
			} elseif (
				preg_match('/^>>.*/', $v) || 
				preg_match('/^(\()?\$.*/', $v)	||
				preg_match('/^\(bool\).*/', $v)	||
				preg_match('/^\(int\).*/', $v)	||
				preg_match('/^\(long\).*/', $v)	||
				preg_match('/^\(float\).*/', $v)	||
				preg_match('/^\(string\).*/', $v)	||
				preg_match('/^echo.*/', $v)	||
				preg_match('/^[a-zA-Z_]([a-zA-Z0-9_]+)?\(.*/', $v)	||
				preg_match('/^[a-zA-Z_]([a-zA-Z0-9_]+)?\:\:[a-zA-Z_]([a-zA-Z0-9_]+)?\(.*/', $v)	||
				preg_match('/^[a-zA-Z_]([a-zA-Z0-9_]+)?\:\:[a-zA-Z_]([a-zA-Z0-9_]+)?\-\>[a-zA-Z_]([a-zA-Z0-9_]+)?.*/', $v)	||
				preg_match('/.*\-\>[a-zA-Z_]([a-zA-Z0-9_]+)?(\-\>+)?\(.*/', $v) ||
				preg_match('/^\'.*/', $v) || 
				preg_match('/^".*/', $v)
			) {
				return preg_replace('/^>>/',  '', $v);
			} elseif ( preg_match('/^\#.*/', $v) ) {
				return "/** ".preg_replace('/^\#/',  '', $v)." */";
			} else {
				return '"'.addslashes($v).'"';
			}
		}
		$this->no_tab=false;
		return $v;
	}

	private function request($req) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $req['url']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, $req['returnTransfer']??true); 
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $req['connectTimeout']??50);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $req['sslVerifyPeer']??false); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $req['followLocation']??true); 
		if (!isset($req['no-cookies']) || !$req['no-cookies']){
			curl_setopt($ch, CURLOPT_COOKIEFILE, $req['cookieFile']??$this->cookieFile); 
			curl_setopt($ch, CURLOPT_COOKIEJAR, $req['cookieJar']??$this->cookieFile); 
		}
		curl_setopt($ch, CURLOPT_ENCODING, $req['encoding']??"");
		curl_setopt($ch, CURLOPT_MAXREDIRS, $req['maxRedirs']??10);
		curl_setopt($ch, CURLOPT_TIMEOUT, $req['timeout']??300);
		if (isset($req['httpVersion'])){
			switch($req['httpVersion']) {
				case 'HTTP/1':
				case 'HTTP/1.0':$req['httpVersion']=CURL_HTTP_VERSION_1_0;break;
				case 'HTTP/1.1':$req['httpVersion']=CURL_HTTP_VERSION_1_1;break;
				case 'HTTP/2':
				case 'HTTP/2.0':$req['httpVersion']=CURL_HTTP_VERSION_2_0;break;
			}
			curl_setopt($ch, CURLOPT_HTTP_VERSION, (is_int($req['httpVersion'])?$req['httpVersion']:CURL_HTTP_VERSION_1_1)); 
		} else {
			curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		}
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $req['method']);
		if (strtolower($req['method'])=='post') {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $req['post']);
		} else if (strtolower($req['method'])=='delete') {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $req['delete']);
		}
		if (isset($req['headers'])){
			$headers=array();
			foreach ($req['headers'] as $index=>$header){
				$headers[]=$header['name'].': '.$header['value'];
			}
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		$RESPONSE = curl_exec($ch); 
		$code=-1;
		$infos=array();
		if(!curl_errno($ch)){
			$infos = curl_getinfo($ch);
			$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		}
		curl_close($ch);
		return array('code'=>$code, 'response'=>$RESPONSE, 'infos'=>$infos);
	}

	/**
	 * Afficher l'aide
	 *
	 * @param Boolean $display affiche ou pas le echo
	 * @param Integer $code code de sortie du exit()
	 */
	private function __help($display=true, $code=0){
		if ($display){
			$this->log(self::PROGRAM_NAME.' '.self::PROGRAM_VERSION.'
by '.self::PROGRAM_AUTHOR.'
Options:
	-h	Afficher l\'aide
	-d	Afficher les messages de debuggage
	-f	Nom du fichier
	-o	options in json format (ex: -o\'{"namevar1":5,"namevar2":true,"namevar3":"string"}\' use: $OPT[\'namevar2\'])
	-p	Preserve cookie file
', 'standard', true);
			exit($code);
		}
	}
}

?>

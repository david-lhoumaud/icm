<?php

class HelpersMemory
{
		public $shm_id;
		public $shm_size;
		public $items;
		public $id;
		public $mode;
		public $byte;
		private $semaphore;
		
		public function __construct()
		{
			$this->id=ftok(__FILE__, 't');//0xff3;
			$this->mode=0644;
			$this->byte=128 * 128 * 100;
			$this->semaphore = sem_get($this->id, 1, $this->mode, 1);
			return;
		}
		
		public function ID($_id=null)
		{
			if (!is_null($_id)){
				if (is_string($_id)) {
					if (file_exists($_id))$this->id=ftok(__FILE__, 't');
					else return false;
				} else if (is_int($_id)){
					$this->id=$_id;
				} else return false;
			}
			return $this->id;
		}
		
		public function c(){
			// Demander le verrou
			sem_acquire($this->semaphore);
			$this->shm_id = shmop_open($this->id, 'c', $this->mode, $this->byte);
			if (!$this->shm_id) {
				sem_release($this->semaphore);
				throw new Exception("Couldn't create shared memory segment");
			} else {
				$this->items = shmop_write($this->shm_id, serialize([]), 0);
				$this->shm_size = shmop_size($this->shm_id);
			}
			sem_release($this->semaphore);
		}
		
		public function n(){
			// Demander le verrou
			sem_acquire($this->semaphore);
			$this->c();
			if ($this->shm_id) {
				$this->free();
				//throw new Exception("Couldn't create new shared memory segment");
			} 
			$this->shm_id = shmop_open($this->id, 'n', $this->mode, $this->byte);
			$this->items = shmop_write($this->shm_id, serialize([]), 0);
			$this->shm_size = shmop_size($this->shm_id);
		}
		
		public function a(){
			// Demander le verrou
			sem_acquire($this->semaphore);
			$this->shm_id = shmop_open($this->id, 'a', $this->mode, $this->byte);
			if (gettype($this->shm_id)!='object') {
				sem_release($this->semaphore);
				return $this->c();
			} else {
				$this->shm_size = shmop_size($this->shm_id);
			}
			sem_release($this->semaphore);
		}
		
		public function w(){
			// Demander le verrou
			sem_acquire($this->semaphore);
			$this->shm_id = shmop_open($this->id, 'w', $this->mode, $this->byte);
			if (!$this->shm_id) {
				sem_release($this->semaphore);
				throw new Exception("Couldn't write shared memory segment");
			} else {
				$this->shm_size = shmop_size($this->shm_id);
			}
			sem_release($this->semaphore);
		}
		
		public function isEmpty()
		{
			return empty(unserialize(shmop_read($this->shm_id, 0, $this->shm_size)));
		}
		
		public function insert($item)
		{
			// Demander le verrou
        	sem_acquire($this->semaphore);
			$array = unserialize(shmop_read($this->shm_id, 0, $this->shm_size));
			array_unshift($array, [$item]);
			$this->items = shmop_write($this->shm_id, serialize($array), 0);
			$this->shm_size = shmop_size($this->shm_id);
			sem_release($this->semaphore);
		}
		
		public function remove()
		{
			// Demander le verrou
        	sem_acquire($this->semaphore);
			$array = unserialize(shmop_read($this->shm_id, 0, $this->shm_size));
			array_pop($array);
			$this->items = shmop_write($this->shm_id, serialize($array), 0);
			$this->shm_size = shmop_size($this->shm_id);
			sem_release($this->semaphore);
		}
		
		public function items()
		{
				return unserialize(shmop_read($this->shm_id, 0, $this->shm_size));
		}
		
		public function get()
		{
				$array = unserialize(shmop_read($this->shm_id, 0, $this->shm_size));
				$out=end($array);
				return $out??[];
		}

		public function use()
		{
				$array = unserialize(shmop_read($this->shm_id, 0, $this->shm_size));
				$this->remove();
				$out=end($array);
				return is_array($out)?$out[0]:$out;
		}

		public function update($newItem)
		{
			// Demander le verrou
        	sem_acquire($this->semaphore);
			$array = unserialize(shmop_read($this->shm_id, 0, $this->shm_size));
			$array[0] = $newItem;
			$this->items = shmop_write($this->shm_id, serialize($array), 0);
			$this->shm_size = shmop_size($this->shm_id);
			sem_release($this->semaphore);
		}
		
		public function close()
		{
			sem_acquire($this->semaphore);
			if ($this->shm_id) {
				shmop_delete($this->shm_id);
			}
			if (function_exists('shmop_close') ) {
				shmop_close($this->shm_id);
			} 
			$this->shm_id = null;
			sem_remove($this->semaphore);
		}
		
}
?>
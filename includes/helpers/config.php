<?php
/**
 * Class HelpersConfig
 *
 * This class handles configuration loading and sharing between processes using shared memory.
 * 
 * @property $__shared__ Array Shared configuration data stored in shared memory. Contains 'config' and 'used' keys.
 *
 * @method __construct(array $__shared__) Constructor that initializes the shared memory and loads configuration.
 * @method get() Gets the configuration data, loading from config.json if not already loaded.
 *
*/

class HelpersConfig {

	/**
	 * Shared configuration data stored in shared memory.
	 * 
	 * @var array|null $__shared__ The shared configuration array containing 'config' and 'used' keys.
	 */
	private $__shared__=null;

	/**
	 * Constructor that initializes the shared memory and loads configuration.
	 *
	 * Checks if shared memory functions are available, initializes shared memory segment,
	 * loads existing configuration from shared memory or reads from config.json file if not present.
	 * 
	 * @param array|null $__shared__ Shared configuration data array containing 'config' and 'used' keys.
	 *
	 * @uses Helpers::load() Loads Memory helper class. 
	 * @uses Memory->c() Creates new shared memory segment.
	 * @uses Memory->a() Attaches to existing shared memory segment.
	 * @uses Memory->get() Gets data from shared memory segment.  
	 * @uses Memory->w() Writes data to shared memory segment.
	 * @uses Memory->update() Updates existing data in shared memory segment.
	 */
	public function __construct($shared=null){
		if (function_exists('shmop_open')){
			$__Memory__=Helpers::load('memory');
			$this->__shared__=[
				'config'=>[],
				'used'=>[]
			];
			if (!$__Memory__->ID()) {
				$__Memory__->c();
			} else {
				$__Memory__->a(); 
			}
			$this->__shared__ = $__Memory__->get();
			if (empty($this->__shared__['config'])){
				$configFilePath = __DIR__ . '/../../config.json'; // Path to config.json
				if (file_exists($configFilePath)) $config = json_decode(file_get_contents($configFilePath), true);
				$this->__shared__['config']=$config??[];
				$__Memory__->w();
				$__Memory__->update($this->__shared__);
			} 
		}
	}


	/**
	 * Gets the configuration data, loading from config.json if not already loaded.
	 *
	 * Checks if the shared configuration data is set, returns it if available.
	 * Otherwise tries to load the configuration from the config.json file.
	 * 
	 * @return array Configuration data array, or empty array if not found.
	 */ 
	public function get(){
		if (!is_null($this->__shared__)) return $this->__shared__['config'];
		$configFilePath = __DIR__ . '/../../config.json'; 
		if (file_exists($configFilePath)) return json_decode(file_get_contents($configFilePath), true);
		return [];
    }

}
?>
<?php
class HelpersThread {
	public $WUI_SERVER;
	public $headers;
	public $referer;
	public $userAgent;
	public $timeout;
	public $connecttimeout;
	public $followLocation;
	public $returnTransfer;
	
	
	public function __construct(){
		$this->WUI_SERVER=getenv('WUI_SERVER');
		$this->headers[] = 'Content-Type: application/json;charset=UTF-8';
		$this->timeout=1;
		$this->returnTransfer=true;
	}
	
	public function start($data=array(), $delay=0) {
		$data['__delay__']=$delay;
		$process = curl_init($this->WUI_SERVER);
		curl_setopt($process, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($process, CURLOPT_HEADER, false);
		curl_setopt($process, CURLOPT_TIMEOUT, $this->timeout);
		curl_setopt($process, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($process, CURLOPT_RETURNTRANSFER, $this->returnTransfer);
		curl_setopt($process, CURLOPT_POST, true);
		curl_setopt($process, CURLOPT_CUSTOMREQUEST, 'POST');
		$content = curl_exec($process);
		$error 	= ($content === false ? curl_error($process) : null);
		$status 	= curl_getinfo($process,CURLINFO_HTTP_CODE);
		$url 		= curl_getinfo($process,CURLINFO_EFFECTIVE_URL);
		curl_close($process);
		return array(
			'error'		=>$error,
			'status'	=>$status,
			'url'			=>$url,
			'content'	=>$content
		);
		
	}
}
?>
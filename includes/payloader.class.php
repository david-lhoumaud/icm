<?php
require_once __DIR__.'/map.class.php';
/**
 * Classe de gestion du payload.
 * 
 * @return stdClass
 */
class Payloader extends Map{
	/** 
	 * Flux de transmission
	 * @var array<mixed> $payload 
	 */
	protected $payload;
	
	protected $__error__;
	protected $config;

	/**
	 * Initialise les données de configuration et le payload
	 * 
	 * @return void
	 */
	public function __construct(){
		$this->config = Helpers::load('config')->get();
		parent::__construct();
		self::iniPayload();
	}
	
	/**
	 * Initialise le payload
	 * 
	 * @return void
	 */
	private function iniPayload() {
		if (!isset($this->payload)) {
			$this->payload=array(
				'code'=>0,
				'error'=>array(),
				'config'=>$this->config,
				'client'=>array(),
				'server'=>array(),
				'request'=>array(),
				'maps'=>array(
					'input'		=> '',
					'process'	=> '',
					'output'	=> ''
				),
				'files'=>array(
					'input'		=> '',
					'output'	=> ''
				),
				'raws'=>'',
				'datas'=>array(
					'input'		=> array(),
					'process'	=> array(),
					'output'	=> array()
				)
			);
		}
	}

	
	/**
	 * Recharge le payload avec le paramètre $datas
	 * 
	 * @param array $datas payload transitoire
	 * @return void
	 */
	public function payloadReload($datas){
		$this->payload=$datas;
	}
	
	protected function payloadDelete($keys=null){
		self::iniPayload();
		if (!is_null($keys)){
			$path=explode('/', $keys);
			if (is_array($path) && count($path)>1){
				$return=$this->payload;
				$_r='';
				for ($i=0; $i<count($path); $i++){
					$_r.='[\''.$path[$i].'\']';
					if (isset($return[$path[$i]])){
							$return=$return[$path[$i]];
					}else {
							$return=null;
							break;
					}
				}
				eval('if (isset($this->payload'.$_r.')) unset($this->payload'.$_r.');');
			} else {
				if (isset($this->payload[$keys])) unset($this->payload[$keys]);
			}
		} else {
			unset($this->payload);
		}
	}
	
	
	/**
	 * Retourne ou inscrit une donnée dans le payload
	 *
	 * @param string $keys sous forme de chemin exemple : self::payload('data/input')
	 * @param mixed $value donnée à inscrire
	 * @return array|mixed contenu réclamé au payload
	 */
	public function payload($keys=null, $value=null){
		self::iniPayload();
		if (!is_null($keys)){
			$path=explode('/', $keys);
			if (is_array($path) && count($path)>1){
				$return=$this->payload;
				$_r='';
				for ($i=0; $i<count($path); $i++){
					$_r.='[\''.$path[$i].'\']';
					if (isset($return[$path[$i]])){
						$return=$return[$path[$i]];
					}else {
						$return=null;
						break;
					}
				}
				//echo (!is_null($value)?'SET ':'GET ').$keys.' - '.$_r."\n";
				//if ($this->payload['maps']['output']=='standard' ) exit; 
				//echo $this->payload['maps']['output'].' -> '.'$this->payload'.$_r.'=$value;'."\n";
				if (!is_null($value)){
					eval('$this->payload'.$_r.'=$value;');
				}
			
				return 	$return;
			}elseif (!is_null($value)){
				$this->payload[$keys]=$value;
				return $this->payload[$keys];
			} else {
				if (isset($this->payload[$keys])) return $this->payload[$keys];
				return null;
			}
		} else {
			if(!is_null($value))$this->payload=$value;
		}
		
		return $this->payload;
	}
	
	/**
	 * Vérifie si une variable de session existe
	 * la clé est sous forme de chemin d'accès en string
	 * 
	 * @param string $key clé de la variable de session à vérifier
	 * @return boolean Retourne true si la variable existe sinon false
	 */
	public function payload_exists($key) {
		$path=explode('/', $key);
		if (count($path)>1){
			$return=$this->payload;
			$_r='';
			for ($i=0; $i<count($path); $i++){
				$_r.='[\''.$path[$i].'\']';
				if (isset($return[$path[$i]])){
					$return=$return[$path[$i]];
				}else return false;
			}
			return true;
		}
		if (isset($this->payload[$key])) return true;
		return false;
	}
	
	
	/**
	 * Vérifie si une variable de payload est vide
	 * la clé est sous forme de chemin d'accès en string
	 * 
	 * @param string $key clé de la variable de session à vérifier
	 * @return boolean Retourne true si la variable existe sinon false
	 */
	public function payload_empty($key) {
		$path=explode('/', $key);
		if (count($path)>1){
			$return=$this->payload;
			$_r='';
			for ($i=0; $i<count($path); $i++){
				$_r.='[\''.$path[$i].'\']';
				if (isset($return[$path[$i]])){
					$return=$return[$path[$i]];
				}else return false;
			}
			return empty($return);
		}
		if (isset($this->payload[$key]) && empty($this->payload[$key])) return true;
		return false;
	}
	
	/**
	 * Vérifie si une variable de payload est null
	 * la clé est sous forme de chemin d'accès en string
	 * 
	 * @param string $key clé de la variable de session à vérifier
	 * @return boolean Retourne true si la variable existe sinon false
	 */
	public function payload_is_null($key) {
		$path=explode('/', $key);
		if (count($path)>1){
			$return=$this->payload;
			$_r='';
			for ($i=0; $i<count($path); $i++){
				$_r.='[\''.$path[$i].'\']';
				if (isset($return[$path[$i]])){
					$return=$return[$path[$i]];
				}else return false;
			}
			return is_null($return);;
		}
		if (isset($this->payload[$key]) && is_null($this->payload[$key])) return true;
		return false;
	}
}

?>

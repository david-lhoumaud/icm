<?php
class QueriesMysqlProxyCreateIp {

	public function __construct(){
		return;
	}

	public function proxyID() {
		return "CREATE TABLE IF NOT EXISTS `proxy-bot-id` (
		  `host` varchar(16) NOT NULL,
			`port` int(11) NOT NULL,
			`id` varchar(32) NOT NULL DEFAULT 0,
			`job` varchar(5) NOT NULL DEFAULT '',
			`date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			CONSTRAINT `proxy_id` UNIQUE (`host`, `port`, `id`, `job`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	}

	public function execute() {
		return "CREATE TABLE IF NOT EXISTS `proxy-bot` (
		  `host` varchar(255) NOT NULL,
			`port` int(11) NOT NULL,
			`scheme` varchar(255) NOT NULL,
			`date_use` datetime NULL,
			`error` int(11) NOT NULL DEFAULT 0,
			`date_error` datetime NULL,
			`black` int(2) NOT NULL DEFAULT 0,
			`date_black` datetime NULL,
			`date_start` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`date_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			CONSTRAINT `proxy` UNIQUE (`host`, `port`, `scheme`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	}
}
?>

<?php
class QueriesMysqlProxyUpdateIp {

	public function __construct(){
		return;
	}

	public function insert(
		$host,
		$port,
		$scheme,
		$error=false,
		$black=false
	) {
		if (is_bool($black))$black=($black?1:0);
		if (is_bool($error))$error=($error?1:0);
		$date=date("Y-m-d H:i:s");
		return "INSERT INTO `proxy-bot`
			(
				`date_start`,
				`host`,
				`port`,
				`scheme`,
				`error`,
				`black`
			)
			VALUES (
				'$date',
				'$host',
				'$port',
				'$scheme',
				'$error',
				'$black'
			) ON DUPLICATE KEY UPDATE `error`=(error+$error);
		";
	}

	public function error(
		$host,
		$port=null,
		$scheme=null
	) {
		$date=date("Y-m-d H:i:s");
		$where="WHERE `host` LIKE '$host'";
		if (!is_null($port)) $where.=' AND `port`='.$port;
		if (!is_null($scheme)) $where.=" AND `scheme` LIKE '".$scheme."'";
		return "UPDATE `proxy-bot` SET `error`=(error+1), `date_error`='$date' $where";
	}

	public function black(
		$host,
		$port=null,
		$scheme=null
	) {
		$date=date("Y-m-d H:i:s");
		$where="WHERE `host` LIKE '$host'";
		if (!is_null($port)) $where.=' AND `port`='.$port;
		if (!is_null($scheme)) $where.=" AND `scheme` LIKE '".$scheme."'";
		return "UPDATE `proxy-bot` SET `black`=1, `date_black`='$date' $where";
	}

	public function use(
		$host,
		$port=null,
		$scheme=null
	) {
		$date=date("Y-m-d H:i:s");
		$where="WHERE `host` LIKE '$host'";
		if (!is_null($port)) $where.=" AND `port`=".$port;
		if (!is_null($scheme)) $where.=" AND `scheme` LIKE '".$scheme."'";
		return "UPDATE `proxy-bot` SET `date_use`='$date' $where";
	}

	public function insertID(
		$host,
		$port,
		$id=0,
		$job=''
	) {
		return "INSERT INTO `proxy-bot-id`
			(
				`host`,
				`port`,
				`id`,
				`job`
			)
			VALUES (
				'$host',
				'$port',
				'$id',
				'$job'
			);";
	}

}
?>

<?php
/**
 * Classe de récupération des seeders
 * 
 * @return stdClass
 */
class Seeder {
	
	public function __construct() {}
	
	/**
	 * Récupère le fichier de flux de donnée
	 * 
	 * @param string $string sous forme de chemin
	 * @return array|object|boolean
	 */
	static function load($name) {
		if (file_exists(__DIR__.'/seeeders/'.$name.'.json')) return json_decode(file_get_contents(__DIR__.'/seeeders/'.$name.'.json'), true);
		else return false;
	}
	
}
?>
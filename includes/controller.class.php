<?php
/**
 * Classe de routage.
 * On peut accéder à cette classe en y faisant appel normalement : new Controller($options) \
 * Mais on peut aussi l'utiliser comme extends,  \
 * dans ce cas il faut utiliser a methode __web() pour l'initialiser 
 * 
 * @return stdClass
 */
class Controller extends Payloader {

	/**
	 * Classe de sortie de flux
	 * @var stdClass $Output
	 */
	protected $Output;
	/**
	 * Classe de gestion des logs
	 * @var stdClass $Log
	 */
	protected $Log;

	protected $tokens;

	/**
	 * Classe de gestion de session
	 * @var stdClass $session
	 */
	protected $session;
	/**
	 * Schema de la requête
	 * @var string $scheme
	 */
	private $scheme;
	/**
	 * Méthode de la requête
	 * @var string $method
	 */
	private $method;
	/**
	 * Adresse du serveur
	 * @var string $host
	 */
	private $host;
	/**
	 * URI de la requête
	 * @var string $uri
	 */
	private $uri;
	/**
	 * Adresse IP du serveur
	 * @var string $iphost
	 */
	private $iphost;
	/**
	 * Entête de la requête
	 * @var array $headers
	 */
	private $headers;
	/**
	 * Racine client
	 * @var string $clienthost
	 */
	private $clienthost;
	/**
	 * Adresse IP du client
	 * @var string $ip
	 */
	private $ip;
	
	/**
	 * Token du client
	 * @var string $scheme
	 */
	private $token;
	/**
	 * Permission du client
	 * @var string $is
	 */
	private $is;
	
	/**
	 * Options de lancement
	 * @var array $options
	 */
	private $options;
	/**
	 * Initialise les options getOpts si on passe par la ligne de commande
	 * 
	 * @param array|null $_opts Liste des options getOpts
	 * @return void
	 */
	public function __construct($_opts=null) {
		if (!is_null($_opts)) $this->options=$_opts;
		self::__config();
	}
	
	/**
	 * Methode utilisée si on passe par le service web.
	 * Contrairement au constructeur, on vérifie si l'utilisateur à la permission d'accéder au contenu
	 * 
	 * @return void
	 */
	public function __web() {
		$this->Output=Helpers::load('output');
		$this->Output->report(false);
		$this->session=Helpers::load('session');
		self::__config();
		self::checkPermission();
	}
	
	/**
	 * Initialisation des données de la requête dans le payload.
	 * Payload client : Données liés à l'utilisateur \
	 * Payload server : Données liés au serveur \
	 * Payload request : Données liés à la requête web \
	 * Payload maps : Classes de mappages 'maps/input' 'maps/process' 'maps/ouput' \
	 * Payload files : Fichiers d'entrée 'files/input' et de sortie 'files/ouput'
	 *
	 * @return void
	 */
	public function  __config(){
		$this->Output=Helpers::load('output');
		$this->Output->report();
		$this->session=Helpers::load('session');
		parent::__construct();
		$this->scheme=$_SERVER['REQUEST_SCHEME']??'';
		if (!empty($this->scheme))$this->scheme.='://';
		$this->host=$_SERVER['HTTP_HOST']??dirname(__DIR__);
		$this->uri=explode('?',$_SERVER['REQUEST_URI']??'')[0];
		$this->iphost=$_SERVER['SERVER_ADDR']??'';
		$this->clienthost=realpath('.');
		$this->ip=$_SERVER['REMOTE_ADDR']??'';
		$this->method=strtolower($_SERVER['REQUEST_METHOD']??'CMD');
		$this->Log=Helpers::load('log', ($this->method=='cmd'?'cmd':'standard'));
		$this->headers=self::getHeaders();
		$this->token='';
		$this->is='public';
		if (isset($this->headers['icmauth']) && !empty($this->headers['icmauth'])){
			//14406d2d1a500e17fe5b5387d83816573e9ea3aa
			$this->token=sha1($this->headers['icmauth']);
		} else if (isset($_SESSION['icmauth'])){
			$this->token=$_SESSION['icmauth'];
		}
		
		$this->payload(
			'client', 
			array(
				'host'=>$this->clienthost,
				'ip'=>$this->ip,
				'is'=>$this->is,
				'token'=>$this->token
			)
		);
		
		if (isset($this->headers['icmx']) || isset($this->options['l']) ){
			$this->payload(
				'client/limit-output',
				$this->headers['icmx']??($this->options['l']??'output')
			);
		}
		
		$this->payload(
			'server', 
			array(
				'host'=>$this->host,
				'ip'=>$this->iphost,
				'is'=>$this->for??($this->method=='cmd'?'public':'root')
			)
		);
		
		$is_embed=($this->options['e']??false);
		if ($this->options['q']??false && ($this->method=='cmd' || $is_embed) ){
			parse_str($this->options['q'], $_GET);
		}
		$this->payload(
			'request', 
			array(
				'method'=>$this->method,
				'uri'=>$this->uri,
				'scheme'=>$this->scheme,
				'host'=>$this->host,
				'params'=>array(
					'get'=>$_GET??array(),
					'post'=>$_POST??array(),
					'cookie'=>$_COOKIE??array(),
					'files'=>$_FILES??array(),
					// 'server'=>$_SERVER??array(),
				),
				'headers'=>$this->headers
			)
		);
		
		if ($this->options['e']??false) {
			$this->payload(
				'maps', 
				array(
					'is_embedded'=>true,
					'input'=>($this->options['I']??''),
					'process'=>($this->options['P']??''),
					'output'=>($this->options['O']??''),
				)
			);
			$this->payload(
				'files', 
				array(
					'input'=>($this->options['i']??''),
					'output'=>($this->options['o']??''),
				)
			);
			$this->payload(
				'raws', $this->options['r']??''
			);
		} else {
			$this->payload(
				'maps', 
				array(
					'is_embedded'=>false,
					'input'=>$this->headers['icmic']??($this->options['I']??''),
					'process'=>$this->headers['icmpc']??($this->options['P']??''),
					'output'=>$this->headers['icmoc']??($this->options['O']??''),
				)
			);
			$this->payload(
				'files', 
				array(
					'input'=>$this->headers['icmif']??($this->options['i']??''),
					'output'=>$this->headers['icmof']??($this->options['o']??''),
				)
			);
			$this->payload(
				'raws', $_POST['icmraws']??($_GET['icmraws']??($this->headers['icmraws']??($this->options['r']??'')))
			);
		}
		
		
	}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __init(){}
	
	/**
	 * Entrée du flux.
	 * Le payload est datas/input
	 * 
	 * @return void
	 */
	public function __input(){}
	
	/**
	 * Généralisation du flux.
	 * Le payload est datas/process
	 * 
	 * @return void
	 */
	public function __process(){}
	
	/**
	 * Sortie du flux.
	 * Le payload est datas/output
	 * 
	 * @return void
	 */
	public function __output(){
		if ($this->payload('maps/is_embedded')) return $this->payload;
		else $this->Output->json($this->payload, true);
	}
	
	/**
	 * Vérifie la permission de l'utilisateur.
	 * Enregistre le type de l'utlisateur dans le payload 'client/is'
	 * 
	 * @return void
	 */
	public function checkPermission($_for=null, $step=null) {
		if (!is_null($_for))$this->for=$_for;
		$permission=$this->for??'root';
		$is_root=(self::getReferencedToken('root')==$this->token);
		$is_guest=false;
		$is_guest_name='';
		foreach (self::getReferencedToken() as $k => $t) {
			if ($k=='root') continue;
			if($t==$this->token){
				$is_guest_name=$k;
				$is_guest=true;
				break;
			}
		}
		$is_public=(!$is_root && !$is_guest);
		
		if ($is_root)$this->is='root';
		else if ($is_guest)$this->is=$is_guest_name;
		$this->payload('client/is',$this->is);
		
		if (is_array($permission)){
			$access=false;
			foreach ($permission as $p){
				if ($p=='public' || $is_root || $is_guest_name==$p) {
					$access=true;
					break;
				}
			}
			if (!$access){
				if (!$is_guest) $this->Output->error(401, null, $step);
				else $this->Output->error(403, null, $step);
			}
		} else {
			if ($permission!='public' && !$is_root){
				if ($permission=='root' && $is_guest){
					$this->Output->error(403, null, $step);
				} else if ($is_guest && $is_guest_name!=$permission){
					$this->Output->error(403, null, $step);
				} else if (!$is_guest){
					$this->Output->error(401, null, $step);
				}
			}
		}
		
	}

	
	/**
	 * Get headers request.
	 * 
	 * @param string $function_name nom de la  fonction à utiliser si possible
	 * @return array Tableau associatif (string)key => (string)value
	 */
	private function getHeaders($function_name='getallheaders'){
		$all_headers=array();
		if(function_exists($function_name)){ 
			foreach ($function_name() as $name=> $value){
				$name=strtolower(str_replace(' ', '-', str_replace('_',' ',$name)));
				$all_headers[$name]=$value;
			}
		}else{
			foreach($_SERVER as $name => $value){
				if(substr($name,0,5)=='HTTP_'){
					$name=strtolower(str_replace(' ', '-', str_replace('_',' ',substr($name,5))));
					$all_headers[$name] = $value; 
				}elseif(substr($name,0,8)=='REQUEST_'){
					$name=strtolower(str_replace(' ', '-', str_replace('_',' ',substr($name,8))));
					$all_headers[$name] = $value; 
				}elseif($function_name=='apache_request_headers'){
					$all_headers[$name] = $value; 
				}
			}
		}
		return $all_headers;
	}
	
	/**
	 * Retourne la liste des utilisateurs 
	 * 
	 * @return array liste des utilisateurs
	 */
	public function getReferencedUser() {
		$users=array();
		foreach(self::getReferencedToken() as $usr=>$token){
			$users[]=$usr;
		}
		return $users;
	}
	
	/**
	 * Retourne la valeur d'une clé.
	 * 
	 * @param string|null $key Clé à récupérer
	 * @return string|array Valeur de la clé si elle existe sinon false
	 */
	private function getReferencedToken($key=null) {
		if (!isset($this->tokens)) {
			if (isset($this->headers['icmauth']) && !empty($this->headers['icmauth'])){
				$this->tokens=$this->json(__DIR__.'/keys/api.json');
			} else {
				$this->tokens=$this->json(__DIR__.'/keys/root.json');
			}
		}
		if (!is_null($key) && isset($this->tokens[$key])) return $this->tokens[$key];
		return $this->tokens;
	}
	
}

?>

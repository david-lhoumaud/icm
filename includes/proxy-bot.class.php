<?php
class ProxyBot {
	private $response;
	private $ips;
	private $filters;
	private $error;
	private $ch;
	private $core;

	public function __construct($DB=NULL){
		$this->response="";
		$this->ips=array();
		$this->filters=array();
		$this->error="";
		$this->ch = NULL;
		if (!is_null($DB))$this->config= (file_exists('config.json')?json_decode(file_get_contents('config.json'),true):array('mysql'=>NULL));
		$this->DB=(!is_null($DB)?$DB:Helpers::load('mysql', $this->config['mysql']));
		$this->DB->query(Helpers::load('mysql/proxy/create/ip')->execute());
		$this->DB->query(Helpers::load('mysql/proxy/create/ip')->proxyID());
		$this->core=Helpers::load('core');
		$this->DB->query(Helpers::load('mysql/proxy/delete/ip')->error());
	}

	/**
	 * @desc Récupère la liste des addresse ip des proxys libre disponible
	 */
	public function getIPList($timeout=1, $anon='all'){
		$url=array(
			// ['https://free-proxy-list.net/', 'html'],
			// ['https://www.us-proxy.org', 'html'],
			// ['https://www.sslproxies.org/', 'html'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://gimmeproxy.com/api/getProxy?get=true&supportsHttps=true&protocol=http', 'json'],
			// ['https://proxy11.com/api/proxy.txt?key=MTk2.XRDlzg.ayXTQzhpPFbp1-RkJbn1Pf7PIYk', 'txt'],
			// ['http://pubproxy.com/api/proxy?format=txt&type=http&limit=5&https=true', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=FR', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=BE', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=ES', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=DE', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=NL', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=IT', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=PT', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=GB', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=RO', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=RU', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=LI', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=PO', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=UK', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=HU', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=GR', 'txt'],
			['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=CZ', 'txt'],
			// ['https://www.proxy-list.download/api/v1/get?type=https&anon='.$anon.'&country=US', 'txt'],
			// ['https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list-raw.txt', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=fr&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=be&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=es&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=it&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=pt&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=de&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=nl&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=gb&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=ro&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=ru&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=li&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=po&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=uk&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=hu&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=gr&ssl=all&anonymity='.$anon.'', 'txt'],
			['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=cz&ssl=all&anonymity='.$anon.'', 'txt'],
			// ['https://api.proxyscrape.com/?request=getproxies&proxytype=https&timeout=10000&country=us&ssl=all&anonymity='.$anon.'', 'txt'],
			// ['http://multiproxy.org/txt_anon/proxy.txt', 'txt'],
		);

		for ($i=0; $i<count($url); $i++) { //
			$this->ch = curl_init();
			$this->core->log($url[$i][0]);
			curl_setopt_array($this->ch, array(
				CURLOPT_URL => $url[$i][0],
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 5,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Accept: */*",
					"Cache-Control: no-cache",
					"Connection: keep-alive",
					"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0",
					"accept-encoding: gzip, deflate",
					"cache-control: no-cache"
				),
			));
			$this->curl();
			if($this->code==200 || $this->code==429) $this->ipFilters($url[$i][1], $timeout);
			else continue;
			//$array=	explode("\n",str_replace("\r",'',$this->response));

		}
		return count($this->ips);
	}

	public function ipFilters($id_listproxy='txt',$timeout=1, $start=0, $num=NULL, $inc=0){
		switch ($id_listproxy) {
			case 'txt': $this->parseAndFiltersTXT($timeout, $start, $num, $inc); break;
			case 'html': $this->parseAndFiltersHTML($timeout, $start, $num, $inc); break;
			case 'json': $this->parseAndFiltersJSON($timeout, $start, $num, $inc); break;
			default: $this->parseAndFiltersTXT($timeout, $start, $num, $inc); break;
		}
	}


	public function parseAndFiltersJSON($timeout=1, $start=0, $num=NULL, $inc=0){
		if ($inc==0)$this->filters=$this->response;
		else $this->response=$this->filters;
		$array=	json_decode($this->response,true);
			if (!isset($array['ipPort'])) return;
			$ip=explode(":",$array['ipPort']);
			if (!isset($ip[0]) || !isset($ip[1])) return;
			if (self::pingSys($ip[0], $timeout)){
				$this->core->log("\033[32m".'[1/1] '.$this->code."\t".'https'."\t".$ip[0]."\t:".$ip[1]);
				$this->ips[]=array(
					'host'=>$ip[0],
					'port'=>$ip[1],
					'scheme'=>'https'
				);
				$this->DB->query(Helpers::load('mysql/proxy/update/ip')->insert($ip[0],$ip[1], 'https', false, false));
			 } else {
			 	$this->core->log("\033[31m".'[1/1] '.$this->code."\t".'https'."\t".$ip[0]."\t:".$ip[1]);
			 }
	}

	public function parseAndFiltersTXT($timeout=1, $start=0, $num=NULL, $inc=0){
		if ($inc==0)$this->filters=$this->response;
		else $this->response=$this->filters;
		$array=	explode("\n",str_replace("\r",'',$this->response));
		if (is_null($num) || $num<=0)$num=count($array);
		$id_ping_service=0;
		for ($i=$start; $i<($num+$start); $i++){ //count($array)
			if (!isset($array[$i])) continue;
			$ip=explode(":",$array[$i]);
			if (!isset($ip[0]) || !isset($ip[1])) continue;
			if (self::pingSys($ip[0], $timeout)){
				$this->core->log("\033[32m".'['.($i+1).'/'.($num+$start).'] '.$this->code."\t".'https'."\t".$ip[0]."\t:".$ip[1]);
				$this->ips[]=array(
					'host'=>$ip[0],
					'port'=>$ip[1],
					'scheme'=>'https'
				);
				$this->DB->query(Helpers::load('mysql/proxy/update/ip')->insert($ip[0],$ip[1], 'https', false, false));
			} else {
				$this->core->log("\033[31m".'['.($i+1).'/'.($num+$start).'] '.$this->code."\t".'https'."\t".$ip[0]."\t:".$ip[1]);
			}
		}
	}

	/**
	 * @desc Filtre les addresses ip récupérer chez le fournisseur pour les tester
	 * @param Integer $timeout delay de temps de Connexion
	 * @param Integer $start get ip to 
	 * @param Integer|NULL $num nombre d'ip à récupérer
	 */
	public function parseAndFiltersHTML($timeout=1, $start=0, $num=NULL){

		$xml_=explode('id="proxylisttable">',$this->response);
		$xml = explode('</table',$xml_[1]);
		$ob= simplexml_load_string("<table>".preg_replace("/class='.'/", '',preg_replace('/class="."/', '',$xml[0]))."</table>");
		$ips_parse=json_decode(json_encode($ob),true);
		unset($ips_parse["thead"]);
		unset($ips_parse["tfoot"]);

		if (is_null($num) || $num<=0)$num=count($ips_parse["tbody"]["tr"]);
		$id_ip=0;
		for($i=$start; $i<$num; $i++){
			if (!isset($ips_parse["tbody"]["tr"][$i]) && !isset($ips_parse["tbody"]["tr"][$i]["td"])) continue;
			$data=$ips_parse["tbody"]["tr"][$i]["td"];
			$host=$data[0];
			$port=$data[1];
			$iso2=$data[2];
			$country=$data[3];
			$scheme=($data[6]=='no'?'http':'https');
			$delay=str_replace(
				array(' hours ', ' hour ', ' minutes ', ' minute ',' seconds ',	' second ', 'ago'),
				array('h','h','m','m','s','s',''),
				(isset($data[7])?$data[7]:'')
			);

			if ($scheme=='http' || $host==null || $port==null) {
				//$num++;
				continue;
			}
			// $this->pingWith($host, $port, $timeout, $id_ip++);
			// if ($id_ip==2)$id_ip=0;
			// if ($this->code >= 200 && $this->code < 300) echo "\033[32m";
			// else echo "\033[31m";
			// echo '['.($i+1).'/'.$num.'] '.$this->code."\t".$scheme."\t".$host."\t:".$port."\t".$delay."\t".$iso2."\n\033[0m";
			// if (!empty($this->info["primary_ip"]) && ($this->code >= 200 && $this->code < 300)){
			if (self::pingSys($host, $timeout)){
				$this->core->log("\033[32m".'['.($i+1).'/'.$num.'] '.$this->code."\t".$scheme."\t".$host."\t:".$port."\t".$delay."\t".$iso2);
				$this->ips[]=array(
					'host'=>$host,
					'port'=>$port,
					'iso'=>$iso2,
					'delay'=>$delay,
					'country'=>$country,
					'scheme'=>$scheme,
				);
				//echo Helpers::load('mysql/proxy/update/ip')->insert($host,$port, $scheme, false, false); exit;
				$this->DB->query(Helpers::load('mysql/proxy/update/ip')->insert($host,$port, $scheme, false, false));
			} else {
				$this->core->log("\033[31m".'['.($i+1).'/'.$num.'] '.$this->code."\t".$scheme."\t".$host."\t:".$port."\t".$delay."\t".$iso2);
			}
		}
		//$this->ips=json_decode(json_encode($this->filters), true);
	}

	private function pingSys($host, $timeout){
		return true;
		$handle = popen("ping -c1 $host -W$timeout 2>&1","r");
		$ping = fread($handle, 2096);
		pclose($handle);
		if(preg_match("/100\% packet loss/", $ping)){
			return false;
		}
		return true;
	}


	/**
	 * @desc effectue un ping avec une ip de proxy
	 */
	private function pingWith($host, $port, $timeout, $id_ip){
		$url = array('https://mon-ip.io', 'http://www.monip.org/', 'https://ping.eu/ping/');
		$this->ch = curl_init();
		curl_setopt($this->ch, CURLOPT_URL,$url[$id_ip]);
		curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($this->ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($this->ch, CURLOPT_HTTPPROXYTUNNEL, false);
		curl_setopt($this->ch, CURLOPT_PROXY, $host.':'.$port);
		curl_setopt($this->ch, CURLOPT_NOBODY, true);
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_HEADER, false);
		curl_setopt($this->ch, CURLOPT_AUTOREFERER, true);
		curl_setopt($this->ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
		$this->curl();
	}
	/**
	 * @desc sauvegarde les addresse filtré dans un fichier JSON
	 */
	public function ipSave($filename='ips.json') {
		if (count($this->ips)>0){
			shuffle($this->ips);
			file_put_contents(
				$filename,
				str_replace(
					array('},{', '[{', '}]'),
					array("},\n\t{","[\n\t{","}\n]"),
					json_encode($this->ips)
				)
			);
		}
	}

	/**
	 * @desc retour le tableau d'ip
	 * @return Array $this->ips tableau des adresses ips
	 */
	public function loadIPSFile($filename='ips.json') {
		if (file_exists($filename)){
			$this->ips=json_decode(file_get_contents($filename),true);
			return $this->ips;
		} else return array();
	}

	/**
	 * @desc retour le tableau d'ip
	 * @return Array $this->ips tableau des adresses ips
	 */
	public function getIP() {
		return $this->ips;
	}

	public function setIP($array=array()) {
		$this->ips=$array;
	}

	/**
	 * @desc appel curl et fermeture
	 */
	private function curl(){
		$this->response = curl_exec($this->ch);
		$this->error = curl_error($this->ch);
		$this->info = curl_getinfo($this->ch);
		$this->code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
		//if ($this->error)die("cURL Error #:" . $this->error);
		curl_close($this->ch);
	}
}

?>

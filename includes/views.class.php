<?php
require_once 'loader.class.php';
require_once 'helpers.class.php';
/**
 * Charge les classes d'outils
 */
class Views extends Loader{
	/**
	 * Charge une classe d'outil avec un chemin relatif au répertoire de la classe courante et sans extention de nom de fichier.
	 * Le nom de la classe du helper, doit avoir le prefix 'Helpers'. \
	 * exemple : HelpersClassname \
	 * \
	 * Si le helpers est dans un sous dossier le nom de la classe devra comporter le nom du repertoire parent. \
	 * exemple : HelpersDirnameClassname
	 * 
	 * @param string $classPath chemin d'accès de la classe
	 * @param mixed|null $params paramètre envoyé à la classe
	 * @return object|false
	 */
	static function load($classPath, $params=NULL){
		return self::load_class(
			self::classpath_to_classname($classPath, 'views'),
			$classPath, 
			$params, 
			'views'
		);
	}
}

?>

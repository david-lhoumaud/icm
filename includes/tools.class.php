<?php
/**
 * Classe de gestion des outils.
 * Cette classe charge un certain nombre de classes : \
 * $this->Log gère l'affichage des logs \
 * $this->DB gère la base de donnée \
 * $this->Parsing permet de faire des traitement sur des chaine de caractère \
 * $this->Curl permet d'envoyer des requetes web \
 * $this->UserAgent permet de choisir une liste d'user-agent référencé
 * 
 * @return stdClass
 */
class Tools {
	/** 
	 * Classe qui gère l'affichage des logs
	 * @var stdClass $Log 
	 */
	protected $Log;
	/** 
	 * Classe qui gère la base de donnée 
	 * @var stdClass $DB
	 */
	protected $DB;
	/** 
	 * Classe qui gère les traitements dans les chaine de caractère 
	 * @var stdClass $Parsing
	 */
	protected $Parsing;
	/** 
	 * Classe qui gère les requête web
	 * @var stdClass $Curl
	 */
	protected $Curl;
	/** 
	 * Classe qui gère les user-agent
	 * @var stdClass $UserAgent
	 */
	protected $UserAgent;
	
	public function __construct() {
		if (!isset($this->config)) $this->config = Helpers::load('config')->get();
		$this->Log								= Helpers::load('log', ($this->payload('request/method')=='cmd'?'cmd':'standard'));
		$this->DB								= (!is_null($this->config['mysql']??null)?Helpers::load('mysql', $this->config['mysql']):Helpers::load('mysql'));
		if (isset($this->use_proxy) && $this->use_proxy){
			require_once __DIR__.'/proxy-bot.class.php';
			$this->proxy						= array();
			$this->ProxyBot					=	new ProxyBot($this->DB);
			$this->ProxySelect			= Queries::load('mysql/proxy/select/ip');
			$this->ProxyUpdate			= Queries::load('mysql/proxy/update/ip');
			$this->ProxyDelete			= Queries::load('mysql/proxy/delete/ip');
		}
		$this->Parsing					= Helpers::load('parsing');
		$this->Curl							= Helpers::load('curl');
		$this->UserAgent				= Helpers::load('userAgent');
		if (isset($this->config['curl'])){
			if (isset($this->config['curl']['connect_timeout'])) $this->Curl->setConnectTimeout($this->config['curl']['connect_timeout']);
			if (isset($this->config['curl']['timeout'])) $this->Curl->setTimeout($this->config['curl']['timeout']);
			if (isset($this->config['curl']['proxy']) && $this->use_proxy) $this->Curl->proxy($this->config['curl']['proxy']['host'], $this->config['curl']['proxy']['port'], true);
			if (isset($this->config['curl']['user-agent'])) $this->Curl->setUserAgent($this->config['curl']['user-agent']);
		}
	}
	
	/**
	 * Stop le processus de façon aléatoire
	 * 
	 * @param integer $min délai minimum
	 * @param integer $max délai maximum
	 * @return void
	 */
	protected function randomSleep($min=0, $max=60){
		$sec	= mt_rand($min, $max);
		sleep($sec);
	}

	/**
	 * Rafraichi la liste des proxy et charge un proxy dans $this->Curl.
	 * 
	 * @param integer $id identifiant associé au proxy
	 * @param string $job Nom du job associé au proxy
	 * @return void
	 */
	protected function refreshProxy($id, $job='') {
		if (!$this->use_proxy) return;
		$c=1;
		$cn=0;
		while ($c>0){
			$this->DB->query($this->ProxySelect->randomWhite());
			$this->proxy=$this->DB->fetchAll();
			if ((count($this->proxy)<=0 || $cn>=10) && $this->refresh_proxy) {
				$cn=0;
				$this->ProxyBot->getIPList(1);
				$this->DB->query($this->ProxySelect->randomWhite());
				$this->proxy=$this->DB->fetchAll();
			}
			if (count($this->proxy)<=0) return; //self::error(666);

			$this->DB->query($this->ProxySelect->countID($this->proxy[0]['host'], $this->proxy[0]['port'], $id, $job));
			$c=$this->DB->fetchAll()[0]['count'];
			if ($c>0) {
				$this->Log->w("Proxy\t".$this->proxy[0]['host'].':'.$this->proxy[0]['port']." à déjà été utilisé");
				$cn++;
			} else {
				$this->Log->w("Proxy\t".$this->proxy[0]['host'].':'.$this->proxy[0]['port']);
			}
		}
		$this->Curl->proxy($this->proxy[0]['host'], $this->proxy[0]['port'], true);
	}
	
	/**
	 * Change l'User-Agent dans $this->Curl.
	 * 
	 * @param string|null $ua User-Agent
	 * @return void
	 */
	protected function refreshUserAgent($ua=null) {
		$ua=$this->Curl->getUserAgent();
		$this->Curl->setUserAgent((!is_null($ua)?$ua:$this->UserAgent->choose()));
		while ($ua==$this->Curl->getUserAgent()){
			$ua=$this->Curl->getUserAgent();
			$this->Curl->setUserAgent((!is_null($ua)?$ua:$this->UserAgent->choose()));
		}
	}
	
	/**
	 * Créer un fichier de cookie pour $this->Curl.
	 * 
	 * @return void
	 */
	protected function createCookie() {
		$this->Curl->createCookieFile();
	}
	
	/**
	 * Nettoye un fichier de cookie pour $this->Curl.
	 * 
	 * @return void
	 */
	protected function cleanCookie() {
		$this->Curl->cleanCookieFile();
	}

	/**
	 * Lance une requête web avec $this->Curl.
	 * 
	 * @param string $method method de requête à utiliser
	 * @param string $url URL de la requete
	 * @param mixed|null $data donnée à envoyer avec la requête
	 * @return array
	 */
	protected function http($method, $url, $data=NULL) {
		if (!is_null($data)) return $this->Curl->$method($url, $data);
		else return $this->Curl->$method($url);
	}
	
	protected function ls($dir='.') {
		$ls=array();
		if (file_exists($dir)){
			if ($handle = opendir($dir)) {
				while (false !== ($entry = readdir($handle))) {
					if ($entry != "." && $entry != "..") {
						if (is_dir($dir.'/'.$entry)) $ls=array_merge($ls, self::ls($dir.'/'.$entry));
						else $ls[]=$dir.'/'.$entry;
					}
				}
				closedir($handle);
			}
		}
		return $ls;
	}

}
?>
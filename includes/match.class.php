<?php
require_once __DIR__.'/data.class.php';
/**
 * Classe de matchage de map JSON
 */
class Matchs extends Data {
	
	public function __construct() {
		parent::__construct();
		$this->Parse=Helpers::load('parsing');
		$this->Str=Helpers::load('string');
	}
	
	/**
	 * Récupère le tableau de mappage dans le répertoire matchs/
	 * 
	 * @param string $string sous forme de chemin
	 * @param boolean $is_object doit-on retourner un objet ou un tableau
	 * @return array|object|boolean
	 */
	protected function map($name, $is_object = false) {
		if (file_exists(__DIR__.'/matchs/'.$name.'.json')) return self::json(__DIR__.'/matchs/'.$name.'.json', $is_object);
		else return false;
	}
	
	protected function match($datas, $map, $is_from=true){
		return self::each($datas, $map, $is_from);
	}
	
	protected function each($datas, $map, $is_from=true){
		if ($is_from) return self::matchFrom($datas, $map);
		return self::matchTo($datas, $map);
	}
	
	/**
	 * Aligne les données au tableau de mappage chargé depuis le répertoire matchs/
	 * 
	 * @param array $datas données à aligner
	 * @param array $map tableau de mappage
	 * @return array
	 */
	protected function matchFrom($datas, $map){
		$output=array();
		foreach ($datas as $key => $value) {
			if (is_string($key)) $key=trim($key);
			if (empty($key)) continue;
			if (is_string($value)) $value=trim($value);
			if (empty($value)) continue;
			if (isset($map[$key])){
				$current_key=explode('&&',$map[$key]);
				self::suffix($output, $current_key, $value);
			}
		}
		return $output;
	}
	
	/**
	 * Aligne les données au tableau de mappage chargé depuis le répertoire matchs/
	 * 
	 * @param array $datas données à aligner
	 * @param array $map tableau de mappage
	 * @return array
	 */
	protected function matchTo($datas, $map){
		$output=array();
		foreach ($datas as $k=>$v) {
			foreach ($map as $outkey => $outvalue) {
				if (isset($datas[$outkey])){
					$value=$datas[$outkey];
					if (is_string($value)) $value=trim($value);
					$current_key=explode('&&',$outvalue);
					self::suffix($output, $current_key, $value);
				}
			}
		}
		return $output;
	}
	
	/**
	 * Aligne les données au tableau de mappage chargé depuis le répertoire matchs/
	 * 
	 * @param array $datas données à aligner
	 * @param array $map tableau de mappage
	 * @return array
	 */
	protected function matchForceTo($datas, $map){
		$output=array();
		foreach ($datas as $k=>$v) {
			foreach ($map as $outkey => $outvalue) {
				if (isset($datas[$outkey])){
					$value=$datas[$outkey];
					if (is_string($value)) $value=trim($value);
					$current_key=explode('&&',$outvalue);
					self::suffix($output, $current_key, $value);
				} else {
					$current_key=explode('&&',$outvalue);
					self::suffix($output, $current_key, '');
				}
			}
		}
		return $output;
	}
	
	/**
	 * Vérifie si la nouvelle clé a des methods à lancer
	 * 
	 * @param array $current_key tableau des clé à écrire
	 * @param string $value valeur à transmettre à la nouvelle clé
	 * @return string valeur traitée
	 */
	private function method($current_key, $value) {
			if (preg_match('/\$\{.*\}.*?$/',$current_key)){
				//echo preg_replace('/^.*\$\{(.*)\}.*?$/','{$1}',$current_key);
				$methods=json_decode(
					preg_replace('/^.*\$\{(.*)\}.*?$/','{$1}',$current_key), 
					true
				);
				//var_dump($methods); exit;
				foreach ($methods as $method=>$params){
					if (method_exists($this, $method)) {
						eval('$value=$this->'.$method.'('.$params.');');
					} else if (method_exists('self', $method)) {
						eval('$value=self::'.$method.'('.$params.');');
					} else if (method_exists('parent', $method)) {
						eval('$value=parent::'.$method.'('.$params.');');
					} else if (method_exists($this->Parse, $method)) {
						eval('$value=$this->Parse->'.$method.'('.$params.');');
					} else if (method_exists($this->Str, $method)) {
						eval('$value=$this->Str->'.$method.'('.$params.');');
					}else if (function_exists($method)) {
						if ($method=='unlink' || $method=='exec') $method='';
						eval('$value='.$method.'('.$params.');');
					}
				}
				return $value;
			} else {
				return '!!NONE_METHODS!!';
			}
	}
	
	/**
	 * Vérifie si la nouvelle clé a un suffix de mappage
	 * 
	 * @param array $output pointer de sortie
	 * @param array $current_key tableau des clé à écrire
	 * @param string $value valeur à transmettre à la nouvelle clé
	 */
	private function suffix(&$output, $current_key, $value) {
		for ($i=0; $i<count($current_key);$i++){
			$_v=self::method($current_key[$i], $value);
			if ($_v!='!!NONE_METHODS!!'){
				$CK=preg_replace('/\$.*$/', '', $current_key[$i]);
				$value=$_v;
				$output[(string)"$CK"]=$_v;
				$current_key[$i]=preg_replace('/\$\{.*\}/', '', $current_key[$i]);
				//break;
			}
			if (preg_match('/\$\+.+$/',$current_key[$i])){
				$str=preg_replace('/^.*\$\+(.+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\$\+.+$/', '', $current_key[$i]);
				$output[(string)"$CK"]=(string)$value.$str;
			}elseif (preg_match('/\$\..+$/',$current_key[$i])){
				$str=preg_replace('/^.*\$\.(.+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\$\..+$/', '', $current_key[$i]);
				$output[(string)"$CK"]=(string)$str;
				
			}elseif (preg_match('/\+\+.+$/',$current_key[$i])){
				$str=preg_replace('/^.*\+\+(.+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\+\+.+$/', '', $current_key[$i]);
				$output[(string)"$CK"].=(string)$value.$str;
			}elseif (preg_match('/\+\..+$/',$current_key[$i])){
				$str=preg_replace('/^.*\+\.(.+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\+\..+$/', '', $current_key[$i]);
				$output[(string)"$CK"].=(string)$str;
				
			}elseif (preg_match('/\+n$/',$current_key[$i])){
				$CK=str_replace('+n', '', $current_key[$i]);
				$output[(string)"$CK"].=(string)"\n".$value;
			}elseif (preg_match('/\+s$/',$current_key[$i])){
				$CK=str_replace('+s', '', $current_key[$i]);
				$output[(string)"$CK"].=(string)" ".$value;
			}elseif (preg_match('/\+$/',$current_key[$i])){
				$CK=str_replace('+', '', $current_key[$i]);
				$output[(string)"$CK"].=(string)$value;
				
			}elseif (preg_match('/\+F$/',$current_key[$i])){
				$CK=str_replace('+F', '', $current_key[$i]);
				$output[(string)"$CK"]+=(float)$value;
			}elseif (preg_match('/\-F$/',$current_key[$i])){
				$CK=str_replace('-F', '', $current_key[$i]);
				$output[(string)"$CK"]-=(float)$value;
			}elseif (preg_match('/\*F$/',$current_key[$i])){
				$CK=str_replace('*F', '', $current_key[$i]);
				$output[(string)"$CK"]*=(float)$value;
			}elseif (preg_match('/\/F$/',$current_key[$i])){
				$CK=str_replace('/F', '', $current_key[$i]);
				$output[(string)"$CK"]/=(float)$value;
			}elseif (preg_match('/\$F$/',$current_key[$i])){
				$CK=str_replace('$F', '', $current_key[$i]);
				$output[(string)"$CK"]=(float)$value;
			}elseif (preg_match('/\$Fv$/',$current_key[$i])){
				$CK=str_replace('$Fv', '', $current_key[$i]);
				$output[(string)"$CK"]=str_replace('.', ',', (string)$value);
				
			}elseif (preg_match('/\+F\.\d+$/',$current_key[$i])){
				if (preg_match('/,/',$value)) $value=(float)str_replace([',', ' '],['.', ''], $value);
				$dec=preg_replace('/^.*\+F\.(\d+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\+F\.\d+$/', '', $current_key[$i]);
				if (empty($value)) $value=0;
				$output[(string)"$CK"]+=(float)number_format((float)$value, $dec, '.', '');
			}elseif (preg_match('/\-F\.\d+$/',$current_key[$i])){
				if (preg_match('/,/',$value)) $value=(float)str_replace([',', ' '],['.', ''], $value);
				$dec=preg_replace('/^.*\-F\.(\d+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\-F\.\d+$/', '', $current_key[$i]);
				if (empty($value)) $value=0;
				$output[(string)"$CK"]-=(float)number_format((float)$value, $dec, '.', '');
			}elseif (preg_match('/\*F\.\d+$/',$current_key[$i])){
				if (preg_match('/,/',$value)) $value=(float)str_replace([',', ' '],['.', ''], $value);
				$dec=preg_replace('/^.*\*F\.(\d+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\*F\.\d+$/', '', $current_key[$i]);
				if (empty($value)) $value=0;
				$output[(string)"$CK"]*=(float)number_format((float)$value, $dec, '.', '');
			}elseif (preg_match('/\/F\.\d+$/',$current_key[$i])){
				if (preg_match('/,/',$value)) $value=(float)str_replace([',', ' '],['.', ''], $value);
				$dec=preg_replace('/^.*\/F\.(\d+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\/F\.\d+$/', '', $current_key[$i]);
				if (empty($value)) $value=0;
				$output[(string)"$CK"]/=(float)number_format((float)$value, $dec, '.', '');
			}elseif (preg_match('/\$F\.\d+$/',$current_key[$i])){
				if (preg_match('/,/',$value)) $value=(float)str_replace([',', ' '],['.', ''], $value);
				$dec=preg_replace('/^.*\$F\.(\d+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\$F\.\d+$/', '', $current_key[$i]);
				if (empty($value)) $value=0;
				$output[(string)"$CK"]=(float)number_format((float)$value, $dec, '.', '');
			}elseif (preg_match('/\$Fv\.\d+$/',$current_key[$i])){
				if (preg_match('/,/',$value)) $value=(float)str_replace([',', ' '],['.', ''], $value);
				$dec=preg_replace('/^.*\$Fv\.(\d+)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\$Fv\.\d+$/', '', $current_key[$i]);
				if (empty($value)) $value=0;
				$output[(string)"$CK"]=number_format((float)$value, $dec, ',', '');
				
			}elseif (preg_match('/\+I$/',$current_key[$i])){
				$CK=str_replace('+I', '', $current_key[$i]);
				$output[(string)"$CK"]+=(integer)$value;
			}elseif (preg_match('/\-I$/',$current_key[$i])){
				$CK=str_replace('-I', '', $current_key[$i]);
				$output[(string)"$CK"]-=(integer)$value;
			}elseif (preg_match('/\*I$/',$current_key[$i])){
				$CK=str_replace('*I', '', $current_key[$i]);
				$output[(string)"$CK"]*=(integer)$value;
			}elseif (preg_match('/\/I$/',$current_key[$i])){
				$CK=str_replace('/I', '', $current_key[$i]);
				$output[(string)"$CK"]/=(integer)$value;
			}elseif (preg_match('/\$I$/',$current_key[$i])){
				$CK=str_replace('$I', '', $current_key[$i]);
				$output[(string)"$CK"]=(integer)$value;
			}elseif (preg_match('/\$A$/',$current_key[$i])){
				$CK=str_replace('$A', '', $current_key[$i]);
				$output[(string)"$CK"][]=$value;
			}elseif (preg_match('/\$A!.$/',$current_key[$i])){
				$char=preg_replace('/^.*\$A!(.)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\$A!.$/', '', $current_key[$i]);
				$output[(string)"$CK"]=explode($char, $value);
			}elseif (preg_match('/\$_.$/',$current_key[$i])){
				$char=preg_replace('/^.*\$_(.)$/', '$1', $current_key[$i]);
				$CK=preg_replace('/\$_.$/', '', $current_key[$i]);
				$output[(string)"$CK"]=implode($char, $value);
			}elseif (preg_match('/\$B$/',$current_key[$i])){
				$CK=str_replace('$B', '', $current_key[$i]);
				$value=strtolower($value);
				switch((string)$value){
					case 'o':
					case 'y':
					case 't':
					case 'true':
					case 'ok':
					case '1':
					case 'oui':
					case 'yes': 
						$value=true; 
					break;
					case 'n':
					case 'f':
					case 'false':
					case 'ko':
					case '0':
					case 'non':
					case 'no': 
						$value=false; 
					break;
				}
				$output[(string)"$CK"]=(boolean)$value;
			}else {
				$CK=$current_key[$i];
				$output[(string)"$CK"]=(string)$value;
			}
		}
	}
}
?>
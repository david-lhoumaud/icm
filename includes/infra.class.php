<?php
/**
 * Classe de récupération des infras
 * 
 * @return stdClass
 */
class Infra {
	
	public function __construct() {}
	
	/**
	 * Récupère le fichier de configuration d'une infrastructure
	 * 
	 * @param string $string sous forme de chemin
	 * @return array|object|boolean
	 */
	static function load($name) {
		if (file_exists(__DIR__.'/infras/'.$name.'.json')) return json_decode(file_get_contents(__DIR__.'/infras/'.$name.'.json'), true);
		else return false;
	}
	
}
?>
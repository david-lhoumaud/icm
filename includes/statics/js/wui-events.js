function __callback(item, event, callback, params) {
	let objs = {};
	objs['__id__'] = item.id;
	objs['__event__'] = event;
	let all = document.getElementsByTagName("input");
	let fileWuiComponentRegex = new RegExp('file_wui_([\-a-zA-Z0-9_]+)_component');
	let fileWuiValueRegex = new RegExp('file_wui_([\-a-zA-Z0-9_]+)_value');
	for (var i = 0; i < all.length; i++) {
		if (all[i].files!= undefined && Object.keys(all[i].files).length>0){
			continue;
		} else {
			if (fileWuiComponentRegex.test(all[i].id)) {continue;}
			if (fileWuiValueRegex.test(all[i].id)) {
				let _id = all[i].id.replace('file_wui_', '').replace('_value', '');
				objs[_id] = all[i].value;
			}else {
				objs[all[i].id] = all[i].value;
			}
		}
	}
	
	let saveWuiComponentRegex = new RegExp('save_wui_([\-a-zA-Z0-9_]+)_component');
	let saveWuiValueRegex = new RegExp('save_wui_([\-a-zA-Z0-9_]+)_value');
	for (var i = 0; i < all.length; i++) {
		if (all[i].files != undefined && Object.keys(all[i].files).length > 0) {
			continue;
		} else {
			if (saveWuiComponentRegex.test(all[i].id)) { continue; }
			if (saveWuiValueRegex.test(all[i].id)) {
				let _id = all[i].id.replace('save_wui_', '').replace('_value', '');
				objs[_id] = all[i].value;
			} else {
				objs[all[i].id] = all[i].value;
			}
		}
	}
	
	all = document.getElementsByTagName("textarea");
	for (var i = 0; i < all.length; i++) {
		objs[all[i].id] = all[i].value;
	}
	let params_j=JSON.parse(params);
	let params_k = Object.keys(params_j);
	for (var i = 0; i < params_k.length; i++) {
		objs[params_k[i]] = params_j[params_k[i]];
	}
	__send(callback, JSON.stringify(objs));
}


function __send(callback, params){
		let xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", "launcher.cgi", true);
		xmlhttp.setRequestHeader("Method", "POST " + window.location + " HTTP/1.1");
		xmlhttp.setRequestHeader("Content-Type", "application/json; charset=utf-8");
		xmlhttp.responseType = "application/javascript; charset=utf-8";

		xmlhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				if (this.response != '' && this.response!=undefined)
					eval(unescape(this.response));
			}
		};
		xmlhttp.send("{\"callback\":\"" + callback + "\",\"vars\":" + params + "}");
	
		return false;
}

function __confirm(msg, callback){
	if (confirm(msg)){
		__callback({id:'wui_js_confirm'}, true, callback, '[]');
	}else {
		__callback({id:'wui_js_confirm'}, false, callback, '[]');
	}
}

function __prompt(msg, callback, _default) {
	var res = prompt(msg, _default);
	if (res!=null) {
		__callback({ id: 'wui_js_prompt' }, res, callback, '[]');
	} else {
		__callback({ id: 'wui_js_prompt' }, false, callback, '[]');
	}
}

var wui_thread={};
function __thread(_id, callback, delay, loop) {
	if (loop){
		wui_thread[_id] = setInterval(
			function () { __callback({ id: _id }, true, callback, '[]'); },
			delay
		);
	}else{
		wui_thread[_id] = setTimeout(
			function () { __callback({ id: _id }, true, callback, '[]');},
			delay
		);
	}
}

function __closeThread(id, loop){
	if (loop) {
		clearInterval(wui_thread[id]);
	}else {
		clearTimeout(wui_thread[id]);
	}
}

function addslashes(iid) {
	str = iid.replace(/"/g, '\"');
	str = iid.replace(/'/g, '\'');
	return str;
}


selected_dialog=null;
file_component = null;
label_file_component = null;

save_component = null;
label_save_component = null;

directory_component = null;
label_directory_component = null;


var socket
function websocketInit() {
	var host = "ws://127.0.0.1:9000/"; // SET THIS TO YOUR SERVER
	try {
		socket = new WebSocket(host);
		console.log('WebSocket - status ' + socket.readyState);
		socket.onopen = function (msg) {
			console.log("Welcome - status " + this.readyState);
		};
		socket.onmessage = function (msg) {
			console.log("Received: " + msg.data);
		};
		socket.onclose = function (msg) {
			console.log("Disconnected - status " + this.readyState);
		};
	}
	catch (ex) {
		console.log(ex);
	}
}

function websocketSend(msg) {
	if (!msg) {
		console.log("Message can not be empty");
		return;
	}
	try {
		socket.send(msg);
		console.log('Sent: ' + msg);
	} catch (ex) {
		console.log(ex);
	}
}
function websocketQuit() {
	if (socket != null) {
		console.log("Goodbye!");
		socket.close();
		socket = null;
	}
}

function websocketReconnect() {
	websocketQuit();
	websocketInit();
}
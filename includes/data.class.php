<?php
require_once __DIR__.'/tools.class.php';
/**
 * Classe de gestion des données.
 * Cette classe regroupe un certain nombre de methode lui permettant \
 * de travailler sur les flux de donnée (CSV, Dat, Json, Xml)
 * 
 * @return stdClass
 */
class Data extends Tools {
	
	public function __construct() {
		parent::__construct();
	}
 
	/**
	 * Convertie un flux CSV en tableau
	 * 
	 * @param string $text fichier ou chaine de caractère à convertir en tableau
	 * @param string $delimiter Délimitateur des colonnes
	 * @return array
	 */
	private function getCSV($text, $delimiter=null, $enclosure=null){
		if(!isset($this->csv_delimiter)) $this->csv_delimiter=';';
		if(!isset($this->csv_enclosure)) $this->csv_enclosure='"';
		//$Data = str_getcsv($text, "\n"); //parse the rows
		//foreach($Data as &$Row) $Row = str_getcsv($text, $this->csv_delimiter);
		return str_getcsv($text, (!is_null($delimiter)?$delimiter:$this->csv_delimiter),(!is_null($enclosure)?$enclosure:$this->csv_enclosure));
	}
	
	/**
	 * Permet de forcer la cast d'une valeur
	 * 
	 * @param string $cast cast à appliquer à la valeur
	 * @param string $value valuer à caster
	 * @return mixed valeur casté
	 */
	public static function cast($cast='', $value=''){
		switch (strtolower($cast)){
			case 'i':
			case 'int':
			case 'integer': return (integer)$value; break;
			case 'f':
			case 'flt':
			case 'float': return (float)$value; break;
			case 's':
			case 'str':
			case 'string': return (string)$value; break;
			case 'b':
			case 'bool':
			case 'boolean': return (boolean)$value; break;
			default: return $value;
		}
	}
	
	/**
	 * Permet de faire une opération mathématique
	 * 
	 * @param integer|float $v1 première valuer à calculer
	 * @param string $operator opéarteur de calcul
	 * @param integer|float $v2 deuxième valuer à calculer
	 * @return somme du calcul
	 */
	public static function math($v1=0.0000, $operator='+', $v2=0.0000){
		if (is_null($v1) && $v1=='')$v1=0.0000;
		if (is_null($operator) && $operator=='')$operator='+';
		if (is_null($v2) && $v2=='')$v2=0.0000;
		$m=0;
		//echo '$m=('.$v1.''.$operator.''.$v2.');'; exit;
		eval('$m=('.$v1.''.$operator.''.$v2.');');
		return $m;
	}
	
	/**
	 * Concatène deux chaines de caractères
	 * 
	 * @param string $add valeur à ajouter
	 * @param string $value chaine traité
	 * @param boolean $is_begin la concaténation est au début ou à la fin
	 * @return retourne la chaine concatenée.
	 */
	public static function concat($add='', $value='', $is_begin=false){
		if ($is_begin) return $add.$value;
		return $value.$add;
	}
	
	/**
	 * Transforme un flux CSV en tableau
	 * 
	 * @param string $flux fichier ou chaine de caractère à convertir en tableau
	 * @param string $delimiter Délimitateur des colonnes
	 * @param string $has_header La première ligne sera converti en clé associative
	 * @param boolean $is_file est-ce que $flux est un fichier
	 * @return array
	 */
	protected function csv($flux, $delimiter=',', $enclosure='"', $has_header = true, $is_file=true) {
		$this->csv_delimiter=$delimiter;
		$this->csv_enclosure=$enclosure;
		if($is_file && (!file_exists($flux) || !is_readable($flux))) return false;
		else if($is_file) {
			$data = array_map('self::getCSV', file($flux, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
		} else {
			//$flux=preg_replace('/\r\n\r\n|\n\r\n\r|\r\r|\n\n/', "\n", $flux);
			//$flux=str_replace("\r", '', $flux);
			$flux_end='';
			// $flux=preg_replace("/;\"\n/", ';"', $flux);
			//$flux=preg_replace('/'.$delimiter.'"(.*)'."\n".'(.*)"'.$delimiter.'/', $delimiter.'"$1<br/>$2"'.$delimiter, $flux);
			$enclos=false;
			for ($k=0; $k<strlen($flux); $k++) {
				if (!$enclos && substr($flux, $k, 1)==$enclosure && substr($flux, $k-1, 1)!='\\') $enclos=true;
				else if ($enclos && substr($flux, $k, 1)==$enclosure && substr($flux, $k-1, 1)!='\\') $enclos=false;
				if ($enclos && substr($flux, $k, 1)=="\n"){
					$flux_end.='\n';
				} else {
					$flux_end.=substr($flux, $k, 1);
				}
			}
			//echo $flux_end; exit;

			// while (preg_match('/'.$delimiter.'"(.*)'."\n".'(.*)"'.$delimiter.'/', $flux)) {
			// 	$flux=preg_replace('/'.$delimiter.'"(.*)'."\n".'(.*)"'.$delimiter.'/', $delimiter.'"$1<br/>$2"'.$delimiter, $flux);
			// }
			// $flux=preg_replace('/'.$delimiter.'"(.*)\n"'.$delimiter.'/', $delimiter.'"$1\\n"'.$delimiter, $flux);
			// $flux=preg_replace('/'.$delimiter.'"\n(.*)"'.$delimiter.'/', $delimiter.'"\\n$1"'.$delimiter, $flux);
			//file_put_contents('csv.tmp', preg_replace('/'.$delimiter.'"(.*)\n(.*)"'.$delimiter.'/', $delimiter.'"$1\\n$2"'.$delimiter, $flux));
			$data = array_map('self::getCSV', self::getCSV($flux_end, "\n")); //file('csv.tmp', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
			//unlink('csv.tmp');
		}
		
		if ($has_header){
			$headers=$data[0];
			$ii=0;
			array_walk($data, function(&$a) use (&$headers, &$ii) {
				$ch=count($headers);
				$ca=count($a);
				if ($ch!=$ca){
					if ($ch>$ca){
						$ec=$ch-$ca;
						for ($g=0;$g<$ec; $g++) {
							$a[]=null;
						}
					} else {
						$ec=$ca-$ch;
						for ($g=1;$g<$ec+1; $g++) {
							if (isset($a[$ca-$g])) unset($a[$ca-$g]);
						}
					}
				}
				$a = array_combine($headers, $a);
				$ii++;
			});
			if (count($data)>1) array_shift($data); # remove column header
		}
		return $data;
	}
	
	
	/**
	 * Encode un tableau en CSV
	 * 
	 * @param array $array tableau à encoder
	 * @param string $delimiter délimitateur des colonnes
	 * @param string $lr caractère de retour à la ligne
	 * @param string $has_header les clés du tableau seront converties en entête du csv
	 * @return string
	 */
	public static function toCsv($array, $delimiter=';', $lr="\n", $has_header = true) {
		$str='';
		if ($has_header){
			$header='';
			$h=false;
			foreach ($array as $index => $line) {
				$s=true;
				foreach ($line as $key => $value) {
					if (!$h)$header.=($s?'':$delimiter).$key;
					$str.=($s?'':$delimiter).(is_string($value)?'"'.str_replace([/*"\n",*/ '"'],[/*'<br/>',*/'""'],$value).'"':$value);
					$s=false;
				}
				$str.=$lr;
				if (!$h)$header.=$lr;
				$h=true;
			}
			return $header.$str;
		} 
		foreach ($array as $index => $line) {
			$str.=implode($delimiter, $line).$lr;
		}
		return $str;
	}
	
	/**
	 * Transforme un flux Json en tableau
	 * 
	 * @param string $flux fichier ou chaine de caractère à convertir en tableau
	 * @param boolean $is_object doit-on convertir le flux en object
	 * @param boolean $is_file est-ce que $flux est un fichier
	 * @return array|object
	 */
	public static function json($flux, $is_object = false, $is_file=true) {
		if($is_file && (!file_exists($flux) || !is_readable($flux))) return false;
		else if($is_file) return json_decode(self::file($flux), !$is_object);
		else return json_decode($flux, !$is_object);
	}
	
	/**
	 * Encode un tableau en toJson
	 * 
	 * @param array $array tableau à encoder
	 * @param boolean $is_pretty_print encodage non compressé
	 * @return string
	 */
	public static function toJson($array, $is_pretty_print = true) {
		if ($is_pretty_print){
			return json_encode($array, JSON_PRETTY_PRINT);
		}
		return json_encode($array);
	}
	
	/**
	 * Transforme un flux Dat en tableau
	 * 
	 * @param string $flux fichier ou chaine de caractère à convertir en tableau
	 * @param boolean $is_file est-ce que $flux est un fichier
	 * @return array
	 */
	public static function dat($flux, $is_file=true) {
		if($is_file && (!file_exists($flux) || !is_readable($flux))) return false;
		else if($is_file) return unserialize(self::file($flux));
		else return unserialize($flux);
	}
	
	/**
	 * Encode un tableau en Dat
	 * 
	 * @param array $array tableau à encoder
	 * @return string
	 */
	public static function toDat($array) {
		return serialize($array);
	}
	
	
	/**
	 * Décode du base64
	 * 
	 * @param string $str chaine à décoder
	 * @return string
	 */
	public static function b64($str) {
		return base64_decode($str);
	}
	
	/**
	 * Encode en base64
	 * 
	 * @param string $str chaine à encoder
	 * @return string
	 */
	public static function toB64($str) {
		if (is_array($str)) return base64_encode(json_encode($str));
		return base64_encode($str);
	}
	
	/**
	 * Transforme un object en tableau
	 * 
	 * @param object $object object à convertir en tableau
	 * @return array
	 */
	public static function object2array($object) { return @json_decode(@json_encode($object),true); } 
	
	/**
	 * Transforme un flux XML en tableau
	 * 
	 * @todo inclure la gestion LIBXML_NOCDATA
	 * @param string $flux fichier ou chaine de caractère à convertir en tableau
	 * @param boolean $is_file est-ce que $flux est un fichier
	 * @return array
	 */
	public static function xml($flux, $is_file=true) {
		if($is_file && (!file_exists($flux) || !is_readable($flux))) return false;
		else if($is_file) return self::object2array(simplexml_load_string(self::file($flux)));
		else return self::object2array(simplexml_load_string($flux));
	}
	
	/**
	 * Transforme tableau en un flux XML
	 * 
	 * @param array $array tableau à convertir en xml
	 * @param string $tag tag englobant
	 * @return string
	 */
	public static function toXML($array, $tag='') {
		$xml='';
		foreach ($array as $key=>$value) {
			if (is_array($value)) {
				$xml.=self::toXML($value, $key);
		  } else {
				if (is_numeric($key)) $key='__'.$key.'__';
				$xml.="<$key>".$value."</$key>";
			}
		}
		if (is_numeric($tag)) $tag='__'.$tag.'__';
		return "<$tag>".$xml."</$tag>"; 
	}
	
	/**
	 * Supprime les caractère BOM d'un flux
	 * 
	 * @param string $str chaine de caractère à néttoyer
	 * @return string
	 */
	public static function removeBOM($str){return str_replace("\xEF\xBB\xBF", '', $str);}
	
	
	/**
	 * Charge en fichier en chaine de caractère
	 * 
	 * @param string $filename fichier à ouvrir
	 * @return string
	 */
	public static function file($filename) {
		if(!file_exists($filename) || !is_readable($filename)) return false;
		else return file_get_contents($filename);
	}
	
	/**
	 * Charge le contenue d'une URL en chaine de caractère
	 * 
	 * @param string $url url à récupérer
	 * @return string
	 */
	public static function urlStream($url) {
		return file_get_contents($url);
	}
	
	/**
	 * Enregistre une chaine de caractère dans un fichier
	 * 
	 * @param string $filename fichier cible
	 * @param string $str donnée à inscrire dans le fichier
	 * @param boolean $add doit-on ajouter le contenue à la suite dans le fichier
	 * @return void
	 */
	public static function toFile($filename, $str='', $add = false) {
		$dir = dirname($filename);

		if (!file_exists($dir) || !is_dir($dir)) mkdir($dir, 0755, true);
		
		if ($add && file_exists($filename)){
			file_put_contents($filename, $str, FILE_APPEND);
			return;
		}
		file_put_contents($filename, $str);
	}

	/**
	 * Retourne vrai si la chaîne contient des balises
	 *
	 * @param $string
	 *
	 * @return bool
	 */
	public static function isHtml($string) {
		return $string !== strip_tags($string);
	}
	
	/**
	 * Sécurise une chaine de caractère.
	 */
	public static function str_securize($str,$options = array('full')){
		if(in_array('full',$options)):
			$options = array('html','trim','upper','lower','accent','ponc','tab','return','dspace', 'space', 'bom', '..');
		endif;

		foreach($options as $option):
			switch($option){
				// Suppression des espaces vides en debut et fin de chaque ligne
				case 'trim':
					$str = preg_replace("#^[\t\f\v ]+|[\t\f\v ]+$#m",'',$str);
				break;

				// Remplacement des caractères accentués par leurs équivalents non accentués
				case 'accent':
					$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
					$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
					$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. 'œ'
					$str = html_entity_decode($str); 
				break;

				// Transforme tout le texte en minuscule
				case 'lower':
					$str = mb_strtolower($str, 'UTF-8');
				break;

				// Transforme tout le texte en majuscule
				case 'upper':
					$str = mb_strtoupper($str, 'UTF-8');
				break;

				// Remplace toute la ponctuation par des espaces
				case 'ponc':
					$str = preg_replace('#[^\.\-\/\w]#','',$str);
					$exceptions = array("’");
					$str = str_replace($exceptions,'',$str);
				break;
				
				case 'poncfull':
					$str = preg_replace('#([[:punct:]])#','',$str);
					$exceptions = array("’");
					$str = str_replace($exceptions,'',$str);
				break;

				// Remplace les tabulations par des espaces
				case 'tab':
					$str = preg_replace("#\h#u", "", $str);
				break;

				// Remplace les espaces multiples par des espaces simples
				case 'dspace':
					$str = preg_replace('#[" "]{2,}#',' ',$str);
				break;

				// Remplace 1 entrée (\r\n) par 1 espace
				case 'return':
					$str = str_replace(array("\r","\n"),'',$str);
				break;

				// Supprime toutes les balises html
				case 'html':
					$str = strip_tags($str);
				break;
				
				case 'bom':
					$str = self::removeBom($str);
				break;
				
				case '..':
					$str = str_replace(['../','..'],'',$str);
				break;
				
				case 'space':
					$str = str_replace(' ','',$str);
				break;
				
				case '%':
					$str = str_replace('%','',$str);
				break;
				
				case '0':
					$str = str_replace("\0",'',$str);
				break;
			}
		endforeach;
		
		return $str;
	}

    /**
     * Efface les espaces en début et fin de chaque clé et chaque valeur du tableau
     *
     * @param $array
     *
     * @return array|false
     */
	public static function trim_array($array)
    {
        $keys = array_map('trim', array_keys($array));
        $values = array_map('trim', $array);

        return array_combine($keys, $values);
    }
}
?>
